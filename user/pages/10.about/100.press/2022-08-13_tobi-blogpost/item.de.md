---
title: "Suche nach dem richtigen Leben im Falschen"
date: "2020-08-13"
---

_Ein Blogpost, in dem Tobi über seine Erfahrung mit Kanthaus als Teil einer größeren Reise durch Deutschland schreibt._

===

#### Blogpost
Der Blogpost kann barrierfrei auf [Tobi's Blog](https://syntobi.me/post/07-kanthaus/) gelesen werden.
