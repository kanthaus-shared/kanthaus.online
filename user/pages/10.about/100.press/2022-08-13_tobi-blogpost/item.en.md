---
title: "Suche nach dem richtigen Leben im Falschen"
date: "2020-08-13"
---

_A German blogpost detailing Tobi's experience of Kanthaus as part of their bigger tour through Germany._

===

#### Blogpost
The post can be read without barrier on [Tobi's blog](https://syntobi.me/post/07-kanthaus/)
