---
title: "100 Prozent"
date: "2020-08-03"
---

_A German TV report about people who live without a boundary between work and private life. It features Kanthaus as well as two other projects._

===

#### Stream
The show '100 percent - Life without compromise: We share everything! My life is my work' can be streamed for free on [ARD mediathek](https://www.ardmediathek.de/video/100-prozent-leben-ohne-kompromisse/wir-teilen-alles-mein-leben-ist-meine-arbeit/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE1OTY3MzM/).
