---
title: Wunschliste
description: ""
weight: 4
draft: false
---

## Menschen

- interessiert daran, am Haus selbst mitzuarbeiten (bauen, einrichten, Instandhaltung)
- interessiert daran, an bestehenden oder im Aufbau befindlichen Projekten mitzuarbeiten (foodsharing, Repair-Café)
- interessiert daran, eigene Projekte im oder mit dem Haus einzubringen (insbesondere solche, die eine Außenwirkung erzielen)

## Dinge, die gebraucht werden:

Nach unserer Vision versuchen wir Dinge wiederzuverwenden, die anderweitig nicht gebraucht werden (beispielsweise zu viel gekauft oder etwas liegt einfach ungenutzt herum) oder nicht gewollt werden (beispielsweise optische Mängel, geringe Reparaturen notwendig, entspricht nicht mehr dem aktuellsten Stand).
Wir brauchen Material und Werkzeuge für Instandhaltungs- und Sanierungsarbeiten am Haus sowie für Projekte, welche durchgeführt werden sollen.
Wenn du glaubst, etwas von der folgenden Liste (oder ähnliche Dinge) abgeben zu möchten, oder eventuell eine Firma kennst, welche Material übrig hat (Herstellungsfehler etc.), so kontaktiere uns bitte.
Wir freuen uns auch über (leicht) kaputte Dinge, die reparierbar sind. Eventuell nutzen wir diese sogar, um einen Reparaturworkshop durchzuführen.

### Küchenausstattung:
- Thermos- und Teekannen (ab 2 Liter)

### Werkstatteinrichtung:

- Schleifmaschine (z.B. Bandschleifer)
- Oberfräse
- Kernbohrer & -maschine
- Schrauben und anderes Verbrauchsmaterial
- Fahrradwerkzeug und Material

### Laboreinrichtung:

- Labornetzteile
- elektronische Bauteile (aktive & passive Bauelemente)
- Reagenzgläser

## Garten

- Pflanzstangen für Tomaten etc.
- Schaufel
- Spaten

### Wohn- und Büroeinrichtung:

- große Spannbettlaken (1,40-1,60m)
- große Bettdecken und Bezüge
- Schwerlastregale
- Beamer (gerne mit LED-Technologie)
- Mehrfachsteckdosen
- Verlängerungskabel (gerne auch kürzer als 5m)
- LED-Leuchtmittel (E14, E27)
- Flachbildschirme mit mindestens 22 Zoll
- Laptops und Computer, welche mindestens der Intel Core-I Leistungsklasse angehören
- optische Mäuse, Tastaturen
- Telefonkabel (J-2Y(St)Y oder J-YY, 4 oder 8 Adern)
- professionelle Wlan-Accesspoints (802.11ac, Ubiquiti/Cisco oder moderne, von OpenWRT unterstützte Hardware wie TPLink RE450 oder ähnliche)

### Diverses

- Fensterputz-Equipment
- Kletterseile

## Gern gesehen

- [Geld](../../contact) (aber nur wenn du es entbehren kannst!)

## Orte, an denen Dinge gesucht werden kann

Du kannst beispielsweise auf [ebay-Kleinanzeigen](https://www.ebay-kleinanzeigen.de/), der lokalen Recyclingfirma, dem Projektverteiler Leipzig oder auf Webseiten, die sich dem Teilen widmen, suchen.
