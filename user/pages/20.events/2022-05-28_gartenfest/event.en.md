---
title: 'Kanthaus garden fest'
date: '2022-05-28 12:00'
enddate: '2022-05-28 18:00'
header_image: false
---

Kanthaus garden fest! Food, drinks and music. Everyone welcome.

<div markdown="1" style="text-align:center;">
![](gartenfest.png)
</div>

Upcoming Saturday we host a Gartenfest: A day on which our door is open to the public. There will be food, drinks and music. You'll be offered to visit the free shop [1] and attend house tours to learn something about sustainable functional living, but also about cool construction projects (e.g. our indoor compost toilet [2], our experiments with rainwater toilet flushes, heating with a heatpump and solar energy, ...).

Our house is set up to be an international commons project [3] that is enabling people to work on political projects while suffering less economical pressure. At the same time we want to reflect on what we really need and make use of the things existing around us, to recycle and save them from waste. This leads to us using all our infrastructure together: all of the rooms and most of the things.

Maybe you are interested in the different layers of our communal decision making? Or you might be interested in one of the many projects that are being worked on inside our walls? Or maybe you just want to hang out with interesting people from all over the world? Whatever it may be, just come around and let's have a nice time together!

Foot notes:

[1] [What is a free shop?](/projects/open-tuesday) It's a place to which people can bring things and from which people can take things. It is not based on exchange logic, which means that you can always just bring or just take. It's meant to give the abundant stuff around us a second (or third, or fourth, or...) life instead of throwing good stuff away.

[2] [What is a compost toilet?](https://en.wikipedia.org/wiki/Composting_toilet) It's a toilet that doesn't work with a water flush and instead composts the feces to make good humus out of it. There are many different concepts, all of which share the aim to reduce waste, toxins and dependency on centralized water treatment systems.

[3] [What are commons?](https://en.wikipedia.org/wiki/Commons) It's a kind of ownership that shifts the boundaries from individual people to groups or societies. Open souce software, wikipedia, free shops and squats are examples of commons structures.
