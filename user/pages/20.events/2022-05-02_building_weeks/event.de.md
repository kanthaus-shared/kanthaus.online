---
title: 'Selbstorganisierte Bauwochen'
date: '2022-05-02 9:00'
enddate: '2022-05-22 16:00'
header_image: false
---

Das Dach ist fertig - na und?!
Die nächsten Kanthaus-Bauwochen sind dran!

<div markdown="1" style="text-align:center;">
![](nathalie_drill.jpg)
</div>

===

Diesmal wird es statt der einen riesigen viele kleine Baustellen geben: Dachbodendämmung vorbereiten, Lüftungssystem ausdehnen und zu guter Letzt auch endlich mal ein paar Räume schöner machen. Selbstständige Kleingruppen können sich die Projekte suchen, die sie motivieren. Lernen und skillshare findet in diesen kleinen projektgebundenen Einheiten möglichst nachhaltig statt, indem Gelerntes direkt am selben Projekt auch wieder angewendet werden kann.

Wir verstehen uns als anarchistische Baustelle, auf der zwar manche mehr Erfahrung und Wissen mitbringen, aber jeder Mensch für das geachtet wird, was er ist, und keins den Anspruch hat Boss von den anderen zu sein. Besuchis suchen sich ihre Aufgabenfelder möglichst selbst anhand ihrer Interessen aus und bringen sich da ein, wo sie sich wohlfühlen. Wer nicht bauen mag kann trotzdem gerne helfen, denn ohne Repro geht natürlich nichts: Kochen, aufräumen, abwaschen, Kinder betreuen - all das muss auch erledigt sein und wird nicht weniger geschätzt als Kreissäge und Akkuschrauber zu bedienen.

Neben der Arbeit soll es auch Zeit für Vergnügen und gegenseitiges Kennenlernen geben. Wir wollen zusammen Ausflüge machen, Spiele spielen, Filme gucken, Lieder singen, uns unser Herz ausschütten (wer mag) und die gemeinsame Zeit reflektieren - wenn du noch weitere Ideen oder Impulse hast, bring sie gerne ein!

Damit das alles so reibungslos wie möglich funktioniert, sollten Menschen mindestens eine Woche am Stück bleiben, mit Englisch klar kommen (lies zB mal die englische Version dieser Veranstaltungsbeschreibung, da steckt noch viel mehr Info drin... ;)) und sich bald bei uns unter hello@kanthaus.online anmelden. Wir freuen uns auf euch! :)
