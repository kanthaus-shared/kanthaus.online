---
title: 'Self-organized building weeks'
date: '2022-05-02 9:00'
enddate: '2022-05-22 16:00'
header_image: false
---

The roof is done - so what?!
The next building weeks are coming up in Kanthaus!

<div markdown="1" style="text-align:center;">
![](nathalie_drill.jpg)
</div>

===

From May 2 to May 22 we are warmly inviting people to visit and help us to make Kanthaus better! There are many construction sites that could be opened and the actual work that you'll do will depend on what you are motivated to tackle. Still, getting our attics ready to be insulated is one of our main focus areas. Apart from that we can extend our existing ventilation system to get better air quality and less mold in more rooms, and we can do classic renovation to finally have beauty move into more rooms. There will be a large variety of things to do and all technical levels are welcome! If you want to (learn to) measure and cut wood, screw wooden frames to walls, cut pipes, glue tape, plaster walls, rip off wallpaper, paint walls or even build in a roof window, we'll definitely find a task for you.
The construction work will not be possible without the support of people working on reproductive tasks (food saving, cooking, childcare): if you feel no desire to be on the roof and would rather help inside the house, you are also absolutely welcome!

This will be an anarchist workplace: some people will have more skills, knowledge or responsibilities, but all of those are to be shared, nobody is anyone's boss, and everybody is to be respected for what they are. This building time should be an opportunity for empowerment, and experimenting different social setups: if there is interest from the people present, FINTA*-only sessions could be organized.

Of course we will also enjoy some time to relax: there will be amazing community dinners (but also a more cozy/intimate option for the more introverted), we can have games, watch movies, play music, do acro yoga, go swim in the nearby lake, go biking and hiking, make a fire, and if that's not enough, you are warmly invite to bring and share your own way to celebrate life!

For all this to work as smoothly as possible we'd like you to first read our [page about visiting Kanthaus](/about/visiting) stay for at least a week and write to us (hello@kanthaus.online) in advance. We're looking forward to having you, getting to know you and to start working together!
