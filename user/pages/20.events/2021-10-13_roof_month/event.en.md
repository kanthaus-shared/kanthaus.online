---
title: 'Roof month'
date: '2021-10-13 11:00'
enddate: '2021-11-10 13:00'
---

Kanthaus Building Month is back!

From 13 October to 10 November, the Kanthaus residents will be working on renovating the 4th and last part of the house roof, and we would love to have the support of some more people! There will be a large variety of things to do on and around the roof: all technical levels are welcome!

<div markdown="1" style="text-align:center;">
![](/10.about/roofwork.jpg)
</div>

===

While the roof will be the collective priority, this work would not be possible without the support of people working on reproductive work (food saving, cooking, childcare): if you feel no desire to be on the roof and would rather help inside the house, you are also absolutely welcome!

This will be an anarchist workplace: some people will have more skills, knowledge or responsibilities, but all of those are to be shared, nobody is anyone’s boss, and everybody is to be respected for what they are. This building month should be an opportunity for empowerment, and experimenting different social setups: if there is interest from the people present, FINTA*-only sessions could be organized.

Of course we will also enjoy some time to relax: there will be amazing community dinners (but also a more cozy/intimate option for the more introverted), we will have games, watch movies, play music, do acro yoga, go swim in the nearby lake, enjoy a good sauna, dance to the night, sing to the moon, and if that’s not enought, you are warmly invite to bring and share your own way to celebrate life!
German
