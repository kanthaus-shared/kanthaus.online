---
title: 'Dachbaumonat'
date: '2021-10-13 11:00'
enddate: '2021-11-10 13:00'
---

Der nächste Kanthaus Bau-Monat steht an!

Vom 13. Oktober bis zum 10. November werden die Kanthausianer*Innen am vierten und letzten Dachabschnitt arbeiten und wir würden uns super über die Unterstützung weiterer Menschen freuen! Es wird eine Menge unterschiedlicher Dinge auf und am Dach zu tun geben: Alle Level an Erfahrung sind willkommen!

<div markdown="1" style="text-align:center;">
![](/10.about/roofwork.jpg)
</div>

===

Das Dach wird die kollektive Priorität darstellen, allerdings wissen wir, dass die Arbeit daran nicht möglich ist, ohne Menschen, die die notwendigen Repro-Aufgaben übernehmen (Lebensmittel retten, kochen, Kinder betreuen): Falls du also keine Ambitionen hast selbst aufs Dach zu gehen, uns aber gern im Haushalt unterstützen möchtest, bist du ebenfalls absolut gern gesehen!

Es wird ein anarchistischer Arbeitsplatz sein: Manche Menschen werden mehr Fähigkeiten, Wissen oder Verantwortung haben als andere, aber all dies soll geteilt werden, niemand ist der Boss von irgendwem und alle sollen dafür respektiert werden, wer und was sie sind. Dieser Bau-Monat soll eine Gelegenheit zur Selbstermächtigung sein, und dafür mit verschiedenen sozialen Umständen zu experimentieren: Wenn Anwesende Interesse daran ausdrücken, können zB FINTA*-Arbeitsslots organisiert werden.

Natürlich werden wir auch entspannende Zeiten gemeinsam genießen: Es wird tolle gemeinsame Abendessen geben (auch mit ruhigeren, rückzugigeren Optionen für eher introvertierte Menschen), wir werden Spiele spielen, Filme gucken, Musik machen, Acroyoga machen, im nahegelegenen See schwimmen, einen schönen Saunagang genießen, in die Nacht tanzen, den Mond ansingen und wenn das noch nicht reicht, bist du herzlich dazu angehalten deine eigenen Methoden das Leben zu feiern mit einzubringen!
