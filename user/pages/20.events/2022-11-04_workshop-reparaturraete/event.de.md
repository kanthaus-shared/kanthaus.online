---
title: 'Workshop Reparaturräte'
date: '2022-11-04 16:00'
enddate: '2022-11-04 19:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](repair.jpg)
</div>

Was muss geschehen, damit wir endlich weniger Schrott produzieren? Wie können wir bürgerschaftlich aktiv werden und welchen Beitrag leistet eine urbane Reparaturkultur? Diese Themen und Fragen finden überraschende Antworten im Vortrag von Stefan Schridde, ([MURKS? NEIN DANKE!](http://www.murks-nein-danke.de/)) und bereiten auf den nachmittäglichen Workshop vor, um Ideen zu entwickeln zum Aufbau eines Netzwerkes für urbane Reparaturkultur in Wurzen.

===

Zum Vortragenden:<br>
Stefan Schridde ist seit April 2011 Initiator der Kampagne und seit 2013 Vorstand von MURKS? NEIN DANKE! e.V., einer bürgerschaftlichen Organisation für nachhaltige Produktqualität und gegen geplante Obsoleszenz. Journalistisch und als Autor ist Stefan Schridde im Bereich TV, Radio, Print, Online und als Studien- und Buchautor aktiv. Mit zahlreichen Veröffentlichungen, Vorträgen, Rundfunk- und Fernsehinterviews zu geplanter Obsoleszenz ist Stefan Schridde medial auf europäischer Ebene präsent.

Als Gründer der Initiative MURKS? NEIN DANKE! e.V. trägt Stefan Schridde in ganz Europa zu der Debatte über die schadhaften Folgen von geplanter Obsoleszenz und deren Beendigung bei. Dazu steht er seit 2012 in direktem Austausch mit Parteien, Bundestagsfraktionen, Abgeordneten, Ministerien, Behörden, Unternehmen, Hochschulen, Instituten, Forschungseinrichtungen, Berufsverbänden, Gewerkschaften, NGO und weiteren befreundeten Netzwerken.

---

Diese Veranstaltung ist der Auftakt zu den *Wochen der Nachhaltigkeit*. Weitere Termine sind:
- Freitag, der 11.11.22: Film & Gespräch "Der marktgerechte Mensch" im D5
- Mittwoch, der 23.11.22: Küche für alle und Gespräch zu solidarischer Landwirtschaft im D5
- Mittwoch, der 30.11.22: Weiterbildung Lieferketten in der ALM

