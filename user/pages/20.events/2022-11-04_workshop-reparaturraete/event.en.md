---
title: 'Workshop about repair networks'
date: '2022-11-04 16:00'
enddate: '2022-11-04 19:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](repair.jpg)
</div>

*The default language of this event is German!*

What has to happen for us to finally produce less crap? How can we get active as citizens and which part does a culture of urban repair play? These topics and questions will be answered in unexpected ways by Stefan Schridde ([MURKS? NEIN DANKE!](http://www.murks-nein-danke.de/)). The following workshop will make room for ideas to create a local network for repair culture here in Wurzen.

===

About the speaker:<br>
Since April 2011 Stefan Schridde is the initator of the campaign MURKS? NEIN DANKE! e.V. - a citizen organisation for sustainable product quality and against planned obsolescence. He is a journalist and author and active in a wide range of media all over Europe.

As founder of MURKS? NEIN DANKE! e.V. Stefan Schridde contributes to the public debate about planned obsolescence, pointing out its detrimental consequences and how to end the harmful practice. For this he is in direct contact with political parties, members of parliament, ministries, authorities, companies, universities, institutes, research facilities, unions, NGOs and befriended networks since 2012.

---

This event is the start of the weeks of sustainability. More dates are:
- Friday Nov. 11 2022: *Film and exchange "Der marktgerechte Mensch"* in D5
- Wednesday Nov. 23 2022: *Kitchen for all and exchange about community supported agriculture* in D5
- Wednesday Nov. 30 2022: *Workshop about supply chains on the examples textiles and IT* in ALM
