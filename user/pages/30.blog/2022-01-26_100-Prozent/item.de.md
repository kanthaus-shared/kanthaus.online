---
title: 'Gelungener Fernsehbeitrag über uns'
date: "2022-01-26"
taxonomy:
    tag: [blog]
    author: Janina
header_image: true
---

Nach dem Radio und diversen Printmedien hat sich nun also auch das Fernsehen für uns interessiert! Und das Outcome kann sich durchaus sehen lassen, finden wir. Bis auf den Thumbnail vielleicht...^^'

===

Wenn sich die Presse ankündigt (und noch dazu das Fernsehen!) stellt sich bei uns immer eine Mischung aus Freude, Stolz und Motivation, aber auch Sorge, Zögern und Zurückhaltung ein. Die Diskussionen am Esstisch laufen dann in etwa wie folgt ab:

"Ist doch toll, dass wir auf diese Weise ein großes Publikum erreichen können!" - "Aber wir wissen ja garnicht, ob wir auch positiv dargestellt werden. Was, wenn die uns total durch den Kakao ziehen und sich nur über die 'komischen Hippies' lustig machen wollen?" - "Die interessieren sich bestimmt eh wieder nur für unser Wohnkonzept, dabei ist das doch eigentlich voll nebensächlich. Wieso berichtet nicht mal wer über unsere Projekte?!" - "Naja du musst schon auch ein bisschen daran denken, wer das Zielpublikum ist. Ist doch klar, dass die unsere Wohnform erstmal neugierig macht. Und in einem 20-minütigem Beitrag kann dann eben nicht auch noch jedes Projekt im Detail vorgestellt werden. Wenn du sowas sehen willst, dann musst du es schon selber machen." - "Also ich find das nicht sinnvoll da jetzt viel Zeit reinzuinvestieren." - "Ich schon. Ich mach das jetzt."

Und so kam es, dass wir eine knappe Woche lang ein Filmteam zu Gast hatten. Nicht alle Kanthaus-Menschen sind auf den Aufnahmen zu sehen, aber doch viele. Und insgesamt sind wir mit dem Resultat doch sehr zufrieden. Auch wenn nicht im Detail auf alle Projekte eingegangen wurde... ;)

![](100prozent2.png)

Aber seht selbst: In der [ARD Mediathek](https://www.ardmediathek.de/video/100-prozent-leben-ohne-kompromisse/wir-teilen-alles-mein-leben-ist-meine-arbeit/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE1OTY3MzM/) gibt es die Sendung mit dem klangvollen (oder reißerischen..?) Titel "100 Prozent - Leben ohne Kompromisse: Wir teilen alles! Mein Leben ist meine Arbeit" noch bis Januar 2023 kostenfrei zu sehen.
