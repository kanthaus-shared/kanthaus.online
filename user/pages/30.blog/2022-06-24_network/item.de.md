---
title: 'Netzwerk einfach halten?'
date: "2022-06-24"
taxonomy:
    tag: [blog]
    author: chandi
---

Nach 5 Jahren hat das Kanthaus Netz einen Zustand erreicht, welcher auch etwas erfahrenere Netzwerk-Admins beim Versuch einen Überblick zu gewinnen teilweise Kopfzerbrechen bereitet.

Konkret waren es meine Kopfschmerzen und viel Zeit beim Versuch herrauszufinden, welches Kabel denn nun wohin geht.

Um es zukünftig Menschen leichter zu machen, Verantwortung für das Kanthaus-Netzwerk zu übernehmen, hab ich mir vorgenommen das Netzwerk zu überarbeiten, was sich in folgende Punkte zusammenfassen lässt:
- Vereinfachung der Topology
- Niedrigschwelligere WLAN-Netze
- Zentrale Verwaltung der Access Points
- Dokumentationen updaten


## Vereinfachung der Topology
Vermutlich lässt sich das Netzwerk am Besten beschreiben und begründen mit einem Wort: **gewachsen**


### Zuvor: ein gewachsenes Chaos
Alles began im K20-1, mit den ersten Räume die bewohnt wurden und Bedarf an Internet. Von dort aus wurden neue Kabel gelegt, wohin auch immer gerade eines gebraucht wurde. von da aus mit dem geringsten Aufwand weiter wohin auch immer das nächste gebraucht wurde.

Wer zu den Anfangszeiten da war erinnert sich vielleicht auch: Es gab zu Beginn unfassbar viele Baustellen im Haus, das wichtigste: grundlegende Infrastruktur irgendwie zum Laufen zu bringen, damit Menschen hier leben können - Schönheit und Niedrigschwelligkeit waren da erstmal keine relevanten Werte.


**(Bitte nicht zuviel Zeit verschwenden bei dem Versuch es zu durchblicken)**
![network topology before - a mess](https://pad.kanthaus.online/uploads/d40f9e85-d0d5-4779-a528-a23d15799a24.png)

Alle Geräte haben ihren berechtigten Zweck, aber die Verkabelung macht es nicht leicht das Netzwerk zu überblicken.


### Jetzt
Um den Aufbau des Netzes zukünftig leichter verständlich zu haben hält sich der neue Aufbau an folgende Punkte:
- Ein zentraler Ort im Keller
- Jeweils ein Kabel vom Keller in jedes Stockwerk
- Im Stockwerk ein Access Point als zentraler Ort für ggf weitere Verteilung

![network topology now: central rack in the basement, one cable to each floor](https://pad.kanthaus.online/uploads/a265c069-94d2-47b2-900c-3cd9c6f59afb.png)

hoffentlich für dich auch etwas leichter zu überblicken? :)



## Niedrigschwelligere WLAN-Netze
Im Kanthaus nutzen wir verschiedene Netze für mehr Sicherheit und um Menschen auch Zugang ohne Passwort zu ermöglichen.

**Beispiele**
- Öffentliches Gast-Netz, welches über die Niederlande geroutet wird damit wir keine Probleme mit illegaler Nutzung bekommen
- Netz für leicht hackbare Geräte, welche lieber abgekapselt ohne Internetzugriff laufen sollen [#IoT](https://en.wikipedia.org/wiki/Internet_of_things#Security) (Haustür, Drucker, Wechselrichter, Wasserzähler,...)
- Netz, in welchem Geräte sich nicht gegenseitig sehen können, weil es keine Notwendigkeit dafür gibt (zB Smartphones)


### Vorher
- Nutzung von "WPA2 Enterprise", welches zwar technische Vorteile bietet, aber eine sehr hohe Hürde in der Einrichtung welche für nahezu alle nicht ohne einer Anleitung zu schaffen war
- Verschiedene passwörter, die sich kein mensch merkt
- Sinn der verschiedenen Netze schwer zu verstehen, teilweise auch einfach unsinnig.
- Probleme mit Kompatibilität von manchen Funktionen

leztendlich sind fast alle beim altem `wuppdays` WiFi geblieben, weil mindestens eins der Punkte die neuen Netze nicht zugänglich gemacht haben

### Jetzt

- Klarer Entscheidungsweg
  ![](https://pad.kanthaus.online/uploads/9ce05938-c56f-443b-b320-d2699882f064.png)

- Für die beiden verschlüsselten Netze gilt das gleiche, einfach zu merkende, Passwort
- Einloggen können sich Menschen, wie in jedem anderem Netz auch (Auf Name klicken -> Passwort eingeben -> fertig)
- `kanthaus` mit höchstem Sicherheitsstandard als default
- `kanthaus-insecure` mit Kompatiblität für (hoffentlich) alle Geräte




## Zentrale Verwaltung der Access Points
Mitlerweile haben wir 7 Access Points (kurz AP) im Einsatz, welche alle richtig konfiguriert sein möchten. Nicht nur etwas zu ändern ist sehr aufwändig, es ist auch sehr anfällig dafür Fehler einzuführen.

### Vorher
Das Problem der zuvielen Access Points um sie einzeln zu konfigurieren, wurde schon versucht mit einem [selbst gebastelten Script](https://gitlab.com/kanthaus/openwrt-config) zu lösen, was allerdings mit anderen Problem einhergeht:

- Script muss von uns up-to-date gehalten werden mit Änderungen an OpenWRT
- Fehlende Dokumentation -> sehr hochschwellig

### Jetzt
Zentrale Konfiguration mit [OpenWISP](https://openwisp.org/whatis.html), welches für genau diesen Zweck gebaut und auch maintained wird.


## Dokumentationen updaten

Es braucht für alles Dokumentation um Wissen nicht nur in den Köpfen einzelner zu speichern. Leider war vieles der bestehenden Dokumentation nicht mehr up-to-date, was nun jetzt auch besser sein sollte

- Anleitung zum WLAN verbinden: [https://handbook.kanthaus.online/technical/wifi.html](https://handbook.kanthaus.online/technical/wifi.html)
- Ausführliche technische Beschreibungen für Admins: [https://handbook.kanthaus.online/technical/network.html](https://handbook.kanthaus.online/technical/network.html)
- Dieser Blogbeitrag

## Worte zum Schluss

I glaube auf dem Weg immer mehr verstanden zu haben, was es braucht, damit etwas einfach zu verstehen und wartbar ist, aber was für mich ein großes Rätsel bleibt ist, wie wir es schaffen können, dass es so bleibt. Es scheint, als wäre Chaos wirklich der Zustand den alles mit zunehmender Zeit anstrebt, aber was können wir dagegen tun? Vielleicht ist das hier aber schonmal ein Anfang :)