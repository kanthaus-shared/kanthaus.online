---
title: 'Cycling Without Age Wurzen'
date: "2021-05-17"
taxonomy:
    tag: [blog]
    author: Antonin
---

When I need a break from life in Kanthaus, a quick bike outing is generally very effective. Is it the fresh air? The pleasure to glide smoothly on impeccable cycle paths? The eye contact with random people on the street? I do not know exactly, but it just works. Without those journeys on the saddle, pandemic life would be pretty unbearable to me.

When cycling in Wurzen, the chances that you encounter a care home are high. [Nearly one Wurzener in three is 65 or older](https://www.demografie.sachsen.de/monitor/html/atlas.html) and the town is no exception to the aging trend. You can find at least 10 homes in Wurzen and more of them are getting built. When I cycle past them, I can see some light behind the curtains and sometimes even someone at the window. For most of the occupiers, grabbing a bike to escape the daily routine is not an option. The pandemic has forced a monastic isolation on them, on top of their mobility restrictions.

===

At the same time, we often hear in Kanthaus the desire to interact more with our surroundings. In some ways we also tend to form a bubble and the pandemic did not improve that. Our free shop was popular amenity, visited regularly by many neighbours, but it has been temporarily closed for months now. So we have been looking for new ways to build connections with the town. And that is how we decided to start a local group of Cycling Without Age.

[Cycling Without Age](https://cyclingwithoutage.org/) is a movement that started in Copenhagen in 2012 and has spread to many countries over the years.
The principle is simple: volunteers offer rides to residents of care homes on rickshaws. Those are special tricycles which can take up to two passengers, seated in front of their chauffeur.
The rides are free and for fun: the goal is just to have a nice time out. The bikes are eletrically assisted and the seating positions make it easy for rider and passengers to chat during the trip. It's that simple.

Beyond the social aspect, there are many more reasons why we are enthusiastic about this movement. Many of us have been involved, in one way or another, in cycling advocacy, in protests against the car industry or construction of new highways. But it is easy to forget that many people feel excluded from cycling, be it by fear of traffic, of their own physical condition or simply because they have never learned to ride a bike. Getting tricycles on the road is a very good way to show that not all cycles look alike. [Special bikes](https://www.specialbikesshow.com/welcome-to-the-spezi.html) exist for all sorts of purposes and not everyone needs to look like a Tour de France rider when going grocery shopping.

So we are starting [a local group of Cycling Without Age](https://radelnohnealter.de/wurzen/) (Radeln Ohne Alter in German) in Wurzen. We hope to be able to do our first rides this summer, counting on the progress of the vaccination campaign. In fact, other local groups have maintained their activity during the pandemic by developing appropriate hygiene concepts for the rides, so we are hopeful to be able to start soon. Many of us in Kanthaus are keen to ride the rickshaws but we also aim to find volunteers outside of the house, which should also be an opportunity to meet people outside our circles, and will ensure that the bikes are put to good use. If you are interested in getting involved, you can [join our Karrot group](https://karrot.world/#/groupPreview/135) where we do all our project coordination.

The first step to start our local group is to get some rickshaws and that is where we need your help! We have started [a crowdfunding campaign to fund our first rickshaw](https://betterplace.org/p94806). Our second rickshaw will be funded by a grant from the [Deutsche Postcode Lotterie](https://www.postcode-lotterie.de/projekte/). So get the word out, we need your support!

