---
title: Fahrradreparaturstation am Kanthaus
---

## Fahrradreparaturstation

![](reparaturstation_straße.jpg)

Am Haus der Kantstraße 22 findest du unsere Fahrradreparaturstation.
Hier kannst du schnelle Reparaturen an deinem Fahrrad durchführen, wenn du gerade in der Nähe bist oder dir das entsprechende Werkzeug fehlt.
Außerdem kannst du die Luft deiner Fahrradreifen kontrollieren und auffüllen.
Nicht nur Fahrräder müssen repariert werden! Fühl dich frei, das Werkzeug auch für Kinderwägen, Rollstühle oder weitere Geräte zu verwenden.

Du brauchst dringend anderes Werkzeug oder z.B. einen Ersatzschlauch? Klingel doch einfach mal bei der Kantstraße 20, vielleicht hat gerade jemand Zeit, dir weiterzuhelfen.

### Fahrrad aufhängen

Für einige Reparaturen ist es hilfreich, wenn du dein Fahrrad an die Station hängst.
Dazu kannst du den Sattel oder das Oberrohr, sofern dein Fahrrad eines hat, auf die Stangen hängen:

![](reparaturstation_in_benutzung.jpg)

### Werkzeuge

#### Luftpumpe
Auf der rechten Seite der Station befindet sich die Luftpumpe: Auf dem Manometer kannst du den aktuellen Reifendruck ablesen, solange du die + Taste drückst, strömt Luft in deinen Reifen.

1. Lies auf deinem Reifen nach, welchen Druck du einfüllen sollst. Auf der Flanke des Reifens sollte sich eine Inschrift "Inflate to 2.5-3.5 bar" oder "max. 4.0 bar" oder etwas ähnliches finden. Findest du diese Angabe nicht, so fülle nicht mehr als 3,5 bar ein!
1. Verbinde den Reifenfüller mit deinem Fahrradreifen. Es passt nur einer der beiden Ventilköpfe auf dein Reifenventil. Verriegele den Anschluss durch umlegen des Hebels wie auf dem Bild dargestellt. Wenn du deutlich Luft zischen hörst, hast du den Ventilkopf nicht gut genug aufgesteckt. Bitte versuche es nocheinmal :-)
1. Nun kannst du beim französischen und Autoventil den Druck deines Reifens direkt auf dem Manometer ablesen. Beim Dunlop ("Fahrrad") Ventil geht dies erst nach dem füllen.
1. Drücke und halte die + Taste für ein paar Sekunden und beobachte dabei das Manometer. Fülle deinen Reifen keinesfalls über den Druck, welcher auf dem Reifen angegeben ist. Wenn der Reifen schon sehr alt oder porös ist, solltest du den Druck etwas niedriger einstellen.
1. Hast du deinen Wunschdruck erreicht, so kannst du den Fülladapter wieder abnehmen. Vergiss nicht, eine eventuelle Schutzkappe wieder auf dein Ventil aufzuschrauben.

Das System ist auf einen Maximaldruck von etwa 6 bar eingestellt. Du verwendest die Reifenfüllanlage wie weiteres Werkzeug auf eigene Gefahr. Wir übernehmen keine Verantwortung für eine korrekte Eichung des Manometers oder weitere Anwendungsfehler. Wenn dein Reifen platzt (sei es durch Überdruck, Alter, Beschädigung oder sonstige Fehler), bist du selbst schuld :-)

#### Reifenheber

#### Innensechskantschlüsselsatz (Inbusschlüssel)

#### Maulschlüssel 10/12/13/15/17

#### Schraubendreher (PH1, 3mm)

#### Steckdose
Oben links befindet sich eine Steckdose. Du darfst hier gerne dein Handy, E-Bike, Laptop, ... laden.
Die Steckdose liefert 230V/16A und ist zusätzlich mit einem Fehlerstromschutzschalter auf 30 mA abgesichert.

Bitte nutze die Steckdose nicht zum Aufladen deines Elektroautos: Während wir unterstützen, auf Verbrennungsmotoren weitestgehend zu verzichten ist der Umstieg auf Elektromobilität der falsche Anreiz um nachhaltigen Klimaschutz zu betreiben. Zudem benötigt ein Elektroauto Strom für mehrere Euros pro Ladung; das würde auf Dauer unsere Kostenkalkulation für die Fahrradreparaturstation überschreiten & wir müssten das Angebot wieder einstellen.


### Geschichte & Förderung

Die Idee zur Fahrradreparaturstation kam uns bereits im Jahr 2019. Ist es nicht praktisch, wenn Werkzeug zum Verstellen vom Sattel und Aufpumpen der Reifen direkt an der Straße verfügbar ist? Ja! Lasst uns doch eine Fahrradreparaturstation bauen.

Wir haben uns hier dafür entschieden, die Station vollständig selbst zu bauen. Einerseits, weil es Spaß macht, andererseits, weil wir gewisse Anforderungen nicht bei käuflich erwerbbaren Stationen erfüllt gesehen haben. Zudem sind käufliche Stationen extrem teuer - wir wollen das gleiche Geld lieber dafür verwenden, mehr Funktionalität und eine regelmäßige, langfristige Wartung zur Verfügung zu stellen.

Leider hat sich die gesamte Projektumsetzung immer wieder verzögert, zudem sind wir bei der Umsetzung von unserem ursprünglichen Entwurf abgewichen.

Wir glauben dennoch, eine nutzbare Reparaturstation an einer praktisch gelegenen Stelle in Wurzen errichtet zu haben und freuen uns über vielfältige Nutzung.

Zudem haben wir uns für diese Reparaturstation auch von den Sächsischen Mitmach-Fonds unterstützen lassen.
Wir haben eine Förderung von 4100€ erhalten, mit der wir Material-, Betriebs-, und Wartungskosten decken.
Ferner gibt es noch die Idee, mal irgendwann einen Außenwasserhahn in der Nähe zu installieren, damit du während einer Tour ohne Aufwand deine Wasserflaschen auffüllen kannst.

Der Träger des Projekts "Fahrradreparaturstation" ist der gemeinnützige Wandel Würzen e.V..

Der Ideenwettbewerb Sächsische Mitmach-Fonds wurde von der Sächsischen Staatsregierung initiiert. Diese Maßnahme wird mitfinanziert durch Steuermittel des Freistaates Sachsen auf der Grundlage des von den Abgeordneten des Sächsischen Landtages beschlossenen Haushaltes.
![](Logo-MMF_web.png)
