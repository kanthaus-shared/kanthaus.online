---
title: Coordination Meeting
date: "2022-10-17"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #265
* Date: 2022-10-17
* Facilitator: Martin
* Notary: Tilmann
* Mika caretaker: Kita
* Levi caretaker: Janina
* Physical board caretaker: Lara
* Digital calendar: Doug
* Reservation sheet purifier: Larissa
* Present: Martin, Lara, Tilmann, Doug, Larissa, Anneke

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
- **Present:** 11.0 people/day (+0.3)
- **⚡ Electricity**
    - usage: 57.44 €/week (⬆️+12%)
    - of that for heating: ~~16.93 €/week~~ ~4.5 €/week
    - paid: -0.3 €/week 
    - ☀️ self produced: 65% (⬇️-6%)
    - emissions: 18 kg CO₂ₑ/week
- **💧 Water**
    - paid: 18.74 €/week (⬆️+32%)
    - emissions: 1.5 kg CO₂ₑ/week

### Expenditure
- [martin] 18€ for snack kitchen radiator mounting parts


### Income
- 18.50€ donations at potato action (sorted into coin jars in office)

### Things that happened
#### In or around Kanthaus
- We had a (un)critical mass/tour to Spitzberg on Saturday
- Potato action in Market platz
- Tilmann reaccepted as member
- Confirmation that the attic insulation will be done by a company in December
- NDK autumn fair was visited
- Surprise visit by Patrick with great consequences
- Janina started a dental hygiene challenge


#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* Green party dropping it's values.
* Three week old finance minister sacked in GB

## 2. This week planning

### People arriving and leaving
* **Mon.:** silvan leaves
* **Tue.:** 
* **Wed.:** 
* **Thu.:** silvan comes back, Dorata arrives
* **Fri.:** Tatjana comes for one night; Aggi, Q, Fion, Zora come for the weekend
* **Sat.:** Anja comes, Tilmanns parents come for the weekend
* **Sun.:** Anja leaves
* **Mon.:** 
* **Some day:** 

### Weather forecast

We will have a summery Monday (open all windows in the afternoon!), a rainy Tuesday and then a drop of temperature to minimum 3°C on a sunny Thursday. Weekend not so cold but potentially rainy. 
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Zui Member : Absolute Days threshold 194/180 (+8%)
- Janina Member : Absolute Days threshold 185/180 (+3%) 
Absent:
- Anja Volunteer : Absence threshold 112/90 (+24%)

Current people: Unfortunately nobody did the residency record update so far this morning :/

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * ~~Monday food care~~
    * Afternoon potato sorting @garden [Janina, you?]
* Tuesday
    * 15:00 - 17:00 Open Tuesday []
* Wednesday
    * 10:00 - 13:00 Attic cleanup, part 1! [Tilmann, Martin, you?]
* Thursday
    * 10:00 Power Hour [Fac.: Doug, DJ: ?]
    * finance day [Larissa, zui]
* Friday
    * 12:00 Market Pickup [Doug]
    * Yellow bins [Larissa]
* Saturday
* Sunday
    * Mika's bday party
* Next Monday
    * 9:00 CoMe [Tilmann]
    * Blue bins [Lara]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [tilmann] 282€ for two attic entrance doors: https://www.bauhaus.info/kellertueren/padilla-zk-element-classic-plus/p/29253012


## 4. To do

_Newest tasks on top_
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down?
* [x] remount snack kitchen radiator [Martin/Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] Personal GitLab accounts will be removed from the kanthaus organization because of a pricing change (see the #sysops channel for details). If you have local clones of some Git repositories on your machine, this will require re-configuration. I will post some instructions for that in #kanthaus.
* [Janina] Visitor requests
    * Löwenherz, today, could also just be a short visit (see mails channel for details)
        * Janina will not host, can someone mail him immediately after come about how the outcome is for today?
        * https://yunity.slack.com/files/USLACKBOT/F046QC1HCLS/re__kanthaus_kennenlernen
        * Larissa will reply to Löwenherz
    * Jeffrey, workawayer, enthusiastic about permaculture, communal living and traveler for 10 years
        * Janina would decline if nobody wants to take over
        * https://yunity.slack.com/files/USLACKBOT/F046MP744TX/re___2__good_evening__via_workaway.info_
        * nobody up for it right now
    * Side note: I'm happy to push forward a collective agreements change like e.g. "To try and achieve a more equal gender balance in the long run we won't accept open-ended visitor requests of cis-men until April 2023."
* [zui] can we do CoMe next week an hour earlier? so i can schedule my evaluation and propose this monthly dicision making/schedule session try out (we talked about in social sauna)?
    * let's do it!
* [doug] let's use the sleeping places sheet for the weekend!

<!-- check if anyone has a point that didn't speak already -->
 
### Round 2
* [Antonin] [future of the KMW](https://ukuvota.world/#/app/30039c39-9840-301f-f0b0-d70b45d9030f/vote): voting phase has started
* tbr? [Janina] Potato storage in K18?
    * would be cool to know before sorting today, if it's okay to store them there :)
* [larissa] would be good to organize cooking on the weekend!

<!-- check if anyone has a point that didn't speak already -->

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa
* **Tue.:** Anneke
* **Wed.:** 
* **Thu.:** zui
* **Fri.:** 
* **Open Tuesday:**

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Doug
* **Open Tuesday:** 
* **Week:** Tilmann

### Result

- **Mon.:** Larissa
- **Tue.:** Anneke
- **Wed.:** Doug
- **Thu.:** zui
- **Fri.:** Martin
- **Open Tuesday:** Lara

## 7. For next week
* 
