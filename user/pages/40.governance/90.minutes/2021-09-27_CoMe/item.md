---
title: Coordination Meeting
date: "2021-09-27"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #211
- Date: 2021-09-27
- Facilitator: Janina
- Notary: chara
- Mika caretaker: Tilmann, Maxime
- Levi caretaker: Janina
- Physical board caretaker: Matthias, chandi
- Digital calendar: chandi
- Reservation sheet purifier: Matthias
- Present: Matthias, Tilmann, Mika, Maxime, Thore, chandi, Clara, Janina

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_6096b2a3491514e9dbe05f4be4555a7f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.9 people/day (-5.1)
- **⚡ Electricity**
    - usage: 7.06 €/day (⬆️+5%)
    - paid: 0.12 €/day
    - ☀️ self produced: 65% (⬆️+2%)
    - emissions: 16 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.9 €/day (⬇️-14%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure

- 10€ sponges, toilet paper, washing liquid
- [matthias] about 1510€ for heating system connection stuff

### Income
- 11.50€ Pfand

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)

- The Collective Agreements were changed: No more framework, but we require people to be vaccinated, tested or cured.
- This section will be removed after this CoMe, new version of Collective Agreements will be uploaded as well

### Things that happened
#### In and around Kanthaus
- All sheets were removed in a big disinfection action
- A warmshowers person spent a night
- Antonin is now called 'Anna' (by Mika)
- The house was really empty over the weekend
- Puppet theatre at Villa Klug
- A trip to the lake
- Antonin moved into his new WG
- Tilmann and Janina made cream cheese from buttermilk and more experiments are to follow
- The new washing machine is making some weird noises and Matthias is investigating later
- Twelve boxes of potatoes were saved

#### In the wider world
- There was an election in Germany - the result is bad.
- The next big climate strike took place.

## 2. This week planning

### People arriving and leaving
- **Mon.:** kiki S. arrives, Antonin left
- **Tue.:** kiki S. leaves
- **Wed.:** Max and friend arrive, chandi leaves
- **Thu.:**
- **Fri.:** Max and friend leave, zui comes back, larissa + silvan come back, clara leaves
- **Sat.:**
- **Sun.:**
- **Mon.:** chandi comes back
- **Some day:**


### Weather forecast

<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Some rain in the beginning of the week, warm stable autumn weather for the whole week.

### Evaluations and check-ins

- Clara Volunteer : Absolute Days threshold 87/60 (+45%)
- Silvan Volunteer : Absolute Days threshold 67/60 (+12%)
- Janina Member : Absolute Days threshold 193/180 (+7%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * Paper [Antonin]
    * 10:00 CoMe
    * Park cars on even side [Matthias]
- Tuesday
    * rest waste [Tilmann]
    * Open Tuesday
    * 16:00 Clara's evaluation [Tilmann]
- Wednesday
    * Park cars on odd side [Matthias]
- Thursday
    * 10:00 Power Hour [Fac.: Janina]
    * 15:00 Sharing event [?]
- Friday
    * bio waste [Matthias]
    * 12:00 Market Pickup [Andrea]
    * 14:30 Janina's evaluation [Matthias]
    * 19:30 Film about "Ella" in our garden (maybe external visitors come) [Clara, Matthias]
- Saturday
- Sunday

- Next Monday
    * 10:00 CoMe [Janina]
- Next week summary
    - Social sauna
    - Monthly coordination meeting (probably not?!)

- Weeks until Roof month: 2.5!

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
- [tilmann/matthias] 3000-5000€ ventilation parts for attic, K20-2, K22-2 and K20-1
    - ~1000-2000€ for second-hand ventilation device with heat exchanger
    - ~2000-3000€ for air pipes, connectors, valves, fire protection devices for 3 floors + Attic
        - more details in https://pad.kanthaus.online/2021-ventilation-planning#Materialplanung
    - doesn't need to be bought this week, but rather sooner than later

## 4. To do
_Small fixes of things in the house_

### High priority
* [x] secure wash kitchen sink [Antonin]
* [ ] secure k22-2 bathroom sink

### Medium priority
* [ ] More storage spaces for visitors
* [ ] make k20 garden door easily closable from outside (or acquire a new door) -> apparently old door handle in workshop on left https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] fix loose table leg in Lantern
* [ ] empty dry food storage shelf (find new places for the stuff)
* Ventilation preparation:
    * [ ] remove ventilation pipes in K22-2 bathroom
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen - new solution
* [ ] bed in private is dissolving and making many small crumbs
* [ ] reinforce hook for laundry rack K22-1.5

## 5. Discussion & Announcements

### Round 1
- [Antonin] can we have a real clock in the dining room again please? The new one is not very readable.
    - [Janina] yes, pleeeease! ++
    - Maxime and Andrea are to take care..?
- [Janina] Collective Agreements change about language: Voting phase ends tonight!
- [Matthias] Heatpump update: Delivery problem but no clear info. will figure things out and keep you updated.

<!-- Anybody who hasn't spoken so far wants to add a point? -->

### Round 2
- [Janina] New sheet in dining room where you can indicate when you cleared the table. It's meant to show who does it when, because I was quite annoyed some days ago and got the impression that it was always me. Now I already felt an improvement and saw that other people also do it and that I also sometimes forget to tidy up behind myself. Yay, social peace! :grin: :thumbsup:
    - [Matthias] I find this quite motivational!
    - [chandi] I dont really like the direction this could lead to. If people only do visible tasks it would be a problem. Having sheets everywhere for every tiny thing is not the solution. But I get the point and I'm happy that you're happy!
    - [Janina] It's probably temporary anyway... ;)
- [Tilmann] would appreciate more child care again, ideally before and after nap time (12-14, 16-18). I feel I need at least two days a week where I can focus on other topics than children

<!-- Anybody who hasn't spoken so far wants to add a point? -->

### Round 3
- [Janina] Disinfection action: Some tasks are not fully done. Please refer to [this list](https://pad.kanthaus.online/2021-09-26_hygiene_cleaning) to see what still needs to be done. Also, a lot of beds are not yet covered again and there's still a huge pile of laundry to be done. Thanks for your contributions! :)
- [Tilmann] more potatoes? Last year we had way more and if we want them to last for the whole winter we need more.
    - [Thore] I can call him again then it should be easy.
    - [Janina] Maybe we need to clear some boxes before we can get more potatoes.
    - Up for going: Tilmann, Janina, Matthias, Clara? - maybe during Clara's evaluation
    - Thore will call after CoMe and ask about Tuesday afternoon


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** clara
- **Tue.:** Andrea
- **Wed.:** Thore
- **Thu.:**
- **Fri.:**
- **Open Tuesday:** Anja, Janina

### Unavailabilities
- **Mon.:**
- **Tue.:**
- **Wed.:** chandi
- **Thu.:** Maxime, chandi
- **Fri.:** chandi
- **Open Tuesday:** Maxime, chandi, Matthias
- **Week:** Tilmann

### End result
- **Mon.:** Clara, chandi
- **Tue.:** Andrea
- **Wed.:** Thore
- **Thu.:** Matthias
- **Fri.:** Maxime
- **Open Tuesday:** Anja, Janina

## 7. For next week
