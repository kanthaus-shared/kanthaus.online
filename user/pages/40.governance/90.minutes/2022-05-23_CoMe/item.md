---
title: Coordination Meeting
date: "2022-05-23"
taxonomy:
    tag: [come]
---

# CoMe #245
* Date: 2022-05-23
* Facilitator: Antonin
* Notary: kito
* Mika caretaker: Kita
* Levi caretaker: Janina
* Physical board caretaker: Louis
* Digital calendar: Maxime
* Reservation sheet purifier: Martin
* Present: Silvan, Larissa, Martin, Vroni, Hue, Andrea, Clara, Maxime, Janina, Louis, Antonin, chandi, kito

----

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/1b857e3e-87eb-46dd-bb3a-e8017ec36e43.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 18.7 people/day (-3.6)
- **⚡ Electricity**
    - usage: 55.73 €/week (0%)
    - of that for heating: 6.63 €/week
    - paid: -45.17 €/week 
    - ☀️ self produced: 78% (⬇️-3%)
    - emissions: 14 kg CO₂ₑ/week
- **💧 Water**
    - paid: 38.37 €/week (⬇️-7%)
    - emissions: 3 kg CO₂ₑ/week

### Expenditure
* [matthias] 53,99€ for Tools for the bike repair station (via Wandel Würzen)
* [kito] 18€ for oil
* [Martin] 11€ for toilet paper

### Income
* 90€ in the shoe

### Things that happened

#### In or around Kanthaus
- building  weeks ended
- people went to the lake
- we had a kitchen party!
- a lot of amazing food got dumpster dived
- critical mass and foodsharing brunch
- amazing sushi party
- some people went to NDK to support their buildingday
- mosaik in the bathroom started
- IDAHIT!


#### Wider world
- the climat-change-negationist australian conservatives are finally out of government! :tada:
- heavy thunderstorms in Germany
- factory in Dehnitz burned

## 2. This week planning

### People arriving and leaving
* **Mon.:** Anja, zui comes
* **Tue.:** Nathalie and Thore arrive, Louis leaves
* **Wed.:** Mulli arrives, Natascha arrives
* **Thu.:** Elvyn comes (stays between some hours and 2 nights)
* **Fri.:** Mulli leaves, Franzi arrives, Janka arrives
* **Sat.:** Natascha leaves
* **Sun.:** Franzi leaves, Vroni leaves
* **Mon.:** Louis comes back
* **Some day:** 

### Weather forecast

- quite nice temperatures around 20-24 degrees, sunny overall

### Evaluations and check-ins

- Anja Volunteer : Absolute Days threshold 84/60 (+40%)
- Andrea Volunteer : Absolute Days threshold 83/60 (+38%)
- Hue_WA Visitor : Days Visited threshold 22/21 (+5%)
- Doug Member : Absolute Days threshold 188/180 (+4%)
- Matthias Volunteer : Absolute Days threshold 61/60 (+2%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
    * Park KMW on even/this side [Louis]

* Tuesday
    * General waste is collected [Tilmann]
    * 11:00 Louis' evaluation [Antonin]
    * 15:00 - 17:00 Freeshop [Anja,]
* Wednesday
    * Park KMW & trailer on odd/that side [Maxime]
    * 11:00 Martin's evaluation [Andrea]
    * 18:00 Punkrocktresen @D5
    * 
* Thursday
    * 10:00 Power Hour [Fac.: Tilmann, DJ: ]
    * 15:00 social souna [Larissa]
* Friday
    * 12:00 Market pickup [kito]
    * 16:30 AG PSW meeting [zui, Janina + Leipzig FLINTA*\s]
* Saturday
    * Biotonne is collected [Tilmann]
    * 10:00 Building Support @ NDK [kito, chandi]
    * 12:00 Kanthaus Gartenfest :tada:
* Sunday

* Next Monday
    * 10:00 CoMe [Silvan]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Silvan] 24€ for book "Gemeinschaftskompass" https://www.gemeinschaftskompass.de/de/gemeinschaftskompass-eine-orientierungshilfe-fuer-gemeinschaften/
* [Larissa] maybe grout (Fugenmörtel) if we don't have some


## 4. To do
_Newest tasks on top_
* [ ] mount fire extinguisher visible in the staricases K20-1 and K22-1
* [ ] sort first aid stuff, make two sets and mount that also in the staircase [chandi, Martin]
* [ ] repair right bike shed light
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator [kito]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
- [janina] building weeks are over but we still are quite a lot of people. I propose to keep the food tasks sheet in use but put less emphathis on lunch and more on cleanup, food sorting and dumpster washing.
- [chandi] let's have a subgroup thinking/deciding on the future of the kanthaus cloud. who wants to be in? :)
    - Antonin and kito are interested, Zui also
- [kito] Wahlforum by the LVZ, June 2nd, need to register, probably Schweizergartenstr.
- [Silvan] someone takes care of election cards of people who are not here?
    - people were mentioned so they should know

<!-- check if anyone has a point that didn't speak already -->


### Round 2
- [Doug] Please clearly mark towels: I will move unmarked immediately to washing.


### Round 3


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Louis
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** kito, Larissa, Maxime, Vroni, chandi
* **Tue.:** kito, Antonin, chandi, Vroni
* **Wed.:** Larissa, Antonin, Silvan
* **Thu.:** Antonin, Janina, Maxime
* **Fri.:** Larissa, Janina
* **Open Tuesday:** Larissa, Maxime, Silvan, chandi, Vroni
* **Week:** Andrea

### Result
- **Mon.:** Louis, Silvan
- **Tue.:** Hue, Janina
- **Wed.:** chandi, Vroni
- **Thu.:** Martin, kito
- **Fri.:** Antonin, Maxime
- **Open Tuesday:** anja, Clara



## 7. For next week
* 
