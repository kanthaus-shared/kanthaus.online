---
title: Coordination Meeting
date: "2022-02-21"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #232
* Date: 2022-02-21
* Facilitator: Antonin
* Notary: Janina
* Mika caretaker: Clara 
* Levi caretaker: Tilmann
* Alex caretaker: Vince
* Physical board caretaker: Andrea
* Digital calendar: chandi
* Reservation sheet purifier: Matthias
* Present: Larissa, Silvan, Matthias, Vince, Cille, Chandi, Maxime, Andrea, Nathalie, Janina, Antonin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf4817.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 17.9 people/day (+4.1)
- **⚡ Electricity**
    - usage: 113.01 €/week (⬇️-2%)
    - of that for heating: 53.39 €/week
    - paid: 69.42 €/week 
    - ☀️ self produced: 33% (⬆️+8%)
    - emissions: 63 kg CO₂ₑ/week
- **💧 Water**
    - paid: ? €/week (0%)
    - emissions: ? kg CO₂ₑ/week

### Expenditure
- [silvan] 16,50€ stamps
- [matthias] 137,45€ for ventilation filters
- [matthias] 81,28€ for some more hot water installation parts
- [chandi] 250€ for 50m NYM-5x10 Cable for the attics (see https://yunity.slack.com/archives/C3RS56Z38/p1645178073888099)
- [antonin] 20€ for electrical stuff for the silent office
- [tilmann] 160€ for K20-0 ventilation parts (+ some taken that were planned for K22-1)
- [nathalie, chandi] 10€ bauschutt disposal

### Income
- 10€ (shoe)
- 25€ donation

### Things that happened

#### In or around Kanthaus
- ToI after day with Andrea's movies screened
- Acrolotl
- trip to Trebsen and Altenhain
- the washroom is usable again
- stormy critical mass
- foodsharing wurzen meeting
- a gaming afternoon
- 

#### Wider world
- big storm 'Nora' all over Erupe
- maybe there's a war starting East of here (no..)
- the Munich security conference

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Clara leaves
* **Wed.:** Lise arrives, änski arrives
* **Thu.:** zui arrives, Anna and Julia come (or tomorrow)
* **Fri.:** 
* **Sat.:** zui leaves
* **Sun.:** Clara (probably) comes
* **Mon.:** Vince's mum arrives
* **Some day:** Bodhi for the finance meeting, 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_storm, sun, rain, clouds. and colder._

### Evaluations and check-ins

- Andrea Volunteer : Absolute Days threshold 82/60 (+37%)
- Tilmann Member : Absolute Days threshold 206/180 (+14%)
- Silvan Volunteer : Absolute Days threshold 62/60 (+3%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
    * 15:00 foodsharing Wurzen co-working @office [Janina, Nathalie]
    * 17:00 Radeln ohne Alter Wurzen meeting @elephant room [Nathalie, Antonin]
    * Park cars on even/this side [done]
* Tuesday
    * 11:00 Tilmann's evaluation @cloud room [Antonin]
    * 18:00 Workout @yoga room [SiLa]
* Wednesday
    * 11:30 Silvan garden stuff tour (see discussions)
    * 14:00 ToI team meeting @fansipan
    * evening: Park cars on odd/that side [Matthias]
* Thursday
    * 10:00 Power Hour [Fac.: Matthias, DJ: ]
    * 13:00 Finance session [Larissa, chandi, Zui, Bodhi]
* Friday
    * Yellow bin is collected [Larissa]
    * 11:00 Silvan's evaluation [Janina]
    * after dinner: Sing along session [Nathalie]
* Saturday
* Sunday
* Next Monday
    * 10:00 CoMe [Matthias]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [chandi] 60€ for 50 corona rapid tests: https://hygiene-gesellschaft.de/schnelltests/green-spring-profi-sars-cov-2-antigen-schnelltest-kolloidales-gold-4in1-profitest-25-stueck
	- we approx used ~30 tests/month Nov-Feb
	- they are way better with low viral load, which is the normal case amongst vaccinated ppl (40% sensitivity @ Cq≥30, https://schnelltesttest.de/result/AT417%2F20/details)
	- for transparency: i've already ordered them, but could also use them for upcoming events if we decide against

## 4. To do
_Newest tasks on top_
* [ ] mount sink in washroom - not yet!
* [ ] analog week plan needs a repair
* [ ] repair WiFi in the Hipster
* [ ] repair Schaukasten 
* [ ] repair hat/gloves/scarves cupboard in hallway of K20
* [ ] Clean K18 garden (done by wind :P )
* [ ] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Install light in the hallway of the K20-1 flat
* [ ] Install light in the hallway of the K20-2 flat

### more advanced tasks
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Matthias] Washroom progress
    * [Matthias] Could we get 2-3x 2-4 hours of consecutive child care in the next days to allow tilmann to finish dumpster kitchen ventilation stuff + hot water installation?
    * [Matthias] Dumpster kitchen hot water: Any near future plans to change bathtub/sink? Please see slack (https://yunity.slack.com/archives/C3RS56Z38/p1645360703569209)
* [Janina] The fairteiler in Wenceslaigasse has been neglected - let's change that! This week we want to go check it again every day and use the [store on foodsharing.de](https://foodsharing.de/?page=fsbetrieb&id=37756) to coordinate this. If you want to help and don't have a foodsharing account, just talk to me, Nathalie, Kito or other foodsharing people. :)
- [chandi] room for our finance days (Th & Fr): dragon room?
    - the black table can be cleared and Andrea is also willing to compress a bit more on her claimed desk
    - dragon room will be used
- [silvan] On wednesday 11:30 i would like to carry the stuff from my garden to the garage. Would like to use KMW and would need help of people please. (maybe about max 2h)
- [zui] the excursion from Leipzig with Michael Stellmacher can happen between last Week of March till end of April. Do we have any preferences or no-goes for the date? otherwise I would probably go for mid of April.
    - Nothing planned yet, we're happy for Zui to figure it out.
    - please link context (mails, etc)
- [maxime] salami 🐖🪦
    - no resistance to progressively trashing it 🥳🎉
- [Silvan] Glögi also needs to be consumed faster than it is at the current stage, otherwise it will go bad!

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Janina] Lunch team relaunch! Idea: Target time is 1pm, what's left on the table after 2pm is free for all. We make a list of this week's lunch team and put it up somewhere in the kitchen, together with the table of who cooks when.
    - People for this week: Janina, Tilmann, Larissa, Matthias, Cille, Chandi, (Mika, Levi)

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** chandi
* **Thu.:** 
* **Fri.:** Silvan
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** änski, Nathalie, Larissa
* **Tue.:** änski, Antonin, Larissa, Nathalie
* **Wed.:** änski, Antonin
* **Thu.:** änski, Antonin, Larissa
* **Fri.:** Larissa, Cille
* **Open Tuesday:** Maxime, Nathalie, Larissa, Antonin, Cille, änski
* **Week:** Andrea, Janina, Tilmann, Matthias

### Result
- **Mon.:** Antonin, Cille
- **Tue.:** Maxime
- **Wed.:** Chandi, Larissa
- **Thu.:** Nathalie
- **Fri.:** Silvan, änski
- **Open Tuesday:** Vince(?)
