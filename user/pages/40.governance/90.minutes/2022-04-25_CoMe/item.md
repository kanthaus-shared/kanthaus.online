---
title: Coordination Meeting
date: "2022-04-25"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #241
* Date: 2022-04-25
* Facilitator: chandi
* Notary: Andrea
* Mika caretaker: janina
* Levi caretaker: janina
* Physical board caretaker: 
* Digital calendar:  zui
* Reservation sheet purifier: 
* Present: Larissa, Chandi, Nathalie, Silvan, Andrea, Zui, Matthias

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf483d.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.6 people/day (-1.9)
- **⚡ Electricity**
    - usage: 35.46 €/week (⬇️-6%)
    - of that for heating: 37.41 €/week
    - paid: -20.53 €/week 
    - ☀️ self produced: 55% (⬆️+5%)
    - emissions: 13 kg CO₂ₑ/week


### Expenditure
* [chandi+matthias+zui] the infection costed us 120€ this week :)
    * 40€ for 25 really good tests ("longsee")
    * 60€ for 50 good tests ("green spring")
    * 20€ for pizza 😄 ???
* [antonin] 100€ for electricals for the silent office

### Income
* nothing in the shoe

### Things that happened

#### In or around Kanthaus
* Covid party!
* Bought Pizza party!
* Meeting with someone from die Linke
#### Wider world
* war
* france: no Le Pen!

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** maxime probably comes back, chandi leaves
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Clara_WA probably comes, chandi comes back
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Okayish weather with 15º degrees during the day and light sun and only today with rain.

### Evaluations and check-ins
- *Thore* _Volunteer_ : _Absolute Days_ threshold 104/60 (+73%)
- *Larissa* _Member_ : _Absolute Days_ threshold 216/180 (+20%)
- *Nathalie* _Volunteer_ : _Absolute Days_ threshold 66/60 (+10%)

:clock1130: Due for evaluation soon (in the next 7 days, and seen within the last 7 days):

- *Silvan* _Volunteer_ : 1 days until _Absolute Days_ threshold (60)
- *Andrea* _Volunteer_ : 5 days until _Absolute Days_ threshold (60)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [chandi]
    * Park ~~KMW & ~~trailer on even/that side [done]
* Tuesday
    * rest waste [Larissa]
* Wednesday
    * 10:00 building weeks meeting 
    * Park KMW & trailer on odd/other side [Silvan]
* Thursday
    * 10:00 Power Hour [Fac.: Co-Facilitation Andrea Zui, DJ: ?]
* Friday 
    * bio waste [Larissa]
    * 10:00 Attic working session and skillshare [Tilmann]
    * 14:00 social sauna in the garden [Zui]
* Saturday
    * 15:00 Foodsharing Wurzen Info Meeting
* Sunday
    * 14:00 Cycle training session (meet in front of KH) [Antonin]
* Next Monday
    * 10:00 CoMe [chandi]
* Next week summary
    * 
 
_To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do

_Newest tasks on top_
* [ ] make the wall nice in K20-2 bathroom where the boiler has been
* [ ] remove old gas heater from basement to trash corner - needs 3-4 people 
* [ ] repair Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
- [zui] switching power hour and social sauna this week? otherwise I can't participate in social sauna (same in two month) because I have a apointment in leipzig.
- Would not like to pospone it and not do the social structure of KH. Maybe we could do it online. 
    - [Larissa] Online doesn't seem attractive, but in garden?
    - [Nathalie] How are the quarentine rules for each person?
    - [Chandi] We can make it on Friday.
- [Larissa] mask-free areas for negative people?
    - Dining room and office would be areas for negative people. The question is if it would be possible to avoid the children from going around. Maybe blocking the dining room so the negative poeple can use would be a nice idea. Ask Janina and Tilmann about the children. Another question: how is the usage of the main bathroom? 
    - [Chandi] It would be nice to hace communal spaces for people without corona. 
    - [Zui] Having a place to eat on a table would be nice. 
    - [Matthias] Janina is today alone with the kids so it would be nice to offer support so that it is easy for the kids to stay in the 22. 
    - [Nathalie] Cooking and providing this everyday care might help.
    - [Larissa] The question is how to make difficult for the kinder to access the dining room.
    - [Chandi] The idea: dining room and office mask free for the negative people and thinking about ways to block the rooms. Also make sure that some equipment from the snack kitchen is available in the 22.

<!-- check if anyone has a point that didn't speak already -->

### Round 2
*  [zui] KMW on the 13.-14.05.(friday afternoon and saturday morning in the bilding weeks) to bring something to leipzig (for the FLINTA* Technik Treff) and pick something up (very big musik box) for KH
    *  no resistance!
* [Silvan] how do we do it this week with the food tasks? It would be cool to have communal dinner. 
    * [Chandi] We could do the task lottery without lottery, people volunteering for the food. People not here can volunteer to help :)

### Round 3
* [Zui] We have finance week (Steuererklärung) and we will be busy, so we won't be able to take care a lot of the corona people. 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
* Monday:
* Tuesday: Silvan
* Wednesday: chandi
* Thursday:
* Friday (after 6.15pm): Matthias, zui
* Saturday:
* Sunday: 

## 7. For next week
* 
