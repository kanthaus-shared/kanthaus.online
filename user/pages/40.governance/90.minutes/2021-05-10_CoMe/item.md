---
title: Coordination Meeting
date: "2021-05-03"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #190

- Date: 2021-05-10
- Facilitator: Clara
- Notary: Matthias
- Mika caretaker: Lise
- Physical board caretaker: Maxime
- Digital calendar: Chandi
- Reservation sheet purifier: Zui
- Present: Clara, Matthias, chandi, Zui, Nathalie, Thore, Andrea, Doug, Talita, Antonin, Vince, Janina, Maxime, Tilmann, Bodhi

----

<!-- Minute of silence (?) -->

### 0. Check-in round

### 1. Last week review
##### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_af31bc47e3433ef490c537d6b4d9a6d4.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 17.0 people/day (-1.3)
- **⚡ Electricity**
    * usage: 7.6 €/day (⬇️-30%)
    * paid: -3.36 €/day 
    * ☀️ self produced: 74% (⬆️+2%)
    * emissions: 6 kg CO₂ₑ/week

- **💧 Water**
    * paid: 3.76 €/day (⬇️-13%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 1.94 €/day (⬇️-23%)
    * emissions: 46 kg CO₂ₑ/week



##### Expenditure
- [matthias] 90€ for KMW battery + clamps
- [antonin] 98.56€ worth of bike parts

##### Income
- no income

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 3799
- µCOVIDs used last week: 4084 (without Vince's arrival)
- µCOVIDS balance from last week: -285
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 4715
- µCOVIDs/person mean for this week: 277 per person (17 people) 
- 7-days incidence rate: 172.8 (going down)
- the curfew is still in place

##### Things that happened
- we had a summer barbecue in the garden with a pool
- we had a knowledge sharing session on hedgedoc
- a dreaming session on other perspectives on houses happened
- another kritma (kritische männlichkeit) session happened
- a second roof window was half-installed
- kito and andrea had their evaluations
- there's a food sharing point in action and it is getting used
- RoAW got some funding
- Multiple gemök meetings happened
- ChaLaZu were on a road trip and got larissa's stuff

### 2. This week planning

##### People arriving and leaving
- **Mon.:** Larissa, Silvan come back
- **Tue.:** 
- **Wed.:** 
- **Thu.:** JaTiMiLe leave
- **Fri.:** 
- **Sat.:** 
- **Sun.:** Larissa leaves
- **Mon.:** 
- **Some day:** Thu or Fri Bodhi leaves

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

Very warm today and tomorrow, after that still warmer nights and 15-18 degrees during daytime; a bit of rain here and there but generally nice!


##### Evaluations and check-ins
- *Lise* _Volunteer_ : _Absolute Days_ threshold 98/60 (+63%)
- *Bodhi* _Volunteer_ : _Absolute Days_ threshold 75/60 (+25%)
- *Talita* _Volunteer_ : _Absolute Days_ threshold 66/60 (+10%)
- *Doug* _Member_ : _Absolute Days_ threshold 191/180 (+6%)
- *Zui* _Member_ : 1 days until _Absolute Days_ threshold (180)


##### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - Paper trash
    - 10:00 CoMe [Antonin]
    - 15:00 Sharing Event [unfacilitated] 
    - 19:00 Association meetings [Janina, Matthias]
- Tuesday
    - Hausmüll
    - 11:00 Bodhi Evaluation [Doug]
    - 14:30 BuFDi Meeting [Bodhi, Lise, Doug, Janina]
- Wednesday
    - 10:00 Monthly teams meeting [Matthias]
    - 12:00 zui evaluation [Maxime]
    - 18:00 Foodsharing Wurzen meeting
- Thursday
    - 10:00 Power Hour [Fac.: Chandi, DJ: Doug]
    - 14:00 Knowledge Sharing: Music theory: Which chords sound nice together? [Clara]
        <!-- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
    - 15:00 Social Sauna [Fac: Antonin]
    - ~~20:00 Evening Activity~~: ? [?]
- Friday
    - 08:30 roof day breakfast [Thore]
    - 09:30 roof day check-in [chandi]
    - 10:00 roof work start
- Saturday
    - Biotonne
- Sunday
- Next Monday
    - 10:00 CoMe [Doug]
- Next week summary
  

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)


### 3. Shopping plans
- [matthias] Getting air quality sensors for about 60€ from china to have a particle / volatile organic compounds monitoring device https://yunity.slack.com/archives/CFDNQL9C6/p1620046804006700 
- [chandi] 35€ now + optionally 110€ sometime later for an EV charging station
  - matthias is mounting the bike station soon including holes in the wall. If there are already holes, I would like to put a suitable cable in there as well, which would allow us to easily extend the bike station in the future with a plug for charging electric cars.
  - I would buy for now only the cable (35€)
  - Quite some needs for discussion, question of use; suggested to have a session if this should happen. Chandi will schedule a session if he wants to go forward.
- [Bodhi] Dishwashing detergent, FFP2 masks, frying oil

### 4. Discussion & Announcements

#### Round 1
- [clara] Can we hold the book project meeting in Kanthaus from 30th of May till the 1st of June? Tobi, Jörg and Jutta would come. They would do tests when they arrive.
  - Support.
- [matthias] there was these corona framework update votes... And now?
  - Seems that testing proposals have been too complicated; Have a session next week if somebody is willing to have one. Thore pings Larissa, also about how to continue with the vaccination proposals.
  - [matthias] can be integrated about his proposals if wanted. (???) 
- [Doug] roof: what's left to do?
  - [matthias] Is responsibility/schedule for scaffolding tear down before 31st of may clear?
     - [antonin] feels responsible for that and plans to schedule an action in the coming weeks (Thumbs Up!)
   - Two windows partially build in, one remaining
   - Wall making progress but not related to scaffolding
   - Schneefang still missing
   - Some rainwater work to be Done (tilmann?)
   - some Fassadenwork
   - Mount missing solar panels after roof windows are done; get more Sturmklammern
- [Bodhi] Turn off heating?!
  - Seems like no concerns.
- [Maxime] We reached a season where bins are rotting. Keep that in mind and get more fresh food :-)
  - [doug] also our waste bins are rotting. Put the lid on, empty them often.


#### Round 2
- [Bodhi] There is milk in the freezer. Please use it when the fresh milk is used up :-)
    - [Doug] Please drink the fresh cow milk, it doesn't last long!
- [Thore] On Tuesday there is a Leipzig pickup, if somebody wants something
- [Thore] On Friday or saturday we pickup some furniture in Bennewitz. See https://yunity.slack.com/archives/CT0K2AY87/p1620491717000300?thread_ts=1620379197.019400&cid=CT0K2AY87
- [tilmann] wood shed cleanup (maybe before Silvan & Thore get more?)
  - Thore feels responsible and would not get more wood if it it not clear how to store it.
- [zui] roof window action today & tomorrow. 1 person could help, but not neccessary. Antonin might join.
- [Maxime] There is boxes of food to be washed!

#### Round 3


### 5. [Task lottery food planning](https://kanthaus.gitlab.io/task-lottery/)

#### Volunteers
- **Mon.:** Clara, Vince
- **Tue.:** Bodhi, Maxime
- **Wed.:** Talita, Andrea
- **Thu.:** Zui, Matthias
- **Fri.:** Doug, Thore
- **~~Open Tuesday~~:** 

#### Unavailabilities
- **Mon.:** chandi, Thore, Matthias,
- **Tue.:** Doug, Nathalie, Thore
- **Wed.:** Nathalie, Thore, Janina, Matthias
- **Thu.:** 
- **Fri.:** 
- **~~Open Tuesday~~:** 
- **Week:** Tilmann, Janina


### 6. For next week
- 
