---
title: Coordination Meeting
date: "2022-08-15"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #256
* Date: 2022-08-15
* Facilitator: Janina
* Notary: Antonin
* Mika caretaker: Kita
* Levi caretaker: Tilmann
* Physical board caretaker: Kito
* Digital calendar: Maxime
* Reservation sheet purifier: Nathalie
* Present: Janina, Maxime, Matthias, Kito, Eric, Antonin, Nathalie, Helen, Martin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/44b6fa9f-2c75-4777-986c-86c0032c6b4b.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.6 people/day (+4.6)
- **⚡ Electricity**
    - usage: 71.56 €/week (⏫+85%)
    - of that for heating: 2.74 €/week
    - paid: -39.23 €/week
    - ☀️ self produced: 87% (⬆️+3%)
    - emissions: 14 kg CO₂ₑ/week
- **💧 Water**
    - paid: 15.72 €/week (⬆️+32%)
    - emissions: 1.2 kg CO₂ₑ/week

### Expenditure
* [janina] 15€ pasta and oil
* [antonin] 10€ dishwasher tablets and balsamic vinegar
* [maxime] 12€ mooore oil

### Income
* 515€ donations
* 300€

### Things that happened

#### In or around Kanthaus
* A lot of tomatoes were picked up, cooked and preserved!
* There was a bicycle repair workshop at Kulturcafe
* There was a K-pop session at Kulturcafe
* we had project updates!
* four visitors were interested in KH
* JaTiMiLe visited [Luftschlosserei](https://luftschlosserei.org)

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* see https://de.wikipedia.org/wiki/Wikipedia:Hauptseite

## 2. This week planning

### People arriving and leaving
* **Mon.:** Clara arrives, Martin is back
* **Tue.:** Helen, Matthias, Clara leave, Patri and Sento arrive (Andrea's friends)
* **Wed.:** JaTiMiLe leave for a month
* **Thu.:** Martin, Patri and Sento leave (estimated)
* **Fri.:** Thore leaves
* **Sat.:** Nathalie leaves, Matthias arrives
* **Sun.:** Thore returns
* **Mon.:** Martin returns (maybe), Matthias leaves
* **Some day:** Bodhi?, Antonin leaves, Doug will pass by

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins
- Eric_WA Visitor : Days Visited threshold 35/21 (+67%)
- Kito Volunteer : Absolute Days threshold 94/60 (+57%)
- Nathalie Volunteer : Absolute Days threshold 73/60 (+22%)
- Thore Volunteer : Absolute Days threshold 69/60 (+15%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
    * ~~Park KMW & trailer on even/this side []~~
* Tuesday
    * Rest waste [Eric]
    * 10:00 Eric's evaluation [Nathalie]
    * 15:00 - 17:00 Open Tuesday []
* Wednesday
    * ~~Park KMW & trailer on odd/that side []~~
    * 14:00 Hip-hop session @ Kulturcafe
* Thursday
    * 10:00 Power Hour [Fac.: Eric, DJ: ?]
    * 15:00 Social sauna [Maxime]
* Friday
    * Organic waste [Maxime]
    * 12:00 Market pickup [Eric]
* Saturday
    *
* Sunday
    *
* Next Monday
    * Highfield festival saving action
    * 10:00 CoMe []
* Next week summary
    *

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Janina] is toying with the idea with buying pasta

## 4. To do
_Newest tasks on top_
* [ ] change the light bulb (100W; E27) in the special bike shed to a 4-10W LED (available in Workshop in "Bulb" box) []
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
    * [Antonin] I had a look, the sink does not seem blocked to me - did someone fix it already?
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put on and off?
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [maxime] pushing the KH positions/evaluation system reform: [please read and comment the draft](https://pad.kanthaus.online/positions-system-reform)
    * [kito] there is a nice overview by Doug about how the current system was designed and why certain choices were made
    * [janina] but coordinating with Doug was a bit of a blocker to make changes during those previous attempts, so maybe just go for it
* [anja, copied from slack by maxime, tbr?]
	> So the KMW is almost full with Highfield equipment.
	> Can someone do me the following favours:
	> - get a Biertischgarnitur from NDK (2 benches, 1 table; according to @janina we can just get it from the front) [Kito]
	> - load the blech-trailer (+maybe a Zahlenschloss) [done spontaneously]
	> - load the 2 packs of oatmilk next to the front door (didn't do that yet because of heat) before Wednesday midday? [done spontaneously]
	> Doug will drive with it to the festival and it will come back fully loaded with awesome saved stuff on Monday evening.
* [clara, brought forward by janina] wants to take stuff, some already tomorrow: https://pad.kanthaus.online/clara_stuff#
    * [Nathalie + Janina] was not so sure how to understand the second category: what if we want to get rid of those items in 10 years and nobody remembers that? Clara is encouraged to take everything she wants
    * [Matthias] encouraged Clara to mark everything with her name
    * [Janina] if someone wanted to redesign the dining room, they might not know about the status of the lady picture
* [Janina] food sorting responsibility hat, which Janina sort of felt responsible for?
    * [Eric] is also active
* [Eric] picked apples and they are in the dumpster kitchen

<!-- check if anyone has a point that didn't speak already -->

### Round 2


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Antonin
* **Tue.:** Martin
* **Wed.:**
* **Thu.:**
* **Fri.:**
* **Open Tuesday:** Kito

### Unavailabilities
* **Mon.:** Maxime
* **Tue.:** Helen
* **Wed.:** Helen
* **Thu.:** Helen, Maxime
* **Fri.:** Helen
* **Open Tuesday:** Helen, Maxime
* **Week:** Matthias, Janina

### Result
- **Mon.:** Antonin, Helen
- **Tue.:** Martin
- **Wed.:** Maxime
- **Thu.:** Nathalie
- **Fri.:** Eric
- **Open Tuesday:** Kito

## 7. For next week
*
