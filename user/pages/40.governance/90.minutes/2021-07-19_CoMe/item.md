---
title: Coordination Meeting
date: "2021-07-19"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #201
- Date: 2021-07-19
- Facilitator: Andrea, Doug
- Notary: Doug
- Mika caretaker: Tilmann/Anja
- Levi caretaker: Janina/Matthias
- Physical board caretaker: chandi
- Digital calendar: Matthias
- Reservation sheet purifier: Momo
- Present: Zui, Franzi, Thore, Janina, Silvan, Anja, chandi, Doug, Andrea, Matthias, Momo, Michal

----

<!-- Minute of silence -->

## 0. Check-in round

## 1. Last week review
### Stats
- **Present:** 12.7 people/day (+0.4)
- **⚡ Electricity**
    - usage: 4.64 €/day (⬇️-16%)
    - paid: -5.2 €/day 
    - ☀️ self produced: 74% (⬆️+2%)
    - emissions: 4 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.4 €/day (⬇️-11%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
- 3.6€ (RapsOil)

### Income
- 0€

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
_Still suspended_
<!--
- µCOVIDs available last week: 
- µCOVIDs used last week: 
- µCOVIDS balance from last week:  
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week:  
- µCOVIDs/person mean for this week:
Incidence 10.7 (Germany), 3.6 (Saxony)
-->

### Things that happened
- we have a bicycle repair station on the wall of K22!
- almost the whole house went to VK on Friday and enjoyed nice food and athmosphere
- we _almost_ saved a huge amount of mangold... -.-
- there was an interesting social sauna about money, common projects, connection and what holds kanthaus together
- we were so few people that we didn't have cooking teams for Tuesday and Friday
- awesome sushi evening and japanese movie watching took place in the garden
- nice neighbours gave us salad and berries from their gardens
- Franzi took Mika to Grimma for the first time
- Mika and Halvar played two times in one week!

## 2. This week planning

### People arriving and leaving
- **Mon.:** Nathalie arrives in the evening, Larissa and Kiki arrive, Silvan leaves, Sisi, Adam & David arrived
- **Tue.:** Franzi leaves, Maxime comes back, Silvan comes back
- **Wed.:** Sisi, Adam & David leave
- **Thu.:** 
- **Fri.:** Larissa, Kiki and Zui leave, Silvan leaves, Anja leaves, Clara might come
- **Sat.:** chandi leaves (maybe friday)
- **Sun.:** Larissa, Zui comes back
- **Mon.:** Silvan comes back, Anja comes back, 
- **Some day:** Michal maybe leaves around weekend

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_Very nice weather until Friday: Not too hot and dry. Then quite hot again over the weekend.

### Evaluations and check-ins
- Silvan Member : Absolute Days threshold 196/180 (+9%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Tilmann Member : 6 days until Absolute Days threshold (180)
- Anja Volunteer : 4 days until Absolute Days threshold (60)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Andrea, Doug]
    * Park cars house-side [Thore]
    * After dinner: Hanabi and analog game night [Momo]
- Tuesday
    * Hausmüll [Michal]
    * 19:30 'Weit' NDK Sommerkino https://www.ndk-wurzen.de/veranstaltungen/aktuelle/
- Wednesday
    * Park cars far-side [?]
    * 17:45 Landgut Nemt Call/pickup [?]
    * maybe after dinner: talking about last weeks [Nathalie, Larissa, Kiki, ?]
- Thursday
    * 10:00 Power Hour [Fac.: Matthias, DJ: ?]
    * 15:00 Evaluation Silvan [chandi]
    * 19:30 NDK Sommerkino UCKERMARK
- Friday
    * 12:00 Market pickup [Andrea]
    * 17:45 Landgut Nemt Call/pickup [?]
    * Biotonne [Michal]
    * 19:30 NDK Sommerkino WENDE MIGRA – Zeitzeug:innengespräche
- Saturday
    * 19:30 NDK Sommerkino ABER DAS LEBEN GEHT WEITER
- Sunday
    * 17:45 Landgut Nemt Call/pickup [?]
- Next Monday
    * 10:00 CoMe [Janina]
- Next week summary
    * 27th bday party

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
- To do:
    - fix storage shelf in k20 staircase
    - replace broken/string/toaster shoe rack spots
    - make k20 garden door easily closable from outside
    - secure wash kitchen sink
    - get yellow bags
    
## 3. Shopping plans
- [matthias] Solarrail grounding material, ~200-250€
- [matthias] replacement soldering iron tips for the Ersa and Weller soldering stations, 2-3 each for ~10 euros each -> 40-60€
    - (there is just one usable left for the ersa. They wear off quickly when soldering iron is standing around at high temperatures)
- [Thore] jam funnel, 10€

## 4. Discussion & Announcements

### Round 1
-  [JaTi] Levi moves, we need a place for the child bed possibly tonight. How about putting it back into the communal?
    -  Last night he slept in the free shop because he can't fall off a mattress that's already on the floor.
    -  We won't continue using the freeshop lounge.
-  [Doug] GSSX visit on 8th August :) Day trip, 7-10 ppl. Cool inter-project house exchange opportunity!
    -  Sounds good! Confirmed with corona condition.
    -  It's even the same day as the 1st potential foodsharing brunch. :D
-  [Thore] Voting phase for the Private usage ends today at 14:00
-  [Silvan] is the Car-Trailer needed from now on until End of August? Would like to use it to transport stuff to Events :)
    -  Nothing is planned for us to need the trailer. Request passed.
-  [Matthias] Would like to take pictures of the repair station with somebody on it to finish the project report. Might be put to website, volunteers? :)
    -  Momo and maybe Doug and maybe Andrea will do it. Thanks!
-  [Nathalie] Maybe you can just do a temperature check on the tuesday movie and then I'll coordinate around :) PS: writing with the phone is not great :D
    -  [doug] phone experience will be improved when hedgedoc switch to codemirror v2 ;p (already filed issue)
    -  People are interested in the movie and the updating session is rescheduled to Wednesday, so everything is possible! :D
-  [chandi] Postponing current constitution votings?
    -  Two topics opened: evaluation periods and house principles
    -  The principles need more thought and discussion.
    -  Postpone the principle process but carry on with the evluation periods
    -  evaluation and membership periods: https://gitlab.com/kanthaus/kanthaus-governance/-/merge_requests/8/diffs
    -  change in principles: https://gitlab.com/kanthaus/kanthaus-governance/-/merge_requests/9/diffs
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Matthias] Likely doing KMW tour to harzgerode and back on friday
- [Doug] Request: if emptying a bin, take trash straight to destination and immediately return bin/place new bag. Also: cardboard waste should go in the bin, or be stored inside.
    - Also: paper with foil and/or grease on it belongs to packaging and not paper waste
    - Also: we are out of yellow bags and are now using normal garbage bags for pckaging waste. maybe someone can get new yellow bags?
    - Oh no, they are not handed out anymore! How about we try to get another yellow bin?
    - Or we try to get yellow bags from places where they are still in use? Like Leipzig or Harzgerode..?

### Round 3

## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Doug
- **Tue.:** Thore
- **Wed.:**
- **Thu.:**
- **Fri.:**
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** Andrea, Janina, chandi
- **Tue.:** Andrea, chandi
- **Wed.:**
- **Thu.:** chandi
- **Fri.:** Matthias
- **Open Tuesday:** Matthias, chandi, Momo
- **Week:** Anja, Silvan, Franzi, Zui, Michal


### Finished task distribution
- **Mon.:** Doug
- **Tue.:** Thore, Momo
- **Wed.:** Matthias
- **Thu.:** Janina
- **Fri.:** chandi
- **Open Tuesday:** Andrea

## 6. For next week
-
