---
title: Coordination Meeting
date: "2022-05-30"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #246
* Date: 2022-05-30
* Facilitator: Silvan
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Tilmann
* Physical board caretaker: Andrea
* Digital calendar: Doug
* Reservation sheet purifier: Doug
* Present: chandi, Maxime, Andrea, Doug, Nathalie, Janina, Clara, Matthias, Silvan, Kito, Larissa

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/857a2b11-ea28-4c80-91f3-317814b3753c.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 19.7 people/day (+0.0)
- **⚡ Electricity**
    - usage: 60.43 €/week (⬆️+8%)
    - of that for heating: 5.66 €/week
    - paid: -39.36 €/week
    - ☀️ self produced: 80% (⬆️+2%)
    - emissions: 14 kg CO₂ₑ/week
- **💧 Water**
    - paid: 31.77 €/week (⬇️-17%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
* [chandi] 20€ for network switch (silent office / K20-0)
* [Larissa] 23€ fugenmörtel + acryl
* [Silvan] 24€ for the book

### Income
* 22+3€

### Things that happened

#### In or around Kanthaus
* A amazing gartenfest happened after many years of corona-hiatus
* A lot of food was saved and amazingly processed by many hands
* The trash corner has almost disappeared
* The new silent office was inaugurated with an epic LAN party
* three people went to a Krimidinner
* various people enjoyed the coworking space

#### Wider world
* the German chancellor compared klimaactivists to nazis
* war is continuing
* 9€-ticket is finally available and can be used starting on June 1st

## 2. This week planning

### People arriving and leaving
* **Mon.:** Antonin leaves
* **Tue.:** Anja comes back, Matthias leaves in the evening
* **Wed.:** Helen_WA might come
* **Thu.:**
* **Fri.:** chandi & silvan leave
* **Sat.:** Matthias comes back, Larissa leaves
* **Sun.:** Clara leaves, Larissa comes back, Silvan comes back, chandi comes back (possibly with jelli & findus)
* **Mon.:** Antonin comes back, chandi (+jelli&findus) leave
* **Some day:**

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_sun and clouds throughout the week, not much rain, getting warmer again until we have 27°C on Friday_

### Evaluations and check-ins

:person_rechtsprechung: Due for evaluation (and seen within the last 7 days):
- Nathalie Volunteer : Absolute Days threshold 101/60 (+68%)
- Andrea Volunteer : Absolute Days threshold 90/60 (+50%)
- Clara_WA Visitor : Days Visited threshold 26/21 (+24%)
- Doug Member : Absolute Days threshold 195/180 (+8%)

not here
- Thore Volunteer : Absolute Days threshold 139/60 (+132%)
- Anja Volunteer : Absolute Days threshold 91/60 (+52%)
- Martin_WA Visitor : Days Visited threshold 27/21 (+29%)
- Antonin Member : Absolute Days threshold 185/180 (+3%)

:gespenst: Absent:
- Clara Volunteer : Absence threshold 91/90 (+1%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Silvan]
    * Park KMW & Trailer on even/this side [done]
    * Post-Sunday-Dumpster-Diving Food sorting/washing [Maxime, Janina] *see discussion
* Tuesday
    * 13:00 governance café @Lantern
* Wednesday
    * 11:00 Clara's evaluation [Andrea]
    * Park KMW & Trailer on odd/that side [Maxime]
    * 9€-ticket is valid
    * 18:00 Punkrocktresen @D5
* Thursday
    * 10:00 Power Hour [Fac.: Janina, DJ: ?]
    * evening: put out yellow bins [Doug]
* Friday
    * 12:00 Market pickup [Kito]
    * 13:00 Nathalie's evaluation [Kito]
* Saturday
    * 10:00 Building Support @ NDK []
* Sunday
    * (pfingsten)
* Next Monday
    * 10:00 CoMe [Matthias]
    * evening: put out paper bins [Matthias]
    * (pfingstmontag, holiday)
* Next week summary
    *


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Matthias] special hardened drill bit for 5€ for work on bike repair station

## 4. To do
_Newest tasks on top_
* [ ] bring back NDK benches [Kito]
* [ ] mount fire extinguisher visible in the staricases K20-1 and K22-1
* [ ] sort first aid stuff, make two sets and mount that also in the staircase [chandi, Martin]
* [ ] repair right bike shed light
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator [kito]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [maxime] following the discussion at Social Sauna, I would like to try the idea to have up to 2 people assigned to Monday food sorting/washing during CoMe
    * Maxime volunteers to get it started and is happy about another person
    * Janina is up for helping, chandi is happy to take over if she has not enough time
* [chandi] trash tours
    * metal trash @ trebsen [silvan,chandi]
    * sperrmüll @ wurzen [doug,matthias]
* [kito] small birthday party planned for end of june
    * invite some people, hang out in the garden, play some games, have a bonfire, etc.
    * and tomorrow already there will be birthday coffee in the garden for another person ;)
* [Matthias] I'll likely move out in July. List of things I'd like to take posted in #kanthaus-stuff on Slack a week ago, please comment: https://yunity.slack.com/archives/CT0K2AY87/p1653076925513659?thread_ts=1653067865.961069&cid=CT0K2AY87 and I'll also make pictures/a stack with some clothes soon.
* [Larissa] Feedback to bathroom public hours: should we keep it or change it? make it a (longterm) agreement?
    * the issue resolved itself by having less people, making a collective agreement seems excessive
    * the idea was good for time of house fullness and the sign is written nicely, so it can just stay
* [Silvan] I want to take the hockerkocher with a gas bottle to a private birthday next week
    * no resistance
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [kito] egg boxes confusion - what do we do with them?
    * [silvan] they are very useful when getting new egs with shitty boxes. Last week I used nearly all of them
    * okay, now we collect them again until the stack in the dumpster kitchen is full again
* [chandi] group request: UnserAllerWald, 4-6. July, 10ppl
    * doug can imagine to host, but is only 75% sure to be here
    * zui can be a backup (also not totally sure to be there in this time)
    * larissa will definitely be here on the 4th and can help
    * so we approve and chandi tells them that they can come
* [maxime] Jon and Teresa from Witzenhausen would like to visit 13-17/06
    * no resistance

### Round 3
* [maxime] repair bathroom light or install lock?
    * [matthias] I will not repair it because it's too maintenance-heavy and complicated and I will sonn be gone
    * [kito] I can imagine making a crad system like in the intermediate, but it's not ideal
    * [matthias] the original lock is in the snack kitchen and I can mount it in the bathroom
    * [silvan] I will investigate which cable is broken for 10 minutes today and maybe it's gonna work for anyother year
* [chandi] 20min of internet downtime today: when?
    * anytime is annoying to someone, so just announce it beforehand
    * 14:00 it is

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:**
* **Tue.:** Maxime, Andrea
* **Wed.:**
* **Thu.:**
* **Fri.:** Larissa, zui
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Maxime, Nathalie
* **Tue.:** kito, Matthias, Nathalie
* **Wed.:** Matthias
* **Thu.:** kito, Janina, Matthias, Nathalie
* **Fri.:** Matthias
* **Open Tuesday:** Matthias, Maxime, Nathalie
* **Week:** chandi, Silvan

### Result
- **Mon.:** Clara, Matthias
- **Tue.:** Maxime, Andrea
- **Wed.:** Janina, Nathalie
- **Thu.:** Doug
- **Fri.:** Larissa, zui
- **Open Tuesday:** anja, Kito

## 7. For next week
*

