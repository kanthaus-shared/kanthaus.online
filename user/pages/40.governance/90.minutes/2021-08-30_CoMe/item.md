---
title: Coordination Meeting
date: "2021-08-30"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #207
- Date: 2021-08-30
- Facilitator: Andrea
- Notary: Maxime
- ~~Mika caretaker:~~
- ~~Levi caretaker:~~
- Physical board caretaker: Thore
- Digital calendar: Matthias
- Reservation sheet purifier: Doug
- Present: Andrea, Matthias, Zui, Blümchen, Jelli, Findus, Thore, Doug, Maxime

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
- **Present:** 12.1 people/day (+1.7)
- **⚡ Electricity**
    - usage: 5.51 €/day (⬆️+10%)
    - paid: -0.92 €/day 
    - ☀️ self produced: 69% (⬆️+2%)
    - emissions: 12 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.0 €/day (⬇️-7%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [Matthias] 121€ for Steel + Aluminium for the new trailer & to finish Carla Cargo
- [Matthias] 201€ for wheels + light for new trailer + replacement bicycle tubes of sizes we ran out
- [Matthias] 40€ for a diamant Bohrkrone (Sorry, was a bit more expensive, but this hopefully lasts for all the holes we need to do...)

### Income
- 0€

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)

> From [Collective Agreements](https://kanthaus.online/en/governance/collectiveagreements): *If adjusted prevalence is < 0.1%, we have no formal restrictions.*
> Current **adjusted prevalence** can be found [here](https://www.microcovid.org/?duration=600&interaction=oneTime&personCount=1&riskBudget=100000&riskProfile=average&scenarioName=custom&setting=indoor&subLocation=Germany_Sachsen&theirMask=none&topLocation=Germany&voice=loud&yourMask=none&yourVaccineType=johnson): click on "Details" in the "Step 1" column

- Incidence
    - Germany: 77 (+33%)
    - Saxony: 21
    - Landkreis Leipzig: 22.6
- Adjusted prevalence Saxony: 0,06% (last check: 2021-08-29) / Germany: 0,26%
- µCOVIDs available last week: {COVIDS FOR LAST WEEK NOT FOUND!}
- µCOVIDs used last week: 
- µCOVIDS balance from last week:  
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week:  
- µCOVIDs/person mean for this week:  (18 people) 

### Things that happened
#### In and around Kanthaus
* Nick got his trailer back... and Matthias built a new one :-)
* Kanthaus now has a communal 68.58cm WQHD monitor
* Knowledge sharing about radio waves
* Knowledge transfer on roof month organization
* The Co-Up Festival happened

#### In the wider world

## 2. This week planning

### People arriving and leaving
- **Mon.:** Clara left, Antonin left, Zui leaves, Blümchen, Jelli, and Findus might leave or on the next day
- **Tue.:** Clara comes back, chandi comes back, Larissa leaves, Nathalie maybe leaves
- **Wed.:** Andrea leaves, Clara leaves
- **Thu.:** Clara comes back, Zui and Larissa come back
- **Fri.:** Anja will arrive and stay for some time
- **Sat.:** Andrea comes back
- **Sun.:** JaTiMiLe come back
- **Mon.:** Zui leaves
- **Some day:** Antonin comes back, chandi leaves again for some days


### Weather forecast

Monday and Tuesday will be rainy days and from then, it should be warmer and sunnier. 

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->



### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
Antonin Member : Absolute Days threshold 227/180 (+26%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Thore
- Clara Volunteer : 1 days until Absolute Days threshold (60)
- Maxime Member : 6 days until Absolute Days threshold (180)



### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Andrea]
    * 15:00 Knowledge sharing: 
- Tuesday
    * Open Tuesday []
    * Hausmüll [Matthias]
- Wednesday
    * 10:00 Monthly Coordination Meeting [Matthias]
    * 11:00 Thore evaluation [Doug]
    * Park cars on odd-side [Maxime]
    * Landgut Nemt pickup [Thore]
- Thursday
    * 10:00 Power Hour [Fac.: Maxime, DJ: ?]
    * 15:00 Sharing event [Thore]
- Friday
    * Biotonne [Maxime]
    * 12:00 Market Pickup [Doug]
    * Landgut Nemt pickup [Thore]
- Saturday
    * 20:00 Project Updates [Matthias]
- Sunday
    * Landgut Nemt Pickup [Thore]
- Next Monday
    * 10:00 CoMe [Doug]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans

- [Antonin] insulation material for the K20-22 wall, ~40€ (https://yunity.slack.com/archives/CN9H631BM/p1630132200001700)
- [Andrea] olive oil, balsamico, soy sauce
  - [Doug] has recommandations on the soy sauce
- [Matthias] salt

## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure wash kitchen sink
* [ ] secure k22-2 bathroom sink

### Medium priority
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] make k20 garden door easily closable from outside (or acquire a new door) https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000 [Doug may do this out of frustration :( ]
* [ ] fix loose table leg in Lantern
* [ ] empty dry food storage shelf (find new places for the stuff)
* Ventilation preparation:
    * [ ] Tear down chimney in Snack Kitchen
    * [ ] Tear down chimney in communal sleeping room
    * [ ] Tear down chimney in dining room
    * [ ] Tear down chimney in piano room
    

## 5. Discussion & Announcements

### Round 1
- [Doug] Roof month! To start on Wednesday 13 Oct for 4 weeks. [Frame](https://pad.kanthaus.online/roofMonthOct2021?both) updated to reflect feedback. New [poll](https://poll.disroot.org/373TNY9W87QM3TH9) for the 4 weeks—please fill in :) Previous technical responsible people: let's meet soon to figure out what we need to order!
- [Andrea] There are two workaways requests, both planning to visit Germany in October. Invite them for the Roof Month?
    - [janina] sounds good! and also for childcare! :yum:
<!-- check if anyone has a point that didn't speak already -->
- [Antonin] could someone get Findus' bike from under the tarp and decide on its fate with her? If she does not want it I would give/sell it away with the other bikes there
  - Matthias will talk with Findus

### Round 2
- [Matthias] inviting for comments on the proposal of building a small (approx. 2m x 5m) roof in front of K22 garden door https://yunity.slack.com/archives/C3RS56Z38/p1630248338041100
- [Doug] inviting to read/join the discussion started by Janina in #kanthaus-governance on new points in the Collective Agreements
- [Doug] we have a new Slack channel: #kanthaus-in-house, to express things that are useful only to people actively in the house. People not in the house will bear the responsibility for the additional noise

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:**
- **Open Tuesday:** Doug2

### Unavailabilities
- **Mon.:** Matthias, Thore
- **Tue.:** Doug
- **Wed.:**
- **Thu.:** Maxime
- **Fri.:** Maxime
- **Open Tuesday:** Andrea, Matthias, Maxime
- **Week:** Andrea, Zui, Blümchen, Jelli, Findus

### End result
- **Mon.:** Blümchen, Jelli, Findus
- **Tue.:** Maxime
- **Wed.:** Matthias
- **Thu.:** Doug
- **Fri.:** Thore
- **Open Tuesday:** Doug

## 7. For next week
