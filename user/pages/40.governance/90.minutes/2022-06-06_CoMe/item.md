---
title: Coordination Meeting
date: "2022-06-06"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #246
* Date: 2022-06-06
* Facilitator: Matthias
* Notary: Antonin
* Mika caretaker: Tilmann
* Levi caretaker: Tilmann
* Physical board caretaker: Nathalie 
* Digital calendar: Doug
* Reservation sheet purifier: Doug & Kalle
* Present: Kalle, Doug, Silvan, Matthias, Antonin, Nathalie, Janina, Mika, Thore, Anja, Larissa, Maxime, Zui, Tin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/c22ef7ae-97e7-4741-9cc2-b18cb39fe328.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.4 people/day (-4.0)
- **⚡ Electricity**
    - usage: 52.29 €/week (⬇️-14%)
    - of that for heating: 5.13 €/week
    - paid: -54.61 €/week 
    - ☀️ self produced: 79% (⬇️-1%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 26.27 €/week (⬇️-17%)
    - emissions: 2 kg CO₂ₑ/week


### Expenditure
* [chandi] 20€ for first aid boxes

### Income

### Things that happened

#### In or around Kanthaus
* Ultimate frisbee action game
* The yoga room light frame has been completed
* The first strawberries were harvested

#### Wider world
* The 9 euro ticket started and now Germany is a socialist country
* The trains are full now
* Critical mass in Grimma

## 2. This week planning

### People arriving and leaving
* **Mon.:** chandi left
* **Tue.:** 
* **Wed.:** kito, Louis come back
* **Thu.:** chandi comes back, Matthias leaves
* **Fri.:** JaTiMiLe, Zui, Nathalie, Tin and Antonin leave
* **Sat.:** 
* **Sun.:** Nathalie and Tin come back
* **Mon.:** 
* **Some day:** Doug & Anja leave, Kalle leaves

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Sunny and warm (but not hot) with some wind and maybe some bits of rain.

### Evaluations and check-ins

- Thore Volunteer : Absolute Days threshold 146/60 (+143%)
- Anja Volunteer : Absolute Days threshold 98/60 (+63%)
- Andrea Volunteer : Absolute Days threshold 97/60 (+62%)
- Matthias Volunteer : Absolute Days threshold 75/60 (+25%)
- Doug Member : Absolute Days threshold 202/180 (+12%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Matthias]
    * Post-Sunday-Dumpster-Diving sorting/cleaning [Kalle, Tin, Larissa]
* Tuesday
    * Paper waste [matthias]
    * 08:31 Tauschladen trip [Matthias]
    * 15:00 Evaluation Thore [Antonin]
    * 20:00 OpenRefine, string diagrams and why it all matters [Antonin]
* Wednesday
    * Rest waste [Tin]
    * Park KMW & Trailer on odd/that side [Silvan]
    * Hygienemuseum Dresden trip [Anja, Doug]
    * Tobi from HZ comes pick up the scaffolding
    * Punkrocktresen
* Thursday
    * 10:00 Power Hour [Fac.: Larissa, DJ: ?]
    * 18:00 foodsharing Wurzen meeting @Garden [Thore, Janina]
* Friday
    * Market pickup [Maxime]
* Saturday
    * Biomüll [Larissa]
* Sunday
    * Mayor election
* Next Monday
    * 10:00 CoMe [Nathalie]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 10€ for 10€

## 4. To do
_Newest tasks on top_
* [ ] remount snack kitchen radiator [Silvan?]
* [x] sort first aid stuff, make two sets [chandi]
* [ ] mount fire extinguisher ~~& first aid kits visible in the staricases K20-1 and K22-1~~
* [ ] repair right bike shed light
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Janina] Lausitz bike trip about to start! Here's some polite requests:
    * Can Daria come? No concerns so far, please approach Janina if any left.
    * Can we have childcare for packing up?
    * Don't be surprised when all the plastic water bottles disappear :3
        * How do we manage the water bottle shortage? Can bottles be taken from the basement? -> off-CoMe discussion
    * No communal bikes will be used
* [Anja] Doug and me will go to the Hygiene Museum in Dresden on Wednesday. Possibly also visiting Felix in Dresden. Feel free to join us!
    * [Silvan] is there any time when it is free?
         * [Anja] not sure, but 5€ is still affordable
* [Antonin] Please fill in [the K18 poll](https://cloud.kanthaus.online/apps/forms/AEzfKtedLMf5xNym), especially volunteers and members
* [chandi] first aid things are sorted
    * there is now the (almost) same set of material in the bathroom and K22 staircase
    * complies DIN 13164 :) + extra stuff
    * everything else (refill + non first aid things) in the medical storage
    * if you deplete something, make sure to refill it :) and keep the tools in there
* [zui] my mum comes on wednesday, just so you know :)
* [Doug] Axel would borrow/get a tent. Can we find one that bike trip people do not want to take? He is also doing official foodsharing at a festival and wants to use the KMW to do the pickup there.
    * [Silvan] we do not have so many functioning tents so it would be good to have it back
    * [Larissa, Janina] sorting and decision made in a small group?


<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Anja] closing the freeshop for two weeks? Because there are so few people.
    * [Larissa] will there be more people in two weeks?
    * [Matthias] maybe rather 3 weeks?
    * [Nathalie] will do it next week
* [Matthias] Harzgerode pickup: Tobi will need help to get the scaffolding out of the basement
    * [Kalle] feels up for helping
    * [Tin] would be up on Thursday
    * [Larissa] it would be good to keep an inventory of what we lend them
    * [Matthias] giving away windows to Alix: we would need 2-3 people to mix and match windows, Matthias feels up for doing it but would need help
    * [Janina] giving away blankets (without covers) because we have enough of that size? No resistance.
    * [Janina] how many oatmilk do we need? Do we need to tell Tobi?
        * [Silvan] 2-3 pallets?
        * [Matthias] 2 is probably enough
        * [Kalle] a lot of oat milk available from another house project further appart
    * [Anja] the knitting stuff can also go to HZ
* [Kalle] if you need help, approach me
* [Doug] closing the door silently in K20-1 please
* [Janina] huge puddle of white paint in the yoga room. How did that happen? How to remove it? Zui attempted to seal the paint pot by turning it upside down, but in a different location than where the puddle is. Please clean up after your building sites!
    * [Nathalie] sigh, the yoga room!
    * [Nathalie] can imagine doing some cleaning up of the floor
    * [Silvan] can take care for taking the paint boxes away

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Silvan
* **Tue.:** Zui
* **Wed.:** kito
* **Thu.:** 
* **Fri.:** Thore
* **Open Tuesday:** Anja, Kalle

### Unavailabilities
* **Mon.:** Matthias, Nathalie, Larissa, Tin
* **Tue.:** Antonin, Larissa
* **Wed.:** Antonin , Larissa, Tin, Doug
* **Thu.:** Antonin, Matthias, Nathalie, Doug
* **Fri.:** Antonin, Matthias, Nathalie, Larissa, Tin, Doug
* **Open Tuesday:** Antonin, Matthias, Larissa, Maxime
* **Week:** Janina

### Result
- **Mon.:** Silvan, Antonin
- **Tue.:** Zui, Doug
- **Wed.:** kito, Nathalie
- **Thu.:** Tin, Larissa
- **Fri.:** Thore, Maxime
- **Open Tuesday:** Anja, Kalle

## 7. For next week
* 
