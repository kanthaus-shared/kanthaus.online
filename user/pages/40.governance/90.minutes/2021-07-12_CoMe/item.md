---
title: Coordination Meeting
date: "2021-07-12"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #200 🎉
- Date: 2021-07-12
- Facilitator: Doug
- Notary: Silvan
- Mika caretaker: Tilmann
- Levi caretaker: Janina
- Physical board caretaker: Andrea
- Digital calendar: Doug
- Reservation sheet purifier: Doug 
- Present: Janina, Tilmann, Franzi, Silvan, Matthias, Talita, Andrea, Doug

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_11ed31d2ed9655f30ad9f528be43bc24.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.9 people/day (-7.1)
- **⚡ Electricity**
    - usage: 5.65 €/day (⬇️-13%)
    - paid: -2.96 €/day 
    - ☀️ self produced: 72% (⬇️-3%)
    - emissions: 5 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.7 €/day (⬇️-30%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
- 12€ for 15 covid quick tests
- 17.20€ for 64 rolls of Happy End™ Toilet paper with amazingly soft triple layers, 100% recycled.
- 35€ for sanding machine filter bags
- 21,10€ for miele vacuum cleaner bags

### Income
- none in house

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
_Framework is suspended due to low incidence_
<!--
- µCOVIDs available last week: 15000
- µCOVIDs used last week: 
- µCOVIDS balance from last week:  
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week:  
- µCOVIDs/person mean for this week:  (18 people) 
-->

### Things that happened
- Today is Kanthaus' 4th anniversary and 200th CoMe! :tada:
- Lots of people left
- Internal funding meeting took place... and got postponed
- Doug and Antonin went to J&F and helped renovating their bathroom
- Social football watching with Dietmar
- Levi started to move forward!
- foodsharing Wurzen meeting
- Unkraut became a Mika caretaker :D
- JaTiMiLe spent a night in a tent in the garden and then one in newsealand
- Matthias greatly improved the wash-kitchen bath outlet!
- First delicious cucmber from the garden was harvested and many more vegetables are growing :)
- Roof reinforcement work finished!
- soap bubbles making on Wurzen Square happened
- apparently Saturday was a party day

## 2. This week planning

### People arriving and leaving
- **Mon.:** 
- **Tue.:** Nathalie, Thore come back, Silvan leave
- **Wed.:** Morgane arrives, Silvan comes back
- **Thu.:** 
- **Fri.:** 
- **Sat.:** Anja comes
- **Sun.:** Adam, Sisi and David come
- **Mon.:** 
- **Some day:** chandi (around the weekend)

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Very warm and dry, but risk of thunderstorms especially on Tuesday evening, Wednesday, Saturday and Sunday.

### Evaluations and check-ins
- Silvan Member : Absolute Days threshold 189/180 (+5%)
    - delayed
Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Antonin Member : 2 days until Absolute Days threshold (180)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [Doug]
    - 4th anniversary of Kanthaus: Celebration?
    - afternoon: puzzle mats moving action: cave -> sleep kitchen + hipster [Janina, Doug]
    - afternoon: cherry-picking in Altenbach [Franzi, Silvan]
- Tuesday
    - 
- Wednesday
    - 17:30 Landgut Nemt [Janina, Matthias]
- Thursday
    - Gelbe Tonnen [Matthias]
    - 10:00 Power Hour [Fac.: Janina, DJ: Franzi]
    - ~~14:00 Knowledge Sharing: ? [?] ~~<!-- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
    - 15:00 Social sauna [Doug]
- Friday
    - 17:30 Landgut Nemt [Janina, Franzi]
    - ~18:00 Barabend at VK
- Saturday
    - Franzi check-in
- Sunday
    - 16:00 Sushi prep party [Anja]
    - 17:30 Landgut Nemt [Janina, Doug]
    - 19:00 Sushi party [Anja]
- Next Monday
    - 10:00 CoMe [Andrea, Doug]
- Next week summary
    - 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

## 3. Shopping plans
- ?[michal] Rapsöl (~6 EUR)

## 4. Discussion & Announcements

### Round 1
- [Doug] Roof-Finale poll now open! https://poll.disroot.org/kCZtUK2KRDezaR3R
- [Anja] I'd like to have a vegan sushi-making & -eating (with optional summerrolls) event with you + neighbours (Wolfi, J&F, Klugis) on sunday evening. I'd buy specific ingredients but dumpsterdiving before would be very appreciated! If you have any wishes what should go into sushi and we don't have it yet, tell me. :)
    - maybe Tofu!
- [Silvan] Friday is Barabend at VK, would someone be motivated to make some food for this? And it would be great if many people would come :)
- [Janina] Trip to Dresden to get pasta: Who can imagine doing it?
    - [Doug] Motivated for it to happen and also okayish to participate.
    - [Franzi] Also motivated
    - [Janina] Maybe also motivated, especially if we take a child...^^
- [Janina] Constitutional change votes going on!
    - Position system: https://yunity.slack.com/archives/C9B9TGPPT/p1625842428002300?thread_ts=1625842176.002200&cid=C9B9TGPPT
    - Principles: https://yunity.slack.com/archives/C9B9TGPPT/p1625842924002900?thread_ts=1625842176.002200&cid=C9B9TGPPT

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Doug] donation feedback
    - will boost post
- [Silvan] KH anniversary?
    - 16:00 small video session in garden
    - [Doug] alumni call..?

## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Matthias, Janina
- **Tue.:** 
- **Wed.:** Franzi + X
- **Thu.:** Talita, Andrea
- **Fri.:**
- **Open Tuesday:** Franzi, Doug

### Unavailabilities
- **Mon.:** Talita, Andrea
- **Tue.:** Talita, Andrea
- **Wed.:** Talita
- **Thu.:** 
- **Fri.:** Talita, Matthias
- **Open Tuesday:**
- **Week:** michal, Tilmann, Silvan

## 6. For next week
- 
