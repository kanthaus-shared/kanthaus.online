---
title: Coordination Meeting
date: "2022-01-31"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #229
* Date: 2022-01-31
* Facilitator: Antonin
* Notary: chandi
* ~~Mika caretaker:~~
* ~~Levi caretaker:~~
* Physical board caretaker: Eumel
* Digital calendar: chandi
* Reservation sheet purifier: Antonin
* Present: Eumel, Antonin, chandi, Tobi

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/606af270e324df8cb28591509.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.3 people/day (-4.9)
- **⚡ Electricity**
    - usage: 132.66 €/week (⬇️-13%)
    - of that for heating: 71.1 €/week
    - paid: 115.99 €/week 
    - ☀️ self produced: 12% (⬇️-3%)
    - emissions: 78 kg CO₂ₑ/week
- **💧 Water**
    - paid: 19.5 €/week (⬇️-16%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [Antonin] 7.5€ for 10kg wheat flour

### Income
- 170€

### Things that happened

#### In or around Kanthaus
- Another Gemök group met in kanthaus
- A chimney was begun to get killed
- ToI started!

#### Wider world
- Lovely friends finally found a flat!
- The Greens have new leaders - and they already a scandal :D
- Ukraine conflict continues to escalate

## 2. This week planning

### People arriving and leaving
* **Mon.:** Andrea, chandi and findus leave
* **Tue.:** 
* **Wed.:** Antonin leaves
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Änski comes back
* **Mon.:** A lot of people come back
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
a lot of rain and cold and wind

### Evaluations and check-ins
none

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
* Tuesday
    * Hausmüll [Antonin]
    * 15:00 Open Tuesday [Unkraut, Antonin]
* Wednesday
    * Repark cars on the opposite side [Antonin]
* Thursday
    * 10:00 Power Hour [Fac.: tobi, DJ: eumel]
* Friday
    * Biotonne [Tobi]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 
* Next Tuesday
    * 10:00 CoMe [chandi]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [chandi] 10€ for the RFID door system


## 4. To do
* [ ] repair Schaukasten 
* [ ] Clean K18 garden
* [ ] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Install light in the hallway of the K20-1 flat
* [ ] Install light in the hallway of the K20-2 flat

### more advanced tasks
* [ ] continue Grundstücksverschmelzung K20 & K22 combining both properties)


## 5. Discussion & Announcements

### Round 1
* [Antonin] viva im Silent Office um 14:00?
    - doing it
* [chandi] food situation, plenty of food, but not needed anymore in Könnern.
    * there is free shop tomorrow, lets give away there
* [eumel] We could ne wall paint equipment. Would it be possible to borrow it?
    * no resistance
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [antonin] ToI team proposed, to lock the doors.
    * let's do it!


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

* **Open Tuesday:** Antonin

## 7. For next week
* 

