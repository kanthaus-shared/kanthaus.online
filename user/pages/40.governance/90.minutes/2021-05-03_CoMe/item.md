---
title: Coordination Meeting
date: "2021-05-03"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #189

- Date: 2021-05-03
- Facilitator: Kito
- Notary: Antonin
- Mika caretaker: Larissa
- Physical board caretaker: Maxime
- Digital calendar: Maxime
- Reservation sheet purifier: Talita
- Present: Talita, Nathalie, Maxime, Matthias, Andrea, Thore, Tilmann, Janina, Clara, Kito, Antonin

----

<!-- Minute of silence (?) -->

### 0. Check-in round

### 1. Last week review
##### Stats
- **Present:** 18.0 people/day (+0.6)
- **⚡ Electricity**
    * usage: 10.89 €/day (⬆️+24%)
    * paid: -0.49 €/day 
    * ☀️ self produced: 73% (⬆️+1%)
    * emissions: 9 kg CO₂ₑ/week

- **💧 Water**
    * paid: 4.32 €/day (⬆️+4%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 2.51 €/day (⬇️-21%)
    * emissions: 59 kg CO₂ₑ/week


##### Expenditure
- 10€ building rubble
- 15€ building rubble
- 50€ Elektrowerkstatt-Oschi

##### Income
- 5€ donation

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 3850
- µCOVIDs used last week: 5051
- µCOVIDS balance from last week: -1201
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 3799
- µCOVIDs/person mean for this week: 211 (18 people) 

Corona status: 7-day incidence (LK Leipzig): 154,2 (last week: 161,9)
- (source: https://www.coronavirus.sachsen.de/infektionsfaelle-in-sachsen-4151.html#a-9785)

##### Things that happened
- the building weeks came to an end
    - there are tiles on the roof
    - solar panels have been installed
    - the roof windows finally arrived and one was mounted
    - a lot of metal sheets were attached
    - gutter and snow protection were installed
    - rainwater collection was added
    - a nice party was held in the end
- the wenceslai fairteiler appeared
- the first antipat/kritma round in kanthaus happened
- thore got the keys to the flat
- project updates on Sunday
- Electronics workshop got partly rearranged as there is a big lab power supply station now
- KMW got a diagnosis display that hopefully helps a bit with maintenance diagnostics when Matthias is not around :)

### 2. This week planning

##### People arriving and leaving
- **Mon.:** Lise + Bodhi arrive, Cato and Rumo arrive, ChaLaZu leave
- **Tue.:** 
- **Wed.:** ChaZu comes back, Larissa leaves
- **Thu.:** Jums arrives
- **Fri.:** 
- **Sat.:** 
- **Sun.:** 
- **Mon.:** Larissa comes back, Silvan too
- **Some day:** Doug: earliest Friday, possibly later re corona/fullness; kito leaves someday between thursday and saturday; Cuca, Rumo and Cato will leave some day

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


##### Evaluations and check-ins

- Doug Member : Absolute Days threshold 184/180 (+2%)
- Kito Volunteer : Absolute Days threshold 61/60 (+2%)
- Talita Volunteer : 1 days until Absolute Days threshold (60)
- Andrea_WA Visitor : 5 days until Days Visited threshold (21)

##### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
<!-- Ja&Ti's availability due to Mika care as of 2021-02-08: 13-16, 60% of the time -->
- Monday
    - 10:00 CoMe [kito]
    - 15:00 Sharing Event [Fac: ?] 
- Tuesday
    - 14:00 Antipat/Kritma-Meeting [kito]
- Wednesday
    - 15:00 Kito's evaluation [Thore]
- Thursday
    - 10:00 Power Hour [Fac.: Tilmann, DJ: Clara]
    - 14:00 Knowledge Sharing: Hedgedoc [Matthias]
        <!-- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
    - 20:00 Evening Activity: ? [?]
- Friday
    - Gelbe Tonne
    - 15:00 Andrea's evaluation [Talita]
    - 20:00~ living-project-space dreaming [Matthias]
- Saturday
- Sunday
- Next Monday
  - 10:00 CoMe [Antonin]
- Next week summary
  - 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)


### 3. Shopping plans



### 4. Discussion & Announcements

#### Round 1
- [lise] Bodhi and me will arrive today. The corona points will be over the threshold. If people feel uncomfortable with that, we can do quarantine in the Kleingarten. Please write us in that case. (see https://yunity.slack.com/archives/C01015MHJ3G/p1619623700007100)
  - they can come
- [janina] journalists might call because of fairteiler. I'll try to answer te phone but am happy about help! ^^'
  - we will do that!
- [Silvan] I think we ran out of Siebdruck Platten and I would get new ones on 11. or 12.5. Is someone motivated to join?
  - [thore] up for it
  - [janina] should the wood shed be reorganized beforehand? not much space left
- [Doug] I'm interested to 'restart' open-Tuesday in some form when I get back... Termin-shopping? 2 hours of internal sorting/fixing? Zu Verschenken Kiste auf der Strasse? If you interested, let me know.
  - [nathalie] what do we do with the bed in the freeshop?
- [kito] corona-points ukuvota. proposals 1 and 2 are not super elaborated, we aimed for a quick solution
- [tilmann] I'm going to build one or more kitchen high shelves this week; you are welcome to join me in making the wood nice, putting them together and optionally making doors!

#### Round 2
- [matthias] I'd like to facilitate a session on visions of a political living-project-space outside of kanthaus as I notice interest in at some point finding (or founding? D:) a space for the next (tens?) of years. Interest? Should we schedule something?
  - scheduled for friday evening

#### Round 3
- [matthias] Turn off the heating system? I notice, that radiators esp. in moisture-critical rooms get regularly turned off when I turn them on. Is there heat used anywhere?
  - (nice to know: Even though it seems little from the price, we still use more energy for heating than our electricity usage is!)

### 5. [Task lottery food planning](https://kanthaus.gitlab.io/task-lottery/)

#### Volunteers
- **Mon.:** Maxime, Nathalie
- **Tue.:** Clara, Kito
- **Wed.:** Janina, Matthias
- **Thu.:** Thore, Tilmann
- **Fri.:** Talita, Antonin
- **~~Open Tuesday~~:** 
