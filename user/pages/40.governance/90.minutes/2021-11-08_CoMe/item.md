---
title: Coordination Meeting
date: "2021-11-08"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #217
- Date: 2021-11-08
- Facilitator: chandi
- Notary: Matthias
- Mika caretaker: -
- Levi caretaker: Konrad
- Physical board caretaker: Nathalie
- Digital calendar: Chandi
- Reservation sheet purifier: Matthias
- Repro Recruiter: Maxime
----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_d674f9591541bdcb96159dd8105ceac2.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 19.4 people/day (-1.1)
- **⚡ Electricity**
    - usage: 13.2 €/day (⬆️+7%)
    - 🔥 for heating: 3.54 €/day (equivalent gas cost: 3.64€/day)
    - paid: 8.53 €/day 
    - ☀️ self produced: 34% (⬇️-16%)
    - emissions: 46 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.65 €/day (0%)
    - emissions: 2 kg CO₂ₑ/week


### Expenditure
- [tilmann] 382.16€ for more ventilation parts
    - total cost for ventilation project so far: ~4600€
    - enough to ventilate most of 1st and 2nd floor
- [chandi] 15€ for spark plugs (Zündkerzen) for the KMW
- [matthias] 10€ for door hinge parts

### Income
- 21€ in the donation shoe

## Things that happened
- a lot of building activity
- acroyoga session
- haircutting & coloring session
- a new big hole appeared in a wall
- a nice party at the Punkrocktresen
- the communal sleeping room got 0.4m² bigger
- singing + jam session

### World
- 4th corona wave is happening. Landkreis Leipzig at an 7 days incidence rate of 422! germany wide around 100 deaths per day (95% unvaccinated), hospitals are on their limit again
- COP26 is ongoing, climate change will be fixed....sure


## 2. This week planning

### People arriving and leaving
- **Mon.:** zui left
- **Tue.:** chandi's parents come for the day
- **Wed.:**
- **Thu.:** JaTiMiLe leave, Änski leaves, Guido leaves
- **Fri.:** 
- **Sat.:** ZuLa come back (In the evening), WA person comes
- **Sun.:** Thore and Nathalie are leaving 
- **Mon.:** Andrea leaves, Saranke + WA person leave
- **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins
_paused for roof month, but it ends in 2 days!

:judge: Due for evaluation (and seen within the last 7 days):
- *Anja* _Volunteer_ : _Absolute Days_ threshold 168/60 (+180%)
- *Andrea* _Volunteer_ : _Absolute Days_ threshold 82/60 (+37%)
- *Nathalie* _Volunteer_ : _Absolute Days_ threshold 75/60 (+25%)
- *Guido_WA* _Visitor_ : _Days Visited_ threshold 26/21 (+24%)
- *Thore* _Volunteer_ : _Absolute Days_ threshold 68/60 (+13%)
- *Antonin* _Volunteer_ : _Absolute Days_ threshold 61/60 (+2%)

:clock1130: Due for evaluation soon (in the next 7 days, and seen within the last 7 days):

- *Zui* _Member_ : 0 days until _Absolute Days_ threshold (180)
- *Doug* _Member_ : 6 days until _Absolute Days_ threshold (180)
- *Konrad_WA* _Visitor_ : 1 days until _Days Visited_ threshold (21)
- *Saranke_WA* _Visitor_ : 6 days until _Days Visited_ threshold (21)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 09:00 CoMe [chandi]
    * 16:00 ROAW meeting [Nathalie, Antonin]
    * 18:00 Daily coordination meeting [charly]
- Tuesday
    * rest waste [Tilmann]
    * 16:00 Open Tuesday [Doug, Anja]
    * 18:00 Daily coordination meeting
- Wednesday
    * repark cars [Thore]
    * 18:00 Roof month finish meeting?
- Thursday
    * 10:00 Power Hour [Fac.: Doug?, Matthias, DJ: ?]
    * 14:00 Evaluation Nathalie [?]
- Friday
    * bio trash [Saranke]
    * 12:00 Market Pick-Up [Saranke, Nathalie]
- Saturday
    * Party [Nathalie, Doug]
- Sunday
    * 
- Next Monday
    * 10:00 CoMe [Matthias]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [chandi] (in the next months) 240€ for Electricity in the attic
    - 160€ 18m cable NYM 5x10mm²
    - 30€ Unterverteilung
    - 30€ Fuses/RCD


## 4. To do
_Small fixes of things in the house_

* [ ] bringing KMW Trailer to TÜV **within November**
* [x] secure k22-2 bathroom sink [charly]
* [ ] fix K22-2 toilet flush pipe
* [ ] fix hipster room window curtain
* [ ] more storage spaces for visitors
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [x] yellow bin in snack kitchen and kitchen - new solution [chandi]
* [ ] lubricate door hinges (which?)
* [ ] install a light switch (or detector) in the washroom
* [x] replace soap holder in K20-1# toilets [maxime]
* [x] KMW: fix untight exhaustion part [chandi]
* [x] KMW: replace ignition cables [chandi]
* [x] KMW: Fix the water leak under the front shield [chandi] (hopefully, to be checked during next rain)
* [x] KMW: Fix cooling pipe so it does not touch the hot exhaust [chandi]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door


## 5. Discussion & Announcements

### Round 1
* [matthias] Communal sleeping room: I'd like to remove wallpaper/paint it white but I am sure I would not finish before thursday or friday. Alternatively, it could be put back in place now after a deep clean. The plastering doesn't make too much dirt.
    * As there will be anyway no work before Wednesday, the communal will be cleaned just a bit and 2 mattresses put back today. Please be aware that surfaces might still be dirty, so please try to only use the mattresses :D
    * The work there will then continue on thursday for a few days, as we are much less people then
* [Thore] Ilona needs some help to fix her roof in the Kleingarten. It's only putting some screws in, if I understood her right. Everything except the screwdriver is there. Is someone up for doing it?
* [Janina] Sleeping spot orga: Some people had difficulties finding sleeping spots last week. Now with the communal sleeping room being out of order planning could be even more beneficial. There is a new sleeping spot preferences sheet on the magnet board next to the window in the dining room. Please use it until tonight, if you want to take part in collective sleeping spot planning! :)
* [maxime] please avoid releasing sensible amounts of food-incompatible chemicals in kitchens or dining room around cooking and eating times; typically by applying nail polish (but would also apply to starting a gas engine or burning paint)
* [änski] Brought some food stuff yesterday (pasta salad, meaty rice salad, stuffed capsicum). I think it should be eaten today. So maybe the lunch preparer Team could use it or put it outside. 
* [andrea] (I dont know where to write this, since there is no lottery) I volunteer for open tuesdays in case it happens and for cooking dinner on wednesday! also I'm aware that my power hour task isnt finished, i will take care of it tuesday morning! 
* [Saranke] When does mopping happen?
    * It is not a regular task
    * People in power hour normally start a bit later for that and coordinate with vacuum team
* [Silvan] A few days ago I ran into a wheelbarrow right in front of K22 garden door. Would be nice to have nothing in the way there

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Matthias] Rubble tour today? Please bring sand on the way back.
* [Saranke] Would like to take some pictures today of the roof work. Anybody has objections?
    * Chandi would not like to be photographed straight from the face

### Round 3
* [Matthias] Please keep bathroom door closed, especially in the night...

## 6. Reproductive Tasks
Shall we use the sheet again?
(There is a new one below the one from last week on the physical board.)

## 7. For next week
- 


---

