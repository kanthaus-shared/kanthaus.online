---
title: Coordination Meeting
date: "2021-11-15"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #218
- Date: 2021-11-15
- Facilitator: Matthias
- Notary: chandi
- Mika caretaker: -
- Levi caretaker: -
- Physical board caretaker: Doug
- Digital calendar: Zui
- Reservation sheet purifier: Larissa
- Participants: Charly, Larissa, Anja, Doug, Matthias, Silvan, Zui, chandi, Maxime

----

<!-- Minute of silence -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_e0d65444cf1be0c0cec8e010a8482875.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.6 people/day (-3.6)
- **⚡ Electricity**
    - usage: 12.78 €/day (⬇️-4%)
    - of that for heating: 5.35€ / day
    - paid: 7.60 €/day 
    - ☀️ self produced: 39% (⬆️+4%)
    - emissions: 42 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.24 €/day (⬇️-11%)
    - emissions: 2 kg CO₂ₑ/week



### Expenditure
- [Silvan] 10€ thinner
- [chandi] 1540€ as the final payment for the solar plant (we paid the rest already last year. it's now 19233€ in total)
- [charly] 5€ for Bauschaum
- [anja/doug] 20€ for Party ingredients

### Income
- [Shoe] 10€

## Things that happened
- We finished the roof!
- and The final roof party happened!
- A sunday after party as well
- Debate club
- Karaoke night
- Some people watched the entire series of squid game
- Electronic door opener got installed


### Wider World
- covid situaion is going crazy: incidence rate of 935 in our Landkreis, again 160 deaths/day in germany (~98% of them unvaccinated)
    - saxony by far corona hotspot in germany (incidence 754 vs 303 in germany)

## 2. This week planning

### People arriving and leaving
- **Mon.:** Anja, Saranke, Miro, Antonia, Konrad, Andrea leave
- **Tue.:** JaTiMiLe come back
- **Wed.:** Clara comes back
- **Thu.:** chandi leaves, Larissa leaves
- **Fri.:** 
- **Sat.:** Silvan leaves
- **Sun.:** Andrea, Larissa, Silvan, Thore and Nathalie come back
- **Mon.:**
- **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
cold, cloudy, but rain free week, not freezing and warm on the weekend with 11deg

### Evaluations and check-ins

- Zui Member : Absolute Days threshold 187/180 (+4%)
- Doug Member : Absolute Days threshold 181/180 (+1%)

also due to get evaluated: Andrea, Antonin, Nathalie, Thore

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Matthias]
    * repark cars [Matthias]
- Tuesday
    * 14:00 Doug's Evaluation [Zui]
    * 16:00 Open Tuesday [?]
- Wednesday
    * 14:00 Zui's evaluation [matthias]
    * 16:00 Introduction to heating system @ Piano Room [Matthias]
    * repark cars [chandi]
- Thursday
    * 10:00 Power Hour [Fac.: Larissa, DJ: ?]
    * 14:00 Social Sauna [Matthias]
- Friday
    * 12:00 Market Pick-Up [?]
- Saturday
    * yellow bin [?]
- Sunday
- Next Monday
    * 10:00 CoMe [doug]
- Next week summary


_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

## 4. To do
_Small fixes of things in the house_

* [ ] bringing KMW Trailer to TÜV **within November**
* [ ] fix K22-2 toilet flush pipe
* [ ] fix hipster room window curtain
* [ ] more storage spaces for visitors [charly]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] lubricate door hinges (which?)
* [ ] install a light switch (or detector) in the washroom
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door


### New this week
* [ ] Building week clean up tasks
    * [ ] Tarp in the garden
    * [ ] scaffolding net in the garden
         - [antonin] if we are to store the scaffolding in the basement again, then I would wait for that to happen first (to leave space for big pieces first), and then store it there too, maybe?
* [ ] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900)
* [ ] sort and introduce a categorization in the kitchen shelves
* [ ] Door handle for Elephant Room new door

## 5. Discussion & Announcements
 
### Round 1
- [Thore] My parents have a firm mattress (90\*200cm) they don't need anymore. Are we interested in this? 
    - we could replace the lower bed one in the dorm with a good matress, but to be confirmed
    - only if they come anyway, there are avaiabale locally also easily
- [chandi] there are 3 open visitor requests in the mails
    - Nono
        - https://yunity.slack.com/archives/G7E8RPFMH/p1636632968000300
        - knows us via Martin
        - 22nd of December to 3rd of January
        - [Andrea] I would like to be responsible for this one and I wanted to ask people if they would be against it or not! I would host :D
            - [matthias] generally in favor of this one, concerns, hosting if you're not here most of the day?
    - Tania
        - https://yunity.slack.com/archives/G7E8RPFMH/p1636663009000700
        - from the Canary Islands
        - Janina replied, no further response so far
    - Laurie
        - https://yunity.slack.com/archives/G7E8RPFMH/p1636730379001200
        - Workaway says: "Arrival: 15/11/2021 - Departure: 29/11/2021"
        - Janina is in contact, they are going to some other place now and may come afterwards
    - [Andrea] To this point, I wanted to ask, how is the situation after the building month? How are we going to do it with the new workaway requests? 
        - [doug] How I see it is that it is up for the host to communicate and decide. 
        - [chandi] The work away profile should reflect our non-building-month-situation
        - [zui] Who's hosting should be clear and people should not be hosting to many people, perceived andrea & janina overwhelmed
    - [silvan] Maybe a better hosting system / transparent list of current visitors & their hosts?
- [Matthias] Would be nice to have the elephant room finished & usable again at some point. Who feels responsible for the finishup?
    - Staircase has not been deep-cleaned
        - -> power hour
    - on top of elephant room bed there is also still lots of dust.
        - -> power hour
    - Door handle?
    - Should we install a door closer? I heard people are unable to reliably close the door Elephant <-> piano room - this one closes even harder.
        - [chandi] I would wait for another 1-2 weeks and reevaluate then ;)
- [Antonin] future of the scaffolding? I started a [thread on Slack](https://yunity.slack.com/archives/CN9H631BM/p1636897041013900).
    - [Tilmann] let's plan garden side wall insulation before making a sale decision. might be possible to use it even when contracting the work.
- [Silvan] Who will clean up the Basement Party? 
    - [doug] It will happen today! straight after a post come coffee break (PCCB)
- [doug] Bring a tiny bit of the building months forward into the future. For example again something like a wupphour?
    - maybe people want to think about it this week and announce it for next week


<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Matthias] I'll do plastering in the Communal Sleeping room today all day. Are there other people motivated to help/work? Wallpaper removal/painting would be really nice, but I'd also like to set it back in place latest on  saturday
    - no resistance for blocking the room until saturday since the house will be quite empty until then
- [Silvan] Who wants to call Landgut Nemt for beetroot?
    - [doug] I can call there
    - (some discussions about the potatos in the hallway)
- [tilmann] Matthias and me picked up attic insulation planning again, anybody else also would like to get involved soon? We're doing it to protect condensation in ventilation pipes, but also for the general benefit of less heat loss. Expect expense requests in the coming weeks.
    - [zui] I'm interested in the organizational part
- [chandi] How to continue with the weekly reproductive tasks?
    - task lottery is considered as the default
    - a small group of doug & chandi thinks about something for next week

### Round 3
- [Matthias] Heating requirements? I am still optimizing the heating system using temperatures we had the last years as a reference point.
    - Temperature: Currently aiming at about 18 degrees (at ceiling). This leads to heating costs of around 4-5 € / dayhttps://pad.kanthaus.online/come?both currently at 6 degrees average outdoor temperature/little rooms heated. When we increase the temperatures by 1-2 degrees, this would increase the heating cost around 8-15%.
    - Time: Currently the rooms are warm from about 8-22 (but less warm in the morning). Each hour earlier/longer increases our heating cost about 6,5%.
    - no comments: People seem to be happy with the current situation
- [Silvan] there is unwashed food in the dumpster kitchen

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)


### Result
- **Mon.:** chandi, Doug
- **Tue.:** Larissa
- **Wed.:** Charly, Maxime
- **Thu.:** Matthias
- **Fri.:** Zui, Silvan
- **Open Tuesday:** Doug2


## 7. For next week
- 


---

