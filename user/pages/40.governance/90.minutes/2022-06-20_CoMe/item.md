---
title: Coordination Meeting
date: "2022-06-20"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #248
* Date: 2022-06-20
* Facilitator: Louis
* Notary: Nathalie
* ~~Levi caretaker:~~
* ~~Mika caretaker:~~
* Physical board caretaker: Antonin
* Digital calendar: chandi
* Reservation sheet purifier: Kito
* Present: Nathalie, chandi, Maxime, Antonin, Kito, Louis, Silvan

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/59d31323-f5a0-4870-837d-a9c35e37d264.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.4 people/day (-11.4)
- **⚡ Electricity**
    - usage: 36.8 €/week (⬇️-39%)
    - of that for heating: 3.61 €/week
    - paid: -67.49 €/week 
    - ☀️ self produced: 77% (⬇️-2%)
    - emissions: 9 kg CO₂ₑ/week
- **💧 Water**
    - paid: 28.01 €/week (⬇️-2%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- KMW gas refill 

### Income
195€ of donation in the shoe 

### Things that happened
#### In or around Kanthaus
- A lot of stuff was saved at the Melt Festival
- someone brought us a bike, a good one!
- we have a new shower curtain (any feedback -> maxime), it's beautiful!
- trip to the lake
- Nails party in the garden, very fancy
- 30-min piano room dancing
- project updates happened 

#### Wider world
- Forests are burning in Brandenburg (and elsewhere)
- The German government announced that coal power plants that were closed will be restarted to compensate for the cut in gas supplies
    - campaign started this weekend from already over 5.000 people stating, that they will come to Lützerath (a village threated by a coal mine) and stand in the way, if the goverment decides to tear it down https://www.x-tausend-luetzerath.de (english version will be produced later the day in the office^^)
- Progressive Senator Gustavo Petro became Colombia’s first progressive president-elect since the 1930’s
- France is coming back to a parliamentary regime, as the president's party doesn't have the majority in the parliament.

## 2. This week planning

### People arriving and leaving
* **Mon.:** Antonin comes back, Thore leaves
* **Tue.:** Silvan leaves
* **Wed.:** Maxime leaves
* **Thu.:** kito's party guests, maybe some will stay for the night
* **Fri.:** Lozana arrives, Louis leaves
* **Sat.:** 
* **Sun.:** Antonin and Lozana leave, Anja and Doug come back, chandi leaves, kito leaves, Nathalie leaves
* **Mon.:** Anja leaves
* **Some day:** JaTiMiLe, Zui & Larissa come back

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Andrea Volunteer : Absolute Days threshold 111/60 (+85%)
    - Nathalie will remind Andrea
- Chandi Member : Absolute Days threshold 192/180 (+7%)
    - scheduled for friday

Due for evaluation (and seen within the last 7 days):
- Martin_WA Visitor : 3 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Louis]
    * Park KMW & Trailer on even/this side [Louis]
    * Black bin out in the evening [kito]
* Tuesday
    * 15:00 Open Tuesday
    * Fête de la musique programm : https://www.fetedelamusique-leipzig.de/programm-2022/
* Wednesday
    * Park KMW & Trailer on odd/that side [Louis]
    * Punkrocktresen
* Thursday
    * 10:00 Power Hour [Fac.: Antonin ,DJ: ]
    * 14:00 social sauna [chandi]
    * kitos birthday party 
    * Green bin out in the evening [Louis]
* Friday
    * 10:00 chandis evaluation [Antonin]
    * 12:00 Market pickup [kito]
    * Parkfest starting in Wurzen
* Saturday
    * 13:00 Football tournament @ Lüptitzer Str [kito]
    * evening: small concert in D5
* Sunday
* Next Monday
    * 10:00 CoMe [someone from the arriving people :) Antonin will do a post]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [kito] 73,75€ for oats
    - yes!
    - 25 kg
    - shop in Leipzig
    - organic - kito checks again, very likely Bioland/ Naturland


## 4. To do
_Newest tasks on top_
* [x] change shower curtain [maxime]
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher & first aid kits visible in the staricases K20-1 and K22-1
* [ ] repair right bike shed light
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
- [kito] any questions left regarding the party?
    - kito explains party plans
    - some people might sleep here
    - we are looking forward to it!
    - no questions raised
- [chandi] Lot's of things lying around (garden, hallway). how to progress?
    - Who feels responsible for the festival stuff?
        - Louis can give a hand, but needs help where to put stuff
        - Silvan will help :)
        - sleeping, bags, chairs? in the house, freeshop, maybe to Bosnia
    - Do it in power hour?
        - Is it a communal task?
        - not ideal
        - ok as last resort
    - [Silvan] hallways will be cleaned by tomorrow
    - [Silvan] store things in dragon room
        - [Maxime] maybe elephant room bed as temporary storage place when the house is empty anyway
            - we like this idea
            - until july
- [Nathalie] only washed food upstairs - I'd appreciate that
    - [chandi, kito] ++
    - [maxime] onions and mushrooms e.g. degrade by washing
    - use half-way stairs shelf for unwashed food

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [chandi] dumpster diving! 
    - find motivated people now or do we need more structure?
    - discussion about dumpster diving spots
    - people will go someday
- [Silvan] watering the garden
    - everyday or every two days
    - it's a better experience now 
    - someone needs to take care when Silvan is gone

### Round 3
- [Silvan] "Bubble"-Lightchains in the house
    - taken from basement for parties, but not put back
    - pleeasse put them back to the basement
    - if its used it's fine, sad when unused and vortexed
        - often lying around and not used
    - better sign in basement
        - should make clear that they belong to Silvan


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Antonin
* **Tue.:** 
* **Wed.:** 
* **Thu.:** chandi
* **Fri.:** 
* **Open Tuesday:** kito

### Unavailabilities
* **Mon.:** Nathalie
* **Tue.:** Nathalie
* **Wed.:**
* **Thu.:**
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** maxime, Silvan

### Result
- **Mon.:** Antonin
- **Tue.:** 
- **Wed.:** Louis
- **Thu.:** chandi
- **Fri.:** Nathalie
- **Open Tuesday:** kito 

## 7. For next week
