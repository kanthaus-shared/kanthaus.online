---
title: Coordination Meeting
date: "2022-05-09"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #243
* Date: 2022-05-09
* Facilitator: Antonin
* Notary: Kito
* Mika caretaker: Tilmann
* Levi caretaker: Tilmann
* Physical board caretaker: Doug 
* Digital calendar: Zui
* Reservation sheet purifier: Linnea 
* Present: chandi, Louis, Johnny, Janina, Maxime, Clara, Linnea, Lucía, Antonin, Zui, Hue, Anja, kito

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/ffd07678-bac3-465d-8b6f-15dd5b54a19b.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.0 people/day (+2.1)
- **⚡ Electricity**
    - usage: -0.16 €/week (⏬-101%)
    - of that for heating: 9.28 €/week
    - paid: -53.46 €/week 
    - ☀️ self produced: 5564% (⏫+5516%)
        - [chandi] there is obviously something broken. I'll look into it
    - emissions: 5 kg CO₂ₑ/week
- **💧 Water**
    - paid: 26.18 €/week (⏫+97%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
* 0€

### Income
* 0€

### Things that happened

#### In or around Kanthaus
* things were  built (lot of stuff happened in the attic, mirrors were attached to the yoga room, many cables, ...)
* many Kanthausians attended the Georg-Schwarz-Straßenfest
* an almost 2 year-old memory leak was fixed in Inventaire, bringing back joy, and serenity to its system administrators
* people went to Punkrocktresen
* the tax report is finished!!! 
* lot of visitor induction events

#### Wider world
* Sinn Féin ('Shin Fane')—democratic-socialist & Republican (Irish)—gain majority at Northern Ireland Assembly for first time.
* Oben-ohne for women in swimming pools in Göttingen on the weekends, since last Monday. [Taz](https://taz.de/Gleichberechtigung-fuer-nackte-Oberkoerper/!5847274/)
* Fuchsmühle looking for new residents (talk to Doug)
* Abortion rights in the US are about to loose ground

## 2. This week planning

### People arriving and leaving
* **Mon.:** Konrad, Antonia, Smilla, Marita & Julë arrive, Larissa and Martin come back
* **Tue.:** Anja leaves
* **Wed.:** Marita leaves
* **Thu.:** Doug leaves
* **Fri.:** Natascha arrives, Smilla & Julë leave, Mariha arrives (or on Saturday), Doug comes back, Louis leaves
* **Sat.:** Lucía leaves
* **Sun.:** Anja comes back
* **Mon.:** Louis comes back
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
- clouds and sun
- ok, lot of sun, summer will arrive, up to 26 degrees

### Evaluations and check-ins
Check-ins:
- Martin [Janina]
- Louis [Doug]
- Hue [Doug]
- Linnea [chandi]
- Clara [Andrea]

Evaluations:
- Kito Volunteer : Absolute Days threshold 103/60 (+72%)
- Silvan Volunteer : Absolute Days threshold 73/60 (+22%)
- Anja Volunteer : Absolute Days threshold 70/60 (+17%)
- Andrea Volunteer : Absolute Days threshold 69/60 (+15%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
    * 15:30 House tour @garden [Kito]
    * Park KMW & trailer on even/this side [Doug]
    * Put the general waste bin outside [Louis]
* Tuesday
    * TV crew in Kanthaus [Antonin]
    * 15:00 - 17:00 Freeshop []
    * 16:00 book burning memorial @market square
* Wednesday
    * Finance meeting [ChaLaZu]
    * 10:00 gas heater action [Antonin]
    * Park KMW & trailer on odd/that side [Doug]
    * 18:00 Punkrock bar with vegan BBQ @D5
* Thursday
    * organic waste [Antonin]
* Friday
    * 10:00 Kito's evaluation [Janina]
    * 12:00 Market pickup [Hue]
    * 14:00 2nd week of construction reflection round @garden [Tilmann (or Kito?)]
	* 15:00 Power Hour [Fac.: Antonin, DJ: ???]    
* Saturday
    * ~15:15 communal trip to the lake? :)
      * yeay!
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [kito]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* Governance-Co-Working [janina,chandi]

## 3. Shopping plans
* [chandi] 55€ for pipe insulation
    * no concerns :)

## 4. To do
_Newest tasks on top_
* [x] fix garden pavement stones [Doug]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been
* [ ] remove old gas heater from basement to trash corner - needs 3-4 people 
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] Light: special bikes shed
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [chandi] it is time to say goodbye: wuppdays wifi
    * there is `kanthaus`, which is the preferred option, but not working on some (older) devices
    * if there are issues: use `kanthaus-insecure`, which is technically equivalent to `wuppdays`, so there can't be a technical reason why `wuppdays` works better^^
    * it is called `insecure`, but it has the same security level as `wuppdays` always had, the new `kanthaus` is just now way more secure
* [tilmann] 2nd week of construction: kick-off
    * attic team meets in the garden after CoMe
    * silent office team coordinates with Antonin (need help definitely!)
    * yoga room on pause until Wednesday (because Silvan is gone)
    * ventilation team could be created if there's interest (talk to Tilmann if you are interested, chandi could also give an introduction)
    * roof window team starts 12:00
* [janina] sleeping spot planning for this very full week
    * to avoid annoying sleeping place search
    * don't necessarily mean that the person written in the list sleeps there
    * it's preferences, but table in the room is what counts 
* [antonin] TV crew in Kanthaus on Tuesday: ribbons for those not wishing to be filmed
    * [maxime] could you link to the TV website and/or a video they made in the same format they want to do here?
        * they are called [VPTV](https://www.vptv.de/), not exactly clear for which program they do it
* [Doug] Opportunity for other constitutional proposals [until tonight](https://ukuvota.world/#/app/4b17ad1b-32c5-9330-24e5-05cfb8163b93/collect).
* [maxime] the newspaper stack in the dumpster kitchen as been freshly refilled, please use it generously in bins to make those easier to keep clean(ish)
* [kito] can someone welcome Marita and Smilla at 11:30?
    * yes, Doug and Janina feel up to opening the door and saying hi
    * and there will be a house tour, starting 3:30pm in the garden

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [chandi] Visitor-Request: Host for Natasha from Friday on?
    * Kito can do the introduction on Friday
* [janina] Repro orga adjustments
    * include the weekend dinner in lottery?
    * include lunch in lottery?
    * also: a lot of childcare needed this week :3


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** kito, kito_plus_one
* **Tue.:** 
* **Wed.:** Julë, zui
* **Thu.:** maxime 
* **Fri.:**
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Tin, Larissa, Antonin
* **Tue.:** kito,Larissa, Antonin, chandi
* **Wed.:** Larissa, Antonin, Doug
* **Thu.:** kito, Antonin, Doug
* **Fri.:** chandi, Louis, Janina, Doug
* **Open Tuesday:** Janina, Larissa, chandi
* **Week:** 

### Result
- **Mon.:** kito, kito_plus_one
- **Tue.:** Hue, Johnny
- **Wed.:** julë, Zui
- **Thu.:** Maxime, Louis
- **Fri.:** Antonin, Lucía
- **Open Tuesday:** Anja, Clara


## 7. For next week
* 
