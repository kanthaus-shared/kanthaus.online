---
title: Coordination Meeting
date: "2022-04-11"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #239
* Date: 2022-04-11
* Facilitator: Antonin
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Tilmann
* Physical board caretaker: Andrea
* Digital calendar: chandi
* Reservation sheet purifier: Janina
* Present: Andrea, Doug, Tilmann, Levi, Thore, Antonin, chandi, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf483b.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.7 people/day (-1.6)
- **⚡ Electricity**
    - usage: 69.95 €/week (⬇️-11%)
    - of that for heating: 51.27 €/week
    - paid: 12.87 €/week 
    - ☀️ self produced: 49% (⬇️-1%)
    - emissions: 28 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.56 €/week (⬆️+7%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
* [tilmann] 27€ kitchen exhaust pipe and wall box
* [Bodhi] 25€ trailer insurance (paid privately up to now)

### Income
* 50€ donation

### Things that happened

#### In or around Kanthaus
* 1st meeting of FLINTA* Tech AG for programming, server administration and wine
* super little people here over the weekend
* monthly teams meeting for April
* NDK spring fest and Punkrocktresen were attended
* Levi had the first fever of his life
* 1st gartenfest planning meeting

#### Wider world
* Presidential elections in France cemented Status Quo
* Lützi lost court case: It's now legal to tear down the village. Huge demo planned.

## 2. This week planning

### People arriving and leaving
* **Mon.:** Anja comes, Martin comes
* **Tue.:** Matthias comes, Martin leaves
* **Wed.:** Anja leaves
* **Thu.:** chandi leaves, Butze+Chris arrive
* **Fri.:** 
* **Sat.:** zui leaves
* **Sun.:** Butze+Chris leave, chandi comes back today or tomorrow
* **Mon.:** zui comes back
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

- Janina Member : Absolute Days threshold 192/180 (+7%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
* Tuesday
    * ~~10:00 Trash tour? (see expense request)~~
    * 18:00 book club @Villa klug
* Wednesday
    * Park KMW & Trailer on odd/that side [Doug]
    * 17:00 Garden BBQ party [chandi, Janina]
* Thursday
    * Organic waste [Doug]
    * 10:00 Power Hour [Fac.: Antonin, DJ: ?]
    * 18:00 foodsharing Wurzen meeting @lantern [Janina, Thore, Nathalie]
* Friday
    * 10:00 Attic working session and skillshare [Tilmann]
    * 15:00 Janina's Evaluation [Antonin]
* Saturday
    * 13:00 Critical Mass
* Sunday
    * Easter brunch @dining room [Janina]
* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary
    * 
 
To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Nathalie] fill the Fairteiler gap: 413,24€, can this be paid by Kanthaus?
    * slight unhappiness, how did this happen? no resistance though.
* [Antonin] up to 100€ to rent a transporter for a day to bring trash away on Tuesday morning. The car would have a trailer hitch and I would pick it up in Borsdorf or Paunsdorf. I want the trash gone and have nothing to do with the KMW. If someone else wants to rather do a trash tour with the KMW I'm happy to help loading and unloading.
    * [matthias] just a note if i'm not back, there is quite some valuable trash metal (steel, copper, transformators) which should be sold to a trash place instead of trashed
    * [chandi] feels really weird to rent a car when we have one standing outside
    * [janina] interesting way of raising the issue we have with the KMW
    * rejected for now, but the bigger issue needs to be solved!

## 4. To do

_Newest tasks on top_
* [ ] make the wall nice in K20-2 bathroom where the boiler has been
* [ ] remove old gas heater from basement to trash corner - needs 3-4 people 
* [ ] repair WiFi in K22-2
* [ ] repair Schaukasten 
* [ ] Make new signs for the hats/gloves/scarves shelf in the hallway (?)
* [ ] ~~Door-closer for Elephant-staircase door~~
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Andrea] on Sunday the 8th of May Martin and I would like to host the meeting of the shibari group in KH. We would use the yoga room for 4 hours and it would be a closed event. Would that collide with the building month too much? 
* [chandi] anyone experienced any (new) wifi issues over the weekend? I've partially rolled out the upgrade. if there are no issues, I'll finish it with all Access Points this week :)
* [Antonin] spring bike giveaway! I would like to give away 5 bikes which have been left broken and unused for at least a year. If you would like to keep one of them, it would be great if you could feel responsible for bringing it back in a rideable state soon. https://yunity.slack.com/archives/CQPK7722K/p1649593303952639
* [janina] Next host needed: [Rosa_WA](https://www.workaway.info/en/workawayer/rlmpvd/overview)

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Antonin] TV crew coming to film a documentary on 10th May


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Janina, chandi
* **Thu.:** 
* **Fri.:** zui, Andrea
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Nathalie, doug
* **Tue.:** Antonin, Nathalie, doug
* **Wed.:** Antonin, doug
* **Thu.:** Antonin, Nathalie
* **Fri.:** 
* **Open Tuesday:** Antonin
* **Week:** Tilmann, Levi

### Result
- **Mon.:** Antonin
- **Tue.:** Thore
- **Wed.:** Janina, chandi
- **Thu.:** Doug
- **Fri.:** zui, Andrea
- **Open Tuesday:** anja, nathalie
