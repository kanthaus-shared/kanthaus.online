---
title: Coordination Meeting
date: "2022-07-25"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #253
* Date: 2022-07-25
* Facilitator: Matthias
* Notary: Matthias
* Mika caretaker: KiTa
* Levi caretaker: Doug
* Physical board caretaker: Martin
* Digital calendar: Doug
* Reservation sheet purifier: Janina, Eric
* Present: Eric, Andrea, Martin, Janina, Matthias, Doug, Levi

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/4eee6949-a38c-4929-8c49-8ec3bce95e52.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.1 people/day (-1.4)
- **⚡ Electricity**
    - usage: 42.42 €/week (⬇️-12%)
    - of that for heating: 3.42 €/week
    - paid: -57.33 €/week 
    - ☀️ self produced: 82% (⬆️+6%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.15 €/week (⬇️-32%)
    - emissions: 1.3 kg CO₂ₑ/week


### Expenditure
* [doug] Bicycle tube 5€
* [doug] book "Glitzer im Kohlenstaub" 20€

### Income
* 

### Things that happened

#### In or around Kanthaus
* Some sicknesses all around
* Eric is checking out Leipzig
* Bike repair workshop day at Kulturcafe
* Another Flinta-Tech-Treff meeting

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* 

## 2. This week planning

### People arriving and leaving
* **Mon.:** Matthias maybe leaves
* **Tue.:** Nathalie and Thore come back
* **Wed.:** Matthias definitively leaves
* **Thu.:** 
* **Fri.:** 
* **Sat.:** Matthias comes back
* **Sun.:** Tobi comes
* **Mon.:** Nathalie and Thore leave
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

- Hot today, better rest of the week, rainy on the weekend.

### Evaluations and check-ins
- Doug: Can do most day-times. We could offer BBB if people want?
  - [Antonin] BBB could be nice!

- Doug Member : Absolute Days threshold 244/180 (+36%)
- Antonin Member : Absolute Days threshold 234/180 (+30%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [?]
    * Park KMW & trailer on even/this side [Noone]
* Tuesday
    * 
* Wednesday
    * Park KMW & trailer on odd/that side [Doug]
    * 10:00 Monthly teams meeting [Doug]
* Thursday
    * 10:00 Power Hour [Fac.: Martin, DJ: ?]
    * 12:00 Evaluation Doug [Janina potentially]
* Friday
    * Gelbe Tonne [Martin]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do
_Newest tasks on top_
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?] ~~Denkmalschutz der Fassade?~~
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher [chandi]
* [ ] first aid kits visible in the staricases K20-1 and K22-1
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] add a doorbell in the garden
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Andrea] might have a friend visiting from Wednesday to friday.
* [Doug] This is the last week of Kulturcafe in the current place
    * On saturday, there is the meeting to find out how/where to do it in the future. Please bring up your ideas :-)
* [Janina] how is it with the quarantine area?
    * Fine.
* [Eric] I'd like to go to the Auerworld. Does somebody wants to come?
* [Doug] I'd like to go to the Klimacamp for one day this week. Does somebody wants to come?
    * [Antonin] planning on going to the demo against the expansion of the airport
* [Doug] We have a new resident. It's a bit fragile, found it on the street and it's fun. It might break at any point, that's fine. It helps us keep the floors clean.
* [Doug, Martin] We liked the chilled CoMe outside
* [Doug] I'm not gonna be here for open tuesday the next weeks. I'd be grateful if we manage to keep it open.

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Doug

### Unavailabilities
* **Mon.:** Janina
* **Tue.:** Janina
* **Wed.:** Janina
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Andrea, Matthias

### Result
- **Mon.:** 
- **Tue.:** Martin
- **Wed.:** Eric
- **Thu.:** Janina
- **Fri.:** 
- **Open Tuesday:** Doug

## 7. For next week
* 
