---
title: Coordination Meeting
date: "2022-05-16"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #244
* Date: 2022-05-16
* Facilitator: Kito
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Matthias
* Physical board caretaker: Antonin
* Digital calendar: Zui
* Reservation sheet purifier: Larissa
* Present: Clara, Linnea, chandi, Mariha, Anja, Hue, Antonin, Konrad, Antonia, Kito, Larissa, Natascha, Martin, Louis, Zui, Maxime, Tilmann

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/e0a91c54-b978-46bc-85ff-1a1ee8af7933.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 22.1 people/day (+6.0)
- **⚡ Electricity**
    - usage: 49.72 €/week (0%)
    - of that for heating: 7.2 €/week
    - paid: -59.84 €/week 
    - ☀️ self produced: 79% (⬇️-3%)
    - emissions: 12 kg CO₂ₑ/week
- **💧 Water**
    - paid: 41.06 €/week (⬆️+57%)
    - emissions: 3 kg CO₂ₑ/week

### Expenditure
* [chandi] 55€ for pipe insulation
* [antonin] 50€ for Elektrikergips, Acryl, masking tape, piping
* [zui] 4€ oil

### Income
* 

### Things that happened

#### In or around Kanthaus
* More building week action!
    * Dampfsperre is taking shape in the attic
    * Yoga room fancy light is partly there
    * Silent office ventilation and electricity progressed
    * Lake trips and fun evenings
    * roof window progress
* The house was really really full
* It was kinda hot and people slept in the garden
* There is finally also proper wifi in The Private
* We found an incredible amount of ridiculously huge duvets
* 2nd FLINTA* tech meetup in Leipzig
* Book burning memorial and graffiti action
* many people went to Punkrocktresen again
* spontaneous dance party in the garden
* board game night with dressing up

#### Wider world
* 50°C heatwave in Pakistan and India still ongoing
* [Israel killed the journalit Shireen Abu Akleh who was covering its colonisation war, and then sent police to the funeral.](https://www.theguardian.com/commentisfree/2022/may/13/shireen-abu-aqleh-israeli-police-palestinian-funeral)
* [Germany forbid demonstrations of solidarity with Palestine](https://www.aljazeera.com/news/2022/5/13/germany-bans-vigil-in-memory-of-journalist-killed-by-israel)
* russia with apparently [significant losses](https://www.nytimes.com/2022/05/15/world/europe/pro-russian-war-bloggers-kremlin.html) and is getting pushed back by the ukrainian forces
* ukraine won the Eurovision song contest
* elections in NRW and Lebanon

## 2. This week planning

### People arriving and leaving
* **Mon.:** Jonas left
* **Tue.:** Mariha leaves, Kathi comes, Max comes
* **Wed.:** Antonin leaves
* **Thu.:** 
* **Fri.:** Anja leaves, Silvan + Larissa + Kathi (maybe Mika?) leave, Willy comes, Louis leaves
* **Sat.:** Linnea leaves
* **Sun.:** Antonin comes back, Larissa + Silvan (+ Mika) come back, Willy leaves, Louis comes back
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

_finally some rain tomorrow, follwed by lots of sun again. then thunderstorm on the weekend. generally quite warm with up to 29°C on thursday._

### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Silvan Volunteer : Absolute Days threshold 80/60 (+33%)
- Anja Volunteer : Absolute Days threshold 77/60 (+28%)
- Andrea Volunteer : Absolute Days threshold 76/60 (+27%)
- Doug Member : Absolute Days threshold 181/180 (+1%)

Due for evaluation soon (in the next 7 days, a
nd seen within the last 7 days):
- Lise Visitor : 1 days until Days Visited threshold (21)
- Matthias Volunteer : 6 days until Absolute Days threshold (60)
- Clara_WA Visitor : 6 days until Days Visited threshold (21)
- Hue_WA Visitor : 6 days until Days Visited threshold (21)
- Linnea_WA Visitor : 6 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Kito]
    * Park KMW & trailer on even/this side [Louis]
    * 15:00 Radeln ohne Alter Wurzen meeting, online [Nathalie, Antonin]
    * 18:00 introduction to inventaire & webtech @piano room [Maxime]
* Tuesday
    * 15:00 - 17:00 Freeshop [Anja]
    * Sushi evening [Anja]
* Wednesday
    * Park KMW & trailer on odd/that side [Louis]
    * 17:00 Open door @Leuchtenmanufaktur
    * 18:00 Punkrocktresen @D5
* Thursday
    * 11:00 Silvan's evaluation [Kito]
    * 15:30 IDAHIT* @Marktplatz
    * evening: Pictures and stories from Sieben Linden [Sieben Linden](https://siebenlinden.org) [Silvan] (might be in German)
* Friday
    * yellow bin gets emptied [Konrad]
    * 12:00 Market pickup [Linnea]
    * 14:00 The total reflection [Janina]
	* 15:00 Power Hour [Fac.: chandi, DJ: chandi]
	* after dinner: Project updates [Zui]
* Saturday
    * 10:00 foodsharing brunch @garden or dining room [Janina]
    * 15:00 Critical Mass @Marktplatz
* Sunday
    * 15:30 governance coworking cafe @garden [chandi]
* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [maxime] olive oil, rapeseed oil
* [matthias] I still didn't buy the missing tools for the bike repair station and think about getting stainless steel tools?
    * - 76€ for a rollgabelschlüssel, 0-24mm (https://www.banemo.de/763897/KS-Tools-Edelstahl-Rollgabelschluessel-verstellbar-8-964.1302?utm_source=referrer&utm_medium=cpc&utm_campaign=idealo&et_seg1=46)
    * 15€ for a 12/13mm Schlüssel
    * 15€ for a PH1 philips screwdriver
    * ... alternative would be another proxxon set for now 30€ + a standard screw driver 5€.
        * come crowd: the cheaper ones seem more reasonable for this use case
* [larissa] maybe some Fliesenkleber if I don't find any in the house

## 4. To do
_Newest tasks on top_
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [x] remove old gas heater from basement to trash corner - needs 3-4 people 
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] Light: special bikes shed
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [janina] 3rd week of construction starts!
    * attic team: two more people would be good! meet in garden after CoMe
    * silent office team: who is up for painting the room this week? talk to Antonin and Larissa
    * yoga room team: linnea&clara are taking the lead and continue building the lights today
    * cables & pipes: the insulation will be put on the pipes today after lunch
    * ventilation: no immediate plans for now
    * garden: planting things is next, talk to hue
    * roof window: metal sheets and tiles will be finished on wednesday
    * [kito] organisational: short informal exchange in the evening during dinner? sleeping place preferences sheet?
        * yes, thanks kito!
* [chandi] gettig rid of the kanthaus cloud? 
    * I don't want to maintain the instance anymore and if no one else wants to take over, the best solution seems to migrate the data to an "external provider" -> cloud.livingutopia.org
    * Discussion: https://yunity.slack.com/archives/C3RS56Z38/p1652189072534479
    * if no resistance, the transition would start:
        - step 1: everyone (who wants access) needs to have an account on the new cloud: https://cloud.livingutopia.org/apps/registration/
            * please register until sunday :)
        - step 2: group creation & all the files (public,private,photos) will be moved
        - step 3: calendars & contacts will be moved
    * the cloud stays online for all private data, but is then effectively unmaintained (means: no responsible person if something breaks -> no guarantee of uptime or data safety)
* [Larissa] Let's try out "public hours" from 8-10am and 9-11pm in the main bathroom this week!
* [Anja] Sushi dinner: 
    * does it work for the Bauwoche?
    * maybe we can have an anime movie afterwards?
    * task force for inspiration what to roll into the rolls, save some food and shopping -> after come
* [Antonin] silent office tables and chairs in the basement, talk to me if it is a problem if they stay until next week
* [zui] invite people to the Garten Fest on 28th may (next week saturday)
    * [anja] there's flyers to distribute and a task list in the dining room
<!-- check if anyone has a point that didn't speak already -->


### Round 2
* [janina] ~~unpleasant bar atmosphere in the morning: people, tidy up your bottles and glasses!~~ it's summer, let's update our food storage policies:
    * default spot for fruits and veggies stays the food shelf
    * default spot for everything else is the basement
    * just small 'refill' boxes of cheese, spreads, eggs and sausage in food shelf
    * anybody up for introducing the new sorting today?
        * linnea and martin will coordinate with janina
* [chandi] another tech thingy: gitlab (a platform we use) is introducing restrictions for free accounts which doesn't allow us anymore to use it as we did before.
    * people using it, please read this post about how to continue: https://yunity.slack.com/archives/G8JTYBZFD/p1652645094497579


### Round 3
* [janina] workshop about repairing initiatives organized by NDK and happening in piano room on November 4th. similar to [this](https://sebit.info/termine/reparaturrat-f%C3%BCr-dresden). concerns? resistance? otherwise I'd put it in the calendar and be the host of this public event.

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa
* **Tue.:** Anja, Louis
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Zui
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Silvan, kito, chandi, maxime, Clara, Linnea, Hue, Konrad, Antonia, Natascha, Martin, Louis, Janina
* **Tue.:** kito, Antonin, Silvan, chandi, Janina
* **Wed.:** Antonin
* **Thu.:** Antonin, chandi
* **Fri.:** Antonin, Silvan, Louis
* **Open Tuesday:** Antonin, Silvan, Matthias, chandi, maxime
* **Week:** Tilmann, Mariha

### Result
- **Mon.:** Larissa, Antonin
- **Tue.:** Anja, Louis
- **Wed.:** Natascha, Antonia
- **Thu.:** chandi, Maxime
- **Fri.:** Zui, Martin
- **Open Tuesday:** Anja, Clara


## 7. For next week
* 
