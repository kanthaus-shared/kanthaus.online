---
title: Coordination Meeting
date: "2022-09-12"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #260
* Date: 2022-09-12
* Facilitator: Nathalie
* Notary:Silvan
* ~~Mika caretaker: ~~
* ~~Levi caretaker: ~~
* Physical board caretaker: Martin
* Digital calendar: Maxime
* Reservation sheet purifier: Eric
* Present: Maxime, Eric, Nathalie, Martin, Silvan

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/b3bc95eb-47c7-4376-85c7-0974d4873ca1.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.4 people/day (+1.1)
- **⚡ Electricity**
    - usage: 39.9 €/week (⬆️+30%)
    - of that for heating: 3.54 €/week
    - paid: -19.58 €/week
    - ☀️ self produced: 63% (⬇️-1%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 17.31 €/week (⬆️+38%)
    - emissions: 1.3 kg CO₂ₑ/week

### Expenditure



### Income

### Things that happened
#### In or around Kanthaus
- We went to watch a documentary on schools during the GDR
- We watched a documentary on Verkehrswende
- a sharing event
- a evaluation walk

#### Wider world
- A 96-year-old British woman died surrounded by her relatives in Balmoral, Scotland, on Thursday evening. As did many old people in the world, thank you
- war and energy "crisis" is continuing
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* see https://de.wikipedia.org/wiki/Wikipedia:Hauptseite

## 2. This week planning

### People arriving and leaving
* **Mon.:**
* **Tue.:** "The Family" comes back!! whoop-whoop!!, Andrea comes back
* **Wed.:** Martin leaves
* **Thu.:** Eric, Silvan leaves
* **Fri.:**
* **Sat.:**
* **Sun.:** Silvan comes back
* **Mon.:** Antonin comes back
* **Some day:**

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Silvan Volunteer : Absolute Days threshold 116/60 (+93%)
- Nathalie Volunteer : Absolute Days threshold 101/60 (+68%)
- Andrea Volunteer : Absolute Days threshold 74/60 (+23%)
- Eric_WA Visitor : Days Visited threshold 25/21 (+19%)
- Martin Volunteer : Absolute Days threshold 69/60 (+15%)
- Matthias Volunteer : Absolute Days threshold 62/60 (+3%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Maxime Member : 2 days until Absolute Days threshold (180)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * Post-Sunday-Dumpster-Diving Food sorting/washing [Martin]
    * Repark KMW [maxime]
* Tuesday
    * Hausmüll [Eric]
    * 15:00 - 17:00 Open Tuesday []
* Wednesday
    * 15:00 Silvans Evaluation [Fac.:Nathalie]
* Thursday
    * 10:00 Power Hour [Fac.: maxime, DJ: ?]
    * 15:00 Sozialsauna [Nathalie]
    * Repark KMW [maxime]
* Friday
    * Biotonne [maxime]
    * 12:00 Market pickup [Nathalie]
    * start of attic focus weeks
* Saturday
    *
* Sunday

* Next Monday
    * 10:00 CoMe [Silvan]
* Next week summary
    *

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans


## 4. To do
_Newest tasks on top_
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
    * [Antonin] I had a look, the sink does not seem blocked to me - did someone fix it already?
* [x] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down?
* [ ] remount snack kitchen radiator [Martin/Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] Individuals and groups looking for hosts:
    * Steven, for about 2 weeks after mid-September: if no host is found today, I would decline the stay [Nathalie will check with Tilmann again]
    * Anneke, last week of September [Silvan]
    * 9 people for a seminar on self-organized solawi, 4 to 10th November [Martin]

* [Eric] Question about rules for Verschenkeladen, and consistency week to week. Mainly about enforcing only 5 people are allowed in the room at once, and I wonder if we should mention to people to not be in the room for very long if there is a "queue"? Bringing this up because a few people who come to the Verschenkeladen have expressed concerns to me about being uncomfortable with so many people.
    * we do like this 5 people rule
    * Eric takes care of the sign
    * second person for first 15min --> Martin can be there, Eric volunteering this week
    * last time Eric can do this :(
* [maxime] boxes/stuff in public space
    * Silvan: need place for sorting, until thursday everything will be gone, communicate to Andrea about usage of dragon room
    * HZ boxes: postponed until next KH-HZ tour, storing in basement? - It can go to K20-Basement where the scaffholding slept. Looking for Volunteers for moving. Maxime sends a message to Bodhi
* [Silvan] Plans with the Carla-Cargo trailer? - Shed is unuseable now
    * where to store it? not waterproof yet
    * outside with a tarp?
    * a few more weeks in the shed is ok
    * Silvan will open a slack discussion on this topic

<!-- check if anyone has a point that didn't speak already -->

### Round 2


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Silvan
* **Tue.:**
* **Wed.:**
* **Thu.:** Thore
* **Fri.:**
* **Open Tuesday:** Eric

### Unavailabilities
* **Mon.:** Maxime, Nathalie
* **Tue.:**
* **Wed.:** Nathlie
* **Thu.:** Eric, Silvan, Nathalie
* **Fri.:** Eric, Silvan
* **Open Tuesday:** Maxime
* **Week:** Martin


### Result
- **Mon.:** Silvan
- **Tue.:** Nathalie
- **Wed.:**
- **Thu.:** thore
- **Fri.:** Maxime
- **Open Tuesday:** Eric

## 7. For next week
*

