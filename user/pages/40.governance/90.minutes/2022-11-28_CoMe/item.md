---
title: Coordination Meeting
date: "2022-11-28"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #271
* Date: 2022-11-28
* Facilitator: Antonin
* Notary: Janina
* Mika caretaker: Bodhi
* Levi caretaker: Kita
* Physical board caretaker: Martin
* Digital calendar: Maxime
* Reservation sheet purifier: Kito
* Present: Kito, Larissa, Anneke, Martin, Maxime, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/71324e38-4ab0-4783-acd6-a0a83df9b695.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.0 people/day (-1.9)
- **⚡ Electricity**
    - usage: 118.3 €/week (⬇️-4%)
    - of that for heating: 67.5€ (value fixed by Tilmann)
    - paid: 87.69 €/week 
    - ☀️ self produced: 24% (⬇️-3%)
    - emissions: 63 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.64 €/week (⬇️-28%)
    - emissions: 1.3 kg CO₂ₑ/week

### Expenditure
- [Antonin] 36.50€ for stamps
- [tilmann] 8€ for 16A CEE socket
- [tilmann] 37€ for 100m Fensterdichtgummi D-Profil
- [tilmann] 29€ for better heating controller (https://github.com/Egyras/HeishaMon https://www.tindie.com/products/thehognl/heishamon-communication-pcb/)
- [tilmann] 4396€ for attic building material: 3500€ floor, 440€ doors, 300€ drywall, 90€ plaster
    - We bought less Schüttung from Baunativ, it would've been too heavy for the KMW. For K22 we probably need to get another load.

### Income
- 

### Things that happened
#### In or around Kanthaus
- some windows got new sealings
- OSB and drywall arrived
- big attic cleanup
- electricity price will increase to 42,90 ct/kWh from 1.1.2023 (+41%)
- SoLaWi event at D5
- nazi demo in Leipzig

#### Wider world
- Turkey continued to attacked Rojava
- demonstrations in China started against COVID policy and Xi Jinping
- new Iranian revolution is still going strong

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
- Monday: Silvan leaves, Lara comes back
- Tuesday: Bodhi leaves
- Wednesday: Felix arrives
- Thursday:
- Friday: Zui leaves for the weekend
- Saturday:
- Sunday:
- Some Day: kito might leave

### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
It's getting colder, with freezing nights and basically no sun during the day :(

### Evaluations and check-ins
Anneke Evaluation?

- Nathalie Visitor : Days Visited threshold 33/21 (+57%)
- Thore Volunteer : Absolute Days threshold 80/60 (+33%)
- Bodhi Member : Absolute Days threshold 235/180 (+31%)
    - could happen today, get Bodhi in to talk about it
- Silvan Volunteer : Absolute Days threshold 75/60 (+25%)
- Andrea Volunteer : Absolute Days threshold 61/60 (+2%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
    * Monday Food Care [kito, Anneke]
    * 15:00 Bodhi evaluation [Larissa]
* Tuesday
    * 10:00 - 13:00 Attic work session [Tilmann, you?]
    * 15:00 - 17:00 Open Tuesday []
    * 18:00 - Presentation "Wie geht Verkehrswende?" by Bitte Wenden Wurzen @NDK 
* Wednesday
    * 10:00 - 13:00 Attic cleanup, final edition! [Tilmann, you?]
    * 10:00-13:00: Input Lieferketten IT & Textil (not here)
    * evening: 19:00 Punkrocktresen @NDK
* Thursday
    * 7:00 - 15:00 insulation is blown in [Support: Tilmann, Martin til 10, you?]
* Friday
    * yellow bins are collected [maxime]
    * 10:00 Anneke's evaluation [Martin]
    * 7:00 - 15:00 insulation is blown in [Support: Tilmann, you?]
    * 21:00 project updates [Antonin]
* Saturday
    * 11:00 Power Hour weekend edition [Fac.: Anneke, DJ:]
* Sunday
    * 2nd Advent
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* longterm scheduling https://pad.kanthaus.online/KH-plenum#

## 3. Shopping plans
* [Martin] Toilet paper


## 4. To do

_Newest tasks on top_
* [ ] investigate K22 door code failure
* [ ] fix bike repair station instructions
* [ ] treat mold in Ex-food-storage and hipster room
     * Those rooms need a fresh air supply (Zuluft)
     * Some more work would be welcome to finish connecting those rooms to the ventilation system
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]


## 5. Discussion & Announcements
- **put bigger discussions or decision making here: https://pad.kanthaus.online/KH-plenum#**

### Round 1
- [maxime] About temperature in the house
  - It sometimes feels like not heating much despite being high on the radiator: that's a central setting thing right? What are our options there? As many people have been sick in the last weeks, it could be worth considering heating more, if that's an option.
  - The Fansipan seems particularly cold
  - [tilmann] I'll definitely increase the heating water temperature compared to last week; I also ordered a new heating controller that allows better programming than the current one, so I can automate some things I have been doing manually so far (and I often was too late).
      - Rooms only get cozy when they are heated around the clock, because the walls need to get warm as well
      - Keep doors closed, also between warm rooms
      - Radiators heat better when they are clean and not blocked or covered
      - We can install more fans below radiators, that should give them more power - but increases noise level slightly, kicks up a bit more dust and fans need to be plugged in manually
      - Some rooms have too much heat loss, compared to their radiator size; it'll be expensive to get them warm (current examples: Private, Sleep Kitchen, Ex-Food-Storage, Freeshop Lounge, Hipster, Intermediate, Cloud/Dragon)
      - Rooms in the second floor will probably get warmer when the attic insulation is in.
- [tilmann] I'd like to finish some bits in the attic before Thursday and am looking for help!
  - child care in the afternoon
  - Kita pickup
  - support in the attic: drywalling, plastering, woodwork
- [Anneke] When Lara comes back we can make one day a little Lützerath report
- [Janina] I confirmed our ToI time to be in Harzgerode. Details are not yet discussed.
    - [kito] [reminder to add session proposals until 14:30 approximately](https://ukuvota.world/#/app/0bd37663-60a4-2673-240f-a16ba6c96653/collect)
<!-- check if anyone has a point that didn't speak already -->


### Round 2
- [tilmann] We'll get a 350€ BAUHAUS (booh!) gift card to make up for the damage to the OSB panels. It can only be used in an offline shop, and Antonin or Doug offered to go there (in a few weeks). Collecting shopping ideas and proposals here: https://pad.kanthaus.online/bauhau-shopping?edit#
- [maxime] we forgot the KMW reparking :( now we should repark it between thurday and sunday on the thursday side
    - [larissa] when it's freezing the cleaning car doesn't come, so maybe we don't need to.
    - [antonin] we could also park it in front of the rickshaw garage if people just want to drive it there once and then leave it for a few weeks. doug is pushing the selling process, right? so it wouldn't be there forever. but I wouldn't drive it myself.
- [janina] maybe let's have some little christmassy celebration of the insulation on sunday. we could have mulled wine and turn the heating up or something like that...^^
    - also: december 1st ist coming and the advent calendar needs some more love!

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Larissa
- **Tue.:** Anneke
- **Wed.:** Zui
- **Thu.:** Janina
- **Fri.:** 
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** Anneke
- **Tue.:** Martin, Antonin, Maxime
- **Wed.:** Martin
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** Antonin, Maxime
- **Week:** kito

### Result
- **Mon.:** Larissa
- **Tue.:** Anneke
- **Wed.:** zui, Maxime
- **Thu.:** Janina
- **Fri.:** antonin
- **Open Tuesday:** Martin

## 7. For next week
* 
