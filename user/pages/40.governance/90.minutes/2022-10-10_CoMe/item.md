---
title: Coordination Meeting
date: "2022-10-10"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #264
* Date: 2022-10-10
* Facilitator: Larissa
* Notary: Martin
* Mika caretaker: Kita
* Levi caretaker: Kita/Janina
* Physical board caretaker:Doug
* Digital calendar: Tilmann
* Reservation sheet purifier: Lara
* Present: Larissa, Doug, Martin, Lara, Tilmann

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/1b57ece3-bc1b-4937-b5c4-8f9afbb35f9f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.6 people/day (+0.4)
- **⚡ Electricity**
    - usage: 51.06 €/week (⬇️-4%)
    - of that for heating: ~~15.32 €/week~~ 3.5 €/week due to solar
    - paid: -15.2 €/week 
    - ☀️ self produced: 70% (⬆️+3%)
    - emissions: 15 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.19 €/week (⬆️+14%)
    - emissions: 1.1 kg CO₂ₑ/week

### Expenditure
* [tilmann] 103€ for screws, drills, wall anchors, Torx bits and more sealing tape/glue for attic

### Income

### Things that happened
#### In or around Kanthaus
- tax report work for Wandel Würzen started and it's painful!
- project updates!
- vegutopia trip to Brandis
- attic insulation preparation made great progress, almost ready for insulation now!
- new solar display phone :)

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* 

## 2. This week planning

### People arriving and leaving
* **Mon.:** Thore leaves, Silvan comes back, Anneke arrives
* **Tue.:** 
* **Wed.:** kito comes back, Anja comes back
* **Thu.:** Thore comes back, Anja leaves again
* **Fri.:** Kritisches Lehramt-group arrives [kito]
* **Sat.:** 
* **Sun.:** Antonin comes back, group leaves again [kito]
* **Mon.:** kito leaves again
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Tilmann Member : Absolute Days threshold 230/180 (+28%)
- Zui Member : Absolute Days threshold 187/180 (+4%)

 Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Janina Member : 2 days until Absolute Days threshold (180)



### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * Monday food care [Doug, Tilmann, maybe Larissa]
    * Before cooking: Small bean gleaning action [Janina, you?]
* Tuesday
    * Restwaste is getting picked up [not necessary]
    * 15:00 - 17:00 Open Tuesday [Doug]
* Wednesday 
    * 9:00 Tilmann evaluation [Doug]
    * 15:00 - 18:00 [NDK Autumn fair](https://www.ndk-wurzen.de/images/Flyer_Interkulturelles_Herbstfest.png)
    * 20:00 Doug presentation about his trip 
* Thursday
    * 10:00 Power Hour [Fac.: Zui, DJ: ?]
    * 15:00 Sozial sauna [Larissa]
* Friday
    * Bio-waste getting picked up [Martin]
    * 12:00 Market Pickup [maybe Andrea?]
    * 10:30 finance group work day
    * 15:00 - 18:00 foodsharing potato action [Janina, Silvan]
    * 20:00 zui talk in Cloud Room
* Saturday
* Sunday
    * Repark KMW & trailer [by pushing it?]
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

* [Antonin] up to 250€ for getting the carla cargo professionally sanded and powder-coated, to prevent its steel from rusting further. Powder coating is the way frames are "painted" in bike factories, it's proper. :sunglasses: I (or someone else) would cycle it to a workshop in Leipzig, disassemble it and prepare the parts onsite.
    * let's do it!
    * [tilmann] should add bumpers around edges afterwards

## 4. To do

_Newest tasks on top_
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down?
* [ ] remount snack kitchen radiator [Martin/Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] volunteers and members, please fill [the poll for the WoI](https://framadate.org/yiA8a8yG81L9bLMu)! If you do not want any WoI, you can indicate that you are never available :) 
* [Janina] Visitor requests:
    * Steven who already wanted to come in September now asks for November
    * [David](https://www.workaway.info/en/workawayer/DavidXplorer/overview) for November as well, would potentially stay for winter
    * [zui] resistance for more cis guys atm.
       * [Antonin] wants to talk about that
       * -> social sauna this week?
* [tilmann] would be interested in getting the attic insulation contract signed this week, what steps are necessary?
    * for reference: https://yunity.slack.com/archives/G7E8RPFMH/p1664102707025309
    * [zui] finance team checked and it's ok to pay from hkw bank account
    * [tilmann] thank you!
* [zui] hey all, I'm not at all well at the moment and therefore not so much on slack. I would like to participate in come again but don't have time the next half year at this time. pls help me looking for a solution and we could talk about that on friday at 8 pm (preferd without kids) in the cloud room?
    * it's in the calender
<!-- check if anyone has a point that didn't speak already -->
 
### Round 2
* [Antonin] There is [a poll about the future of the KMW](https://ukuvota.world/#/app/30039c39-9840-301f-f0b0-d70b45d9030f/collect). The proposal phase ends on Sunday.
* [Janina] Harvesting actions this week! :D
    * Beans are done and carrots in progress. We can go whenever and do some classic gleaning.
    * Petzold isn't sure that the beans are very worthwhile, but I'd like to go *today* and check. Anybody wanna come in the afternoon? Coordinates in [this post](https://yunity.slack.com/archives/C76HMDEGG/p1665381231497149).
    * Carrot action can happen anytime this week, the field is only harvested half so far.
    * Would be cool to always wear a foodsharing highvis when goin onto the fields. 
* [tilmann] proposes to get two attic entrance doors, a step towards separating attic and staircase, for improving heating efficiency and fire safety. It would be 751€ to get them delivered here, including frames and door handles (376€ per door) - wouldn't be this week, but maybe still this year.
    * https://www.obi.de/zimmertueren-zargen/wohnungseingangstuer-cpl-weiss-gl223-86-cm-x-198-5-cm-anschlag-l/p/5917091
    * https://www.obi.de/zimmertueren-zargen/zarge-cpl-weiss-seidenmatt-gl223-86-cm-x-198-5-cm-x-14-5-cm-anschlag-l/p/9008723
    * https://www.obi.de/tuerbeschlaege/cmi-rosetten-garnitur-winkel-form-edelstahl-matt-38-mm-42-mm/p/5310222
    * let's do it!

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Lara, Janina
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Zui
* **Open Tuesday:** Doug

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Tilmann, Larissa

### Result
- **Mon.:** lara_wa, janina
- **Tue.:** 
- **Wed.:** 
- **Thu.:** Martin
- **Fri.:** zui
- **Open Tuesday:** Doug

## 7. For next week
