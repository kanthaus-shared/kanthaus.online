---
title: Coordination Meeting
date: "2022-09-05"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #259
* Date: 2022-08-29
* Facilitator: Eric
* Notary: maxime
* ~~Mika caretaker: ~~
* ~~Levi caretaker: ~~
* Alex caretaker: Cille
* Physical board caretaker: Martin
* Digital calendar: Zui
* Reservation sheet purifier: Eric
* Present: Eric, Zui, Martin, Maxime, Jums, Nathalie, Thore

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/70acf399-1dc6-468b-841a-f3e577794839.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 7.3 people/day (-0.1)
- **⚡ Electricity**
    - usage: 30.6 €/week (⬇️-4%)
    - of that for heating: 3.1 €/week
    - paid: -40.54 €/week 
    - ☀️ self produced: 63% (⬇️-2%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.58 €/week (⬇️-15%)
    - emissions: 1.0 kg CO₂ₑ/week

### Expenditure
* FFP2 masks 15 €
* olive oil 18€

### Income

### Things that happened
#### In or around Kanthaus
* We found 90kg of potatoes, from which 60kg could be saved

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* see https://de.wikipedia.org/wiki/Wikipedia:Hauptseite

## 2. This week planning

### People arriving and leaving
* **Mon.:** Larissa comes back today or tomorrow
* **Tue.:** 
* **Wed.:** Martin, Andrea, Alex, Jums, and Cille leave
* **Thu.:** 
* **Fri.:** zui leaves
* **Sat.:** Larissa leaves again (for two weeks)
* **Sun.:** Anja might come, Martin comes back
* **Mon.:** 
* **Some day:**

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Kito Volunteer : Absolute Days threshold 115/60 (+92%)
- Nathalie Volunteer : Absolute Days threshold 94/60 (+57%)
- Thore Volunteer : Absolute Days threshold 90/60 (+50%)
- Andrea Volunteer : Absolute Days threshold 67/60 (+12%)
- Martin Volunteer : Absolute Days threshold 62/60 (+3%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * Post-Sunday-Dumpster-Diving Food sorting/washing? [Eric, Zui]
* Tuesday
    * Repark KMW & trailer [maxime]
    * 15:00 - 17:00 Open Tuesday [Eric]
* Wednesday
    * 18:00 Landgut Nemt pickup [Thore, Maxime]
    * ~~Park KMW & trailer on odd/that sidecht []~~
* Thursday
    * 10:00 Power Hour [Fac. zui:, DJ: ?]
    * 15:00 Sharing Event [Nathalie]
* Friday
    * 12:00 Market pickup [Thore]
    * 15:00 Thore evaluation [Maxime]
* Saturday
    * 
* Sunday
    * ~~Park KMW & trailer on odd/that sidecht []~~
* Next Monday
    * 10:00 CoMe [Nathalie]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

* [chandi] a window for 630€, see discussion

## 4. To do
_Newest tasks on top_
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
    * [Antonin] I had a look, the sink does not seem blocked to me - did someone fix it already?
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put on and off?
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
<!-- check if anyone has a point that didn't speak already -->
* [chandi] I started plans regarding replacing the kitchen window. 
    * More info and discssion on slack: https://yunity.slack.com/archives/C3RS56Z38/p1662199845007619
    * clearification/concerns/resistance please until the 10th :) 
    * some dicussion about the bigger picture
        * more collective decision making on building work
        * is ths embedded in a 'bigger plan' - how does that look?
        * do we see a 'salami tactic' pattern? only asking for small expenses/buildung tasks at a time, no concerns to the indivicual ones, but hard to say no at some point --> bringing up the pattern instead!
        * many buildings sites already (attic)
    Outcome:
    * have a meeting on 'building - the bigger picture' soon
    * ok to do this window now
* [maxime] There are 60kg of potatoes stored in the right bike shed, to finish to dry after being sorted and washed. They could be moved to the basement in some days if that space is needed.
* [Thore] about gleaning beans/onions/potatoes/beetroots: I would not be responsible this year, but I could hand over the responsability to someone else.
* [Jums] today COVID test is slightly positive, Alex is still positive

### Round 2


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Nathalie
* **Tue.:** Larissa
* **Wed.:** zui
* **Thu.:** Thore
* **Fri.:** maxime
* **Open Tuesday:** Eric

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Martin, Jums, Cille

### Result


## 7. For next week
* 
