---
title: Coordination Meeting
date: "2022-01-17"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #227
* Date: 2022-01-17
* Facilitator: kito
* Notary: Janina
* Mika caretaker: Thore
* Levi caretaker: Thore
* Physical board caretaker: Larissa
* Digital calendar:  chandi
* Reservation sheet purifier: Doug
* Present: chandi, Kito, Matthias, Tilmann, Larissa, Silvan, Doug, Nathalie, Antonin, Eumel, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/606af270e324df8cb28591500.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.7 people/day (-0.3)
- **⚡ Electricity**
    - usage: 158.41 €/week (⬆️+15%)
    - of that for heating: 96.29 €/week
    - paid: 139.58 €/week 
    - ☀️ self produced: 12% (⬇️-3%)
    - emissions: 94 kg CO₂ₑ/week
- **💧 Water**
    - paid: 20.51 €/week (⬇️-14%)
    - emissions: 2 kg CO₂ₑ/week


### Expenditure
* 2,50€ for staps
* maybe 50€ for insulation material

### Income
* nothing in the shoe

### Things that happened

#### In or around Kanthaus
* The second Critical Mass happened!
* Ex-food-storage now has automated ventilation!
* A team has be formed to determine thew future of the ex food storage
* the bus party took place
* some people did a 2021 reflection session
* bella made a lot of great signs which are partly put in place now
* More attic insulation progress, K20 street side is half done :)

#### Wider world
* bitcoin mining was officially prohibited for 60 days in cosovo to stabilize the electricity situation
* corona measures in Saxony were lowered and most public places are open again

## 2. This week planning

### People arriving and leaving
* **Mon.:** Antonin arrives, Clara comes back
* **Tue.:** 
* **Wed.:** 
* **Thu.:** Lise arrives or has already arrived, Anja leaves; Gemök group arrives, zui comes
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Anja comes, zui leaves
* **Mon.:** Gemök group leaves, Simon comes
* **Some day:** Bodhi comes

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
There will be snow from Wednesday evening on! It also is quite stormy and cold.

### Evaluations and check-ins

- Clara Volunteer: Absolute Days threshold 111/60 (+85%)
    - [Clara] my evaluation could be Tuesday at 3pm?
- Anja Volunteer : Absolute Days threshold 66/60 (+10%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [kito]
    * blue bin
    * Park cars on even/this side [Antonin]
* Tuesday
    * 07:30 Pfennig-Bau / attic insulation person comes by [Matthias, Tilmann]
    * 10:00 maybe Clara's evaluation [Doug]
    * black bin [Tilmann]
    * After dinner: Kanthaus-Documentary-watching
* Wednesday
    * 13:00 ToI orga meeting @Lantern [Doug, Larissa, Nathalie, Janina]
    * 15:00 K18 session [Clara]
    * Park cars on odd/that side [Matthias]
    * 18:00 Workout Yoga Room
    * after dinner: Aoe2 LANParty [Anja, Matthias]
* Thursday
    * 10:00 Power Hour [Fac.: Antonin, DJ: ?]
    * 15:00 Social Sauna [Janina]
* Friday
    * Organic waste [Doug]
    * Finances day [Larissa, zui, chandi, bodhi]
* Saturday
    * 10:30 Bitte Wenden Brunch @Unkraut's [Janina, Clara]
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Doug] Spraypaint (Black, Blue, Yellow, possibly brown) and primer: to make bins that don't need labels.
  * [tilmann] K22 basement has some spray paint, did you check?
  * [Doug] I will, but just want pre-approval for whatever is missing
* [kito] Oats and frying oil
* [ToI team] ~900€ for stay at external place

## 4. To do
_Newest tasks on top_

* [x] Fix upper staircase toilet window (means: cutting a piece of glass and "glueing" it in) [doug,chandi]
* [x] Fix hipster room window curtain [Larissa, Anja, Mika]

* [ ] Clean K18 garden

* [?] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] Electrically disconnect the ceran stove on the right kitchen side
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door


## 5. Discussion & Announcements

### Round 1
* [chandi] RFID tags, useable with our K20 front door, are finally ready 🎉 :)
    * about tags
        * distribution after come
        * they are always personalized, keep the one you get
    * more people should be able to create and register tags -> skillshare at some point
    * [Larissa] Are those also meant for visitors?
        * [chandi] There will be a limited amount for use by visitors on a special shelf in the office
* [matthias] Also, you can open the door over the wifi with your phone or computer
    * Maybe it's useful alternatively? Also ok to prefer tags :-)
    * You get a personalized QR code to configure your phone
        * Don't share that!
    * more people should be able to create and register QR codes -> do people want a skillshare?
    * [Doug] to both points: are there security issues people should be aware of? Are likely risk-vectors accounted for? (Also, perhaps remove)
* [Doug] Toilet Reading Sale! All reading material from all toilet that isn't attached to the walls, take out what you'd like to read. Rest into vortex. Elephant room? All day today?
    * [kito] add newspapers from dining room?
* [janina] childcare: mika kita pickups from Wednesday on at 14:05! please sign up if you wanna help out! :1234:
    * more childcare is needed on Wednesday, Saturday and during cooking shifts, please raise a hand if you're willing to participate then Janina will approach you
* [Anja] there is looooots of food to be washed!
* [clara] Reminder: GemOek group coming Thursday afternoon till Monday Midday (One person until Monday evening)
    * 7 People in total
    * they are all vaccinated and will test in addition at the beginning and at the end of their meeting
    * they wouldn't isolate themselves from us, except there is the wish?
    * I would reserve Yoga Room, Hipster Room and Ex-Foodstorage for them. OK?
        * jati might want the foodstorage, will talk to clara
    * They would participate in communal cooking
        * usually no communal cooking on the weekends - do we want to have additional structure here?
    * Are there any more needs/wishes from Kanthaus?

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Doug] Printers in shelf, k20-1 entrance. Let's get rid of them! Any we want to keep should be labeled with name, purpose and dated 2022 before next CoMe. I would put rest in Freeshop or Trash (i.e. skip vortex)
    - [matthias] Actually most of the stuff there has a "dedicated" purpose :-( But I am happy to label that. Understand that they could be stored somewhere less accessible and maybe 1-2 printers can actually be sorted out.
- [Larissa] Let's have a duvet and pillow covers sale! The shelf in the communal closet is overflowing + there are a lot small ones which we don't really use anymore, so let's reduce the amount we have. Maybe wednesday all day?
    - [matthias] For "comfort size" (215-225 x 150-160) and "normal size" (130-140 x 195-205) we don't have too many spares, e.g. don't sort any of those out except broken please. Despite I would be in favor of having a better sorting for the pillow covers :-)
- [janina] The fairteiler in Wenceslaigasse is a miraculous food distributor! Let's use it more when we have an abundance! How about putting a bag next to the door for people to bring there when they take a walk?
    - General appreciation, will be tried out
    - Bag needs a label and a date
- [chandi] There is now a proposal on how to act, when we get a positive corona case in kanthaus, called *PCCP*
    - https://pad.kanthaus.online/corona-process
    - motivation + discussion: https://yunity.slack.com/archives/C3RS56Z38/p1642288612062700
    - Please test yourself, especially when you have slight symptomes (coughing, cold, headache, fatigue, throat scratching, etc...) 


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa, Silvan
* **Tue.:** Clara
* **Wed.:** Matthias
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** kito, Thore, Nathalie
* **Tue.:** kito, chandi, Nathalie, Tilmann
* **Wed.:** Antonin
* **Thu.:** Janina, Antonin
* **Fri.:** Thore
* **Open Tuesday:** kito, Tilmann, Janina, Antonin, Nathalie
* **Week:** chandi

### Result
* **Mon.:** Larissa, Silvan
* **Tue.:** Clara, Antonin
* **Wed.:** Matthias, Janina
* **Thu.:** Kito, Doug
* **Fri.:** Tilmann, Nathalie
* **Open Tuesday:** Thore, Eumel
