---
title: Coordination Meeting
date: "2021-11-01"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #216
- Date: 2021-11-01
- Facilitator: zui
- Notary: Matthias
- Mika caretaker: Thore
- Levi caretaker: Janina
- Physical board caretaker: 
- Digital calendar: Antonin
- Reservation sheet purifier: Doug
- Present: Antonin, Damien, Colette, Charly, Tilmann, Silvan, Saranke, Konrad, Maxime, Änsky, Chandi, Zui, Doug, Anja, Janina, Guido, Nathalie, Matthias

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_8ea1eb054b4a8f7c580d77c55e241049.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 19.9 people/day (-5.3)
- **⚡ Electricity**
    - usage: 12.26 €/day (⬆️+26%)
    - paid: 5.06 €/day 
    - ☀️ self produced: 50% (⬇️-12%)
    - emissions: 35 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.42 €/day (⬇️-34%)
    - emissions: 2 kg CO₂ₑ/week


### Expenditure
- [chandi] 32€ for KMW repair (zündkabel & exhaust sealing compound)
- [chandi] 20€ for ~300 yellow bags
- [matthias] 15€ for KMW parts + Cooler liquid
- [maxime] 15€ Bauschutt

### Income
- None

### Things that happened
- 2 roof windows have been built in
- lots of roof work by lots of amazing people
- heatpump arrived with a funny adventurously KMW journey and has been installed already
- a singing session took place
- the washroom now has a nice screen mounted above the bathtub
- The bike shed again has a communal spanner set
- nice sauna afternoon/evening happened
- nice game and chilling nights have happened
- a football morning without injuries happened
- metal drill bits are now nicely sorted
- lake has been visited and swam in :-)
- people spent nice time in Leipzig
- Scaffolding was used as an extended sleeping spot because nobody heard the doorbell :D
- Social sauna has happened in the park

## World
- G20 meeting and the climate conference in glasgow are happening, probably without any hope
- a lot of stupid german politicians got released

## 2. This week planning

### People arriving and leaving
- **Mon.:** 
- **Tue.:** Andrea, Änsky leaves
- **Wed.:** Andrea, Änsky come back, Anja leaves
- **Thu.:** Colette and Damien leave
- **Fri.:** Larissa, Silvan, Änsky leave
- **Sat.:** 
- **Sun.:** Silvan, Änsky come back
- **Mon.:** 
- **Some day:** Zui leaves

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

mainly cloudy and a bit colder; a bit of rain. Nicer towards the weekend

### Evaluations and check-ins
_paused for roof month_
 - check-ins?
     - Änsky [chandi]
     - Saranke [zui]
     - Konrad [Doug]
     - Charly [Antonin]

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [zui]
    * 11:00 Daily coordination meeting []
    * 18:00 Daily coordination meeting
- Tuesday
    * 18:00 Daily coordination meeting
- Wednesday
    * repark cars [Maxime]
    * 18:00 Daily coordination meeting
- Thursday
    * 10:00 Power Hour [Fac.: Doug, DJ: ?]
    * 18:00 Daily coordination meeting
- Friday
    * yellow bins [Konrad]
    * 12:00 Market Pick-Up [Andrea]
    * 18:00 Daily coordination meeting
- Saturday
    * 
- Sunday
    * 
- Next Monday
    * 10:00 CoMe [chandi]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [matthias] ~~30€ for spanner set for bike shed: https://www.contorion.de/p/proxxon-slim-line-ring-maulschluesselsatz-15-tlg-6-21-mm-55142153 (see https://yunity.slack.com/archives/C3RS56Z38/p1634651707342400)~~
     - [antonin] has already got a 9mm C-O spanner of the same brand
     - [silvan] and don´t he have a huge amount of unsorted used spanners in the workshop?

## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] bringing KMW Trailer to TÜV within this month
* [ ] secure k22-2 bathroom sink
* [ ] fix K22-2 toilet flush pipe
* [ ] KMW: Fix the water leak under the front shield
* [ ] KMW: Fix cooling pipe so it does not touch the hot exhaust
* [ ] fix hipster room window curtain

### Medium priority
* [ ] more storage spaces for visitors
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen and kitchen - new solution
* [x] reinforce hook for laundry rack K22-1# [Antonin]
* [ ] lubricate door hinges (which?)
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] install a light switch (or detector) in the washroom

## 5. Discussion & Announcements

### Round 1
- [antonin] can we add the daily coordination meetings to the digital calendar too?
    - [janina] good idea! that will give us notifications!
- [matthias] I found two not properly closed windows in the mornings recently (Friday: Elephant, Sunday: Kitchen). Please close the windows properly: E.g. open the bottom lever first, close the window and see that it fully touches the frame on all 4 sides, properly close all (e.g. depending on window type 1 or 3) handles with quite a bit of force.
- [tilmann] cancel gas contract now to save monthly fee?
    - [matthias] + tell chimney cleaner that we don't have any anymore :-)
    - Larissa will look into doing it
- [janina] house tour today: i can not do it before ~4 pm. is that okay or can somebody else do it earlier? it's for charly, colette and damien.
    - 4pm is fine
- [silvan] someone destroyed the curtain in hipster room, it would be great if this person or someone else would repair it. The TV-curtain will be damaged soon by using it for the window.
    - has been added to ToDo list

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [matthias] Heating talk? (e.g. which rooms to heat)
    - (currently, it is too warm [regarding to matthias] in Kanthaus - this will change when the heating setup is finished in a few weeks latest)
    - some people are interested, smallgroup. Maybe after building month
    - by default, now we heat Piano room, Elephant room, Kitchen, Dining room, Office, Snack kitchen and K20-2 bathroom
    - there will be more documentation and presentation about the heating system in a few weeks when it's properly done
    - there will be more monitoring including heating cost statistics in CoMe soon again
- [doug] party comitee for celebrating the end of the building month?
- [chandi] Should we do the extended task lottery or normal task lottery this week?
    - [janina] i found that it was much cleaner and tidier last week than the week before and would attribute that to the extended lottery, so i'd be in favor of doing it again. but we'd definitely need a physical version of the outcomes that can be put below the board in the dinig room to not forget when we have shifts.
        * [chandi] I agree, but see the reason that it was cleaner simply in the fact, that the tasks got done compared to the week before, rather due to the way of distribution
    - [chandi] Issues i see with the extended task lottery: 
        * Distribution difficult (some cooking twice per day + cleanup, some don't cook at all)
        * how to handle people not present at CoMe?
        * why some reprodutive tasks are covered and some not?
        * People get the feeling, they do enough, maybe leading to people feel less responsible for other reproductive tasks, at least making them less visible
        * How can this be made just and not just numerically even distributed?
    * Lots of discussion, some people resist the task lottery (like this), some resist to not have it
    * Suggestion to have a list where cooking and cleanup is distributed and everybody is encouraged to sign up three times
    * List will be put a day before CoMe in the future and we check it is filled up in CoMe


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
 offline solution this week
