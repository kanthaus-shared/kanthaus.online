---
title: Coordination Meeting
date: "2022-10-31"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #267
* Date: 2022-10-31
* Facilitator: zui
* Notary: kito
* Mika caretaker: Tilmann
* Levi caretaker: Tilmann
* Physical board caretaker: Andrea
* Digital calendar: Zui
* Reservation sheet purifier: Martin
* Present: Lara, Martin, Larissa, Andrea, Silvan, Janina, Zui, Kito

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2a81e35a-1b8c-47ee-8735-3964a6dd9fd9.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 7.0 people/day (-9.9)
- **⚡ Electricity**
    - usage: 56.36 €/week (⬇️-7%)
    - of that for heating: 5 €/week
    - paid: 9.47 €/week 
    - ☀️ self produced: 62% (⬆️+7%)
    - emissions: 19 kg CO₂ₑ/week
- **💧 Water**
    - paid: 17.39 €/week (⬇️-23%)
    - emissions: 1.4 kg CO₂ₑ/week

### Expenditure
- nope

### Income
- maybe a bit in the shoe

### Things that happened
#### In or around Kanthaus
- visit from leipzig house project people
- digital selfdefence session
- people went to queer helloween partys on the weekend
- sommerly weather
- several people went to critical mass in leipzig
- several parents visiting
- rainwater collection fixed again

#### Wider world
- Lula won Brazilian elections yesterday
- many people died in a Halloween party in South Korea
- a lot of cocaine was found in Laras city
- still pretty big demonstrations in Iran
- gas prices decreased for a short period
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** Martin arrived, kito leaves, Andrea also leaves
* **Tue.:** Lara leaves, her family visits before for some hours
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 9 lilaluzis arrives, Larissas friends arrive
* **Sat.:** kito comes back, Lara comes back, Anja comes for a night
* **Sun.:**
* **Mon.:** Larissas friends leave
* **Some day:** 

### Weather forecast
- it gets colder, maybe a bit of rain towards the end of the week, but the most sun will be on saturday
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
- Zui Member : Absolute Days threshold 208/180 (+16%)
- Larissa Member : Absolute Days threshold 181/180 (+1%)

:clock1130: Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Lara_WA Visitor : 4 days until Days Visited threshold (21)
- Anneke_Martin Visitor : 5 days until Days Visited threshold (21)
- Nathalie Visitor : 5 days until Days Visited threshold (21)

:ghost: Absent:
- Anja Volunteer : Absence threshold 126/90 (+40%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * Monday food care [kito, Martin, Janina (sorting)]
* Tuesday
    * 15:00 - 17:00 Open Tuesday [Lara]
    * 17:00 swimming pool [Larissa, Mika]
* Wednesday
    * 17:00 "Stadt im Gespräch (talk about future of Kulturcafe and other nice things happening next year in Wurzen @D5) [maybe Andrea, Janina, Doug]
* Thursday
    * 10:00 Power Hour [Fac.: Larissa, DJ: maybe Janina]
    * 14:00 zui evaluation [Doug]
    * 18:00 Bündnis Sozialproteste (planning something to counter right wing demonstrations) at ? [maybe Zui, Larissa]
* Friday
    * 12:00 Market Pickup [Martin]
    * 16:00 - 19:00 Workshop Reparaturräte (how to promote more urban repair culture in Wurzen)
    * Solawi group arrives 
* Saturday
    * 11:00 Project updates [Martin]
    * 19:00 "say hello to group" dinner [Janina]
    * yellow bins get collected [Martin]
* Sunday
* Next Monday
    * 10:00 CoMe [kito]
* Next week summary
    * friday plenary
    * 2 bins!!

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* longterm scheduling https://pad.kanthaus.online/KH-plenum#

## 3. Shopping plans
- [tilmann] 4200€ attic floor components, for K20+K22
    - 2500€ OSB https://www.bauhaus.info/verlegeplatten/osb-verlegeplatte-palette/p/28599908
    - 70€ glue https://www.bauhaus.info/holzleim/ponal-fugen-leim-parkett-und-laminat/p/15350645
    - 260€ Trittschallschutz / noise insulation https://www.baunativ.de/product_info.php?products_id=9060
    - 1100€ Schüttung / filling material https://www.baunativ.de/product_info.php?products_id=840
    - 125€ Rieselschutz / dust seal layer https://www.bauhaus.info/trockenschuettung/fermacell-rieselschutz/p/13862946
    - 65€ Randdämmstreifen / side noise protection https://www.baunativ.de/product_info.php?products_id=10862
    - note: I might do the Bauhaus shopping soon, together with the fire doors mentioned last CoMe. The Baunativ stuff we could get ourselves from Oschatz if the KMW is still operational, otherwise get it shipped.
- [tilmann] 281€ Feuerschutzplatten GKF / fire protection drywall for attic staircases https://www.bauhaus.info/feuerschutzplatten/knauf-feuerschutzplatte-piano-gkf/p/13878370
- --> decisions postponed to big plenary next Friday

## 4. To do

_Newest tasks on top_
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down? - why?
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements
- **put bigger discussions or decision making here: https://pad.kanthaus.online/KH-plenum#**

### Round 1
- [zui] next weeks plenary https://pad.kanthaus.online/KH-plenum#
- [janina] about the group coming on friday
    - 9 people educating themselves about solawi while already working in such
    - they stay until thursday next week
    - please empty backpack and short-term storages as much as possible
    - would be cool to organize dinners on the weekend as well
    - dragon+cloud would be a great alternative to yoga+hipster
    - they are expecting to be getting communal + another private additionally
    - maybe we use the sleeping spot sheet for the weekend?
    - please open the door on friday if the bell rings :) 
- [kito] FFJ-process -> any wishes?
    - is it a Collective Agreement?
    - talk about it in the meeting (KH-plenum), afterwards start the Collective Agreement process
<!-- check if anyone has a point that didn't speak already -->
 
### Round 2
- [janina] potential gleaning this week, since beetroot is ready. didn't reach petzold yet. will keep you posted. which day would fit interested people?
    - is weekend possible?
    - just starting, could happen next week
    - Silvan is interested (not thursday), Martin and Larissa also kind of
    - Janina will keep us updated
- [janina] anybody up for dd today? :)
- [zui] digital calendar working really badly, whats the plan with the nextcloud

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** kito
- **Tue.:** Silvan
- **Wed.:** Zui
- **Thu.:** Larissa
- **Fri.:** Martin
- **Sat.:** Janina
- **Sun.:** 
- **Open Tuesday:** Lara 
