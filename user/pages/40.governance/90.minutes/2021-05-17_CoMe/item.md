---
title: Coordination Meeting
date: "2021-05-17"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #191

- Date: 2021-05-17
- Facilitator: Doug
- Notary: Clara
- ~~Mika caretaker:~~ No Mika! :/
- Physical board caretaker: Antonin
- Digital calendar: Matthias 
- Reservation sheet purifier: Nathalie 
- Present: Clara, Matthias, Jums, Talita, Andrea, Antonin, Nathalie, Thore, Silvan, Doug

----

<!-- Minute of silence (?) -->

### 0. Check-in round

### 1. Last week review
##### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_91148e3558cd336271e77bebb927d3fa.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.6 people/day (-0.6)
- **⚡ Electricity**
    - usage: 7.77 €/day (⬆️+2%)
    - paid: -1.38 €/day 
    - ☀️ self produced: 65% (⬇️-10%)
    - emissions: 7 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.85 €/day (⬆️+3%)
    - emissions: 2 kg CO₂ₑ/week
- **🔥 Gas**
    - usage: 0.0 €/day (⏬-100%)
    - emissions: 0 kg CO₂ₑ/week

##### Expenditure
* [maxime] Bauschutt: 10€
* [Doug] Aqua-fermit: €5 (to fix pipe)
* [Antonin, Doug] Blech und Sturmklammern: €142
* [Antonin, Doug] disposing of DDT wood, 71€
* [Antonin] new angle grinder blade with fancy holes and shiny diamonds, 76€
* [Bodhi] Spüli, oil, FFP2 masks, 40€
* [Matthias] 55€ for different air quality sensors

##### Income
* 290€ donation
* 820€ donation
* 5€ donation
* 20ct shoe
 
##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 4715
- µCOVIDs used last week: 5698
- µCOVIDS balance from last week: -983
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 4017
- µCOVIDs/person mean for this week: ~400

##### Things that happened
- summer arrived
- garden was cleaned and trash was taken away, trash bins moved to original position,...
- there was a bird that might want to move into Kanthaus
- a lot of furniture and stuff arrived from Bennewitz (and left again)
- many lovely plants arrived from Leipzig
- many windows appeared in the roof
- there was a knowledge sharing session on musical theory
- Spontaneous thunderstorm party 
- A Lan Party happened
- We had a game night
- We had two association meetings, a bufdi meeting and a foodsharing Wurzen meeting 
- the smallest evaluation ever took place (only evaluee and facilitator)
- Friday was roof day!

### 2. This week planning

##### People arriving and leaving
- **Mon.:** Silvan leaves
- **Tue.:** Lise leaves, Jums leaves to the North, Maxime leaves to the South
- **Wed.:** Silvan comes back
- **Thu.:** 
- **Fri.:** Anja arrives
- **Sat.:** Larissa comes back, Hannah arrives
- **Sun.:** 
- **Mon.:** Anja leaves
- **Some day:** Maybe NaTho leave for one night on the weekend; Chandi/Zui on Wednesday/Thursday?, Romii arrives Mid-Week

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

Stable weather, some thunderstorms and wind; 15 degrees during the days, but it will get hotter at the end of the week.

##### Evaluations and check-ins
- Doug Member : Absolute Days threshold 198/180 (+10%)
- Matthias Member : Absolute Days threshold 182/180 (+1%)

##### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [Doug]
    - ~~15:00 Sharing Event~~ [] 
- Tuesday
    - 11.00 Dougs Evaluation [Matthias]
    - 14:00 KritMa round [Antonin]
    - 15:00 Closed Tuesday! [Doug]
- Wednesday
    - 11:00 Skillshare Videos/Kanthaus Video [Andrea]
    - 15:00 Matthias Evaluation [Doug]
- Thursday
    - 10:00 Power Hour [Fac.: Clara, DJ: No DJ!]
    - 14:00 Begin of Yoga Room Action (until Friday evening) [Nathalie, Talita]
    - ~~14:00 Knowledge Sharing:~~
        <!-- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
- Friday
    - Gelber Sack
- Saturday
- Sunday
- Next Monday
    - 10:00 CoMe [Antonin]
    - 15:00 Anjas evaluation [Clara]
- Next week summary

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)


### 3. Shopping plans
- [Doug] [Dualit 6-slice toaster](https://www.ebay.de/itm/353493419761?epid=27029693074) ~€60-120 :sweat_smile:
- [Antonin] cement and lime, ~15€
  - [matthias] there is ~40kg of lime in the basement stored in the potato storage room in the blue barrel
- [Silvan] paint for yoga room 

### 4. Discussion & Announcements

#### Round 1
- [Doug] Suggest Wednesday 26th May for scaffolding takedown. (provisionally in calendar)
    - We'll do it. 
- [Antonin] Samuel would like to visit from 3rd to 8th July from Fance
    - Great. 
- [Talita] Yoga room action on Thursday and Friday 20-21 May.
    - chimney tear down 
    - sanding walls
    - Everyone is invited to join, feel free to talk to NaTaSil before if you want to know what will happen
    - it's dusty but easy work 
- [Silvan] Corona Framework: How to continue the process with testing? (Vaccination needs to be added in printed out Version)
    - We will talk about it at 11.15 and see what's missing to implement the ukuvota proposal in our framework. 
- [Andrea] Video about Kanthaus
    - Workshop/Skillshare event on Wednesday at 11:00. 
- [Thore] KMW trip saturday and sunday
    - Are there things that need to go to Harzgerode?
    - KMW is available.
- [chandi] romii would like to come to kanthaus again for a week from We/Th on. Would it be OK and can someone imagine being host for the Wednesday if I come a day later?
    - Talita can host him for Wednesday and later if needed

#### Round 2
- [Doug] Would someone like to fix the pipe with me?
    - Just approach Doug if you are interested, it could be a skillshare.
- [Talita] Brieuc from WA wants to visit Kanthaus for 2 weeks soon 
    - Talita would be interested in hosting him. 
    - Would be nice, only concern would be Corona. Talita will ask him about that and then announce again, when he would come. 
- [Silvan]
    - short garden planting session today, would be happy if someone joins.

#### Round 3
- [Doug] Food to wash!

### 5. [Task lottery food planning](https://kanthaus.gitlab.io/task-lottery/)

#### Volunteers
- **Mon.:** Doug
- **Tue.:** Thore
- **Wed.:** Clara
- **Thu.:** Matthias
- **Fri.:** Antonin
- **~~Open Tuesday~~:** 

#### Unavailabilities
- **Mon.:** Antonin, Matthias, Clara, Thore
- **Tue.:** Clara, Antonin
- **Wed.:** Antonin, Matthias
- **Thu.:** 
- **Fri.:** Thore
- **~~Open Tuesday~~:** 
- **Week:** maxime, andrea, Talita, Nathalie, Silvan, Jums


### 6. For next week
- 
