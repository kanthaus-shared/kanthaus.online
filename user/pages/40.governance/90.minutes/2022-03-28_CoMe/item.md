---
title: Coordination Meeting
date: "2022-03-28"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #237
* Date: 2022-03-28
* Facilitator: Janina
* Notary: chandi
* Mika caretaker: maxime
* Levi caretaker: maxime
* Physical board caretaker: Matthias
* Digital calendar: Doug
* Reservation sheet purifier: Anja
* Present: chandi, Nathalie, Doug, Matthias, Silvan, Anja, Janina, Tilmann, Prairie

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf482d.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.4 people/day (+0.9)
- **⚡ Electricity**
    - usage: 45.37 €/week (⬇️-37%)
    - of that for heating: 45.02 €/week
    - paid: -15.6 €/week
    - ☀️ self produced: 36% (⬇️-10%)
    - emissions: 21 kg CO₂ₑ/week
- **💧 Water**
    - paid: 21.24 €/week (⬆️+10%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [Antonin] 5.97€ (oats, toilet paper)
- [kito] 73.75€ (oats)
- [Silvan] 24,50€ for stamps

### Income
- 19.48€ (pfand)
- 100€ for some heating system parts that have been sold
- 50€ for KMW usage


### Things that happened
#### In or around Kanthaus
* sun
* radishes are sprouting
* a pizza got ordered o.O
* new ventiltion hole in the cave got drilled
* FINTA* tech meet up in leipzig
* antonin worked on the automatic water counter reading
* more attic work
* nice casual project updates in the garden
* matthias became volunteer
* apple tree pruning in doug's garden
* book club at villa klug
* concert in the spitzenfabrik
* AoE LAN party

#### Wider world
* Last days of wikidata data reuse days
* time changed
* ukraine war continues
* saarland elections, GRÜNE and LINKE are out, SPD won


## 2. This week planning

### People arriving and leaving
* **Mon.:** Larissa comes back, kito comes back
* **Tue.:** kito leaves for one month, Andrea comes back
* **Wed.:** Anja leaves
* **Thu.:** Maxime leaves from some weeks
* **Fri.:** Larissa, Silvan and Mika leave, Charli + 2 come ??, Prairie leaves
* **Sat.:** Thore, Nathalie leave for a week
* **Sun.:** Larissa and Mika come back
* **Mon.:** Silvan comes back
* **Some day:** Zui comes back towards the end of the week (thu, Fr)


### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_good wheather will be over from tomorrow on. the sun will stop and rainy, will be coooold_


### Evaluations and check-ins
- Larissa Member : Absolute Days threshold 188/180 (+4%)
- Kito Volunteer : Absolute Days threshold 61/60 (+2%)
- Janina Member : 2 days until Absolute Days threshold (180)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
    * 11:30 Unload KMW [chandi,doug, prairie]
    * 16:00 Lake trip [Matthias]
    * Park KMW and trailer on even/this side [Silvan]
* Tuesday
    * Rest waste [tilmann]
    * 16:00 Bitte wenden co-working @main office [Janina]
* Wednesday
    * 15:00 Focus group sustainability Wurzen @ Alm
    * 17:00 Workout @yoga room [Mika]
    * Park KMW and trailer on odd/that side [chandi]
* Thursday
    * 10:00 Power Hour [Fac.: Janina, DJ: chandi]
    * 15:00 Social sauna [chandi]
* Friday
    * Organic waste [doug]
    * 10:00 Attic skillshare & work session [Tilmann]
* Saturday
    * Zui-coming-back-and-chandi's-chosen-birthday-day 🎉
* Sunday
* Next Monday
    * 10:00 CoMe [chandi]
* Next week summary

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit


## 3. Shopping plans
* [matthias] 20€ for replacement spanners for the bicycle repair station

## 4. To do

_Newest tasks on top_
* [ ] make the wall nice in K20-2 bathroom where the boiler has been
* [ ] remove old gas heater from basement to trash corner - needs 3-4 people
* [ ] repair WiFi in K22-2
* [ ] repair Schaukasten
* [ ] Make new signs for the hats/gloves/scarves shelf in the hallway (?)
* [ ] ~~Door-closer for Elephant-staircase door~~
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] we have a request to film a documentary in Kanthaus in April. April is already pretty full for me so I would not want to organize that myself. Anyone interested in taking that over? Otherwise I would decline in the coming days.
    * No one in the room seems motivated to take over
    * Maybe Andrea could be interested?
    * [doug] I'll talk to antonin
    * [Silvan] Also a second request with need of a protagonist
        * [Janina] zui seemed to have some interest
* [Doug] Kanthaus Hoffest on [28th May](https://yunity.slack.com/archives/C3RS56Z38/p1647948010583459)! Any surprise resistance? Let's organise an action meeting this week!
    * no resistance -> decided
    * [janina] I'm interested
* [Janina] Request for FINTA* bike repair workshop. Can anybody imagine doing it or forwarding it to someone?
    * [chandi] I'll take the hat
* [Nathalie] visitor anouncement: my mom comes (30.03-12.04.)
    * staying in the flat, also while I'm gone
    * let's integrate her, if she feels comfortable :)
* [zui] I would like to do a coming-back-party together with chandi's chosen birthday on saturday. it would be an afternoon garden party if the weather is good, and we would invite some people. if weather is bad we would just do an afternoon hangout inside the house with games without inviting many people from leipzig
* [Andrea] greetings from Paunsdorf! on the weekend Charli (the person that I met once on my way back from Berlin) is visiting with two friends! Nono might also come for the weekend!
    * cool!


### Round 2
* [Janina] Building week invitations are on, please also forward in your circles! Also: hosts will be needed! Someone up for e.g. hosting [a nice American](https://www.workaway.info/en/workawayer/Hue) (I know, again...^^') all of May?
    * Subpoint: Plane usage and how I go about it now
        * [Janina] I'll ask whether they stay for at least a month in europe, not just kanthaus -> support from everyone
* [zui] would like to use the KMW for a private matter (to help a friend) from the 9.-11.04. I would drive it to Berlin and back. Is it free in that time and do you have any concerns?
    * no events or other time concerns
    * but we should finally adjust the gas thingy, so that it drives more efficiently
    * chandi communicates to zui


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Anja, Janina
* **Tue.:**
* **Wed.:** Thore
* **Thu.:** Matthias
* **Fri.:**
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** chandi
* **Tue.:** Janina, chandi
* **Wed.:** Janina, Doug
* **Thu.:** chandi
* **Fri.:** Prairie, chandi
* **Open Tuesday:** Maxime, Prairie, Janina, chandi
* **Week:** Nathalie, Maxime, Tilmann, Silvan

### Result
- **Mon.:** Anja, Janina
- **Tue.:** Prairie
- **Wed.:** thore, chandi
- **Thu.:** Matthias
- **Fri.:** Doug
- **Open Tuesday:** Anja

## 7. For next week
*
