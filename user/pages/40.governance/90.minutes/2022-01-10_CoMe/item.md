---
title: Coordination Meeting
date: "2022-01-10"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #226
* Date: 2022-01-10
* Facilitator: chandi
* Notary: kito
* Mika caretaker: Kita
* Levi caretaker: Tilmann
* Physical board caretaker: bella
* Digital calendar: chandi
* Reservation sheet purifier:  doug
* Present: Thore, Janina, Doug, chandi, bella, Larissa, Silvan, Änski, Andrea, Nathalie, kito

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_222dcafddc843e08371b7d0298f8466f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 17.0 people/day (+3.6)
- **⚡ Electricity**
    - usage: 138.1 €/week (⬆️+36%)
    - of that for heating: 75.76 €/week
    - paid: 116.37 €/week 
    - ☀️ self produced: 15% (⬆️+3%)
    - emissions: 80 kg CO₂ₑ/week
- **💧 Water**
    - paid: 23.85 €/week (⏫+128%)
    - emissions: 2 kg CO₂ₑ/week



### Expenditure
* [matthias] 41€ for 4 more comfort size duvet covers and 4 80x80 pillow covers

### Income
* 120€ donation on the account
* 4€ in the shoe

### Things that happened

#### In or around Kanthaus
* toi session proposal meeting took place
* lots of pumpkins, butter and onions were saved
* levi's birthday was celebrated
* we all forgot about the bus riding party
* Larissa forgot about the bio waste
* MCM meeting took place as an informal breakfast meeting -> https://pad.kanthaus.online/mcm-planning-minutes
* mika started to stay at kindergarten for lunch and he could be picked up now from different KH-people
* Kanthaus financial meeting took place
* zui moved out 😭

#### Wider world
* demonstrations in Kazakhstan against corruption. more than few hundred deaths, thousands arrested. russland sent their army to support the government.
* mandatory vaccination gets negotiated, seems quite sure

## 2. This week planning

### People arriving and leaving
* **Mon.:** Anja, Matthias come, Lyra comes(?)
* **Tue.:** Anja leaves, Clara comes
* **Wed.:** 
* **Thu.:** Bella leaves
* **Fri.:** Willy comes, Pia might come
* **Sat.:** Andrea leaves
* **Sun.:** Antonin, Anja come back, Änski leaves, Willy leaves
* **Mon.:** 
* **Some day:** 


### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
it's gonne be cloudy, cold and freezing the whole week, at least a bit sunny on thursday

### Evaluations and check-ins
- *Thore* _Volunteer_ : _Absolute Days_ threshold 131/60 (+118%)
- *Änsky* _Visitor_
- *Bella* _Visitor_ check-in


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Chandi]
    * 16:00 Ex-Food-Storage-Meeting [Bella]
    * Park cars on even/this side [hopefully done]
* Tuesday
    * 11:00 Thore's Evaluation @ flat [Larissa]
    * 18:00 Workout @ YogaRoom
    * 20:00 Fishbowl Soli-Zimmer [Andrea]
* Wednesday
    * 14:00 ToI orga meeting @lantern [Larissa, Nathalie, Janina, Doug]
    * 16:00 Bella's Check-In [Janina]
    * Park cars on odd/that side [Nathalie]
* Thursday
    * 10:00 Power Hour [Fac.: Janina, DJ: ?]
    * 13:00 Änski's Evaluation [Andrea]
    * 18:00 Foodsharing Wurzen meeting @flat [Thore, Nathalie, Janina, you?]
* Friday
    * yellow bin [Änski]
    * 11:00 bus driving parrrtyyyy [Doug]
    * maybe again an Workout Session?
    * 14:00 Year reflection @ Cloud Room [Kito]
* Saturday
    * 13:00 Critical Mass @Marktplatz [Janina, Clara, everyone?]
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [kito]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [doug] 69.40€ Turing Tumble. Fun, interactive 'game' to learn computation fundamentals. https://www.turingtumble.com/
    * not decided, expense request again next week
* [janina] oats and frying oil! - maybe people are interested to do research regarding bigger packages


## 4. To do
_Newest tasks on top_

* [ ] Fix upper staircase toilet window (means: cutting a piece of glass & "glueing" it in) [doug,chandi]
* [ ] Making new signs for the commode in the hallway

* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] Electrically disconnect the ceran stove on the right kitchen side
    * still nessesary? kind of repaired
* [ ] Fix hipster room window curtain [Larissa, Anja]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door


## 5. Discussion & Announcements

### Round 1
* [janina] childcare 1: picking up mika from the kita
    * would be cool to plan this a little beforehand, also because the kita wants to know who's coming on a given day
    * let's use the childcare sheet's 11:00 - 12:00 slot for indicating kita pickups
    * with ebike it's really easy and takes ~20 minutes per way
    * pickup time is between 11:30 and 11:45, eventually shift back to around ~14:00
    * also possible to bring him in the morning
    * on the way beetroot could be picked up in Nemt
* [chandi] Sleeping place sheet
    * will be used this week, write your name down!
    * remember: name in the sheet is not a reservation, just a preference to stay in this room mainly for the week
* [ToI-Team] We are going to have the ToI in Attac Villa!
    * and you still can put proposals for sessions in the pad https://pad.kanthaus.online/toi2022-sessions
* [kito] I and my brother don't have hosts for the weekend. who wants to be it?
    * doug can be it
* [Bella] I would like to fix some of the signs which go of
    * generally very good idea
    * for most of them it's nice to use the "corporate design"
    * for example in the closet you can chose how you want to do it (handwritten, printed)
    * better to have them laminated
    * some of them are not necessary anymore, you can put them in the Vortex
    * importantto renew: Kommode in the hallway, closet
* [Bella] I can imagine to start process for renovating Ex-Food-Storage. Who would like to join?
    * should be people who are actually interested in doing it as Bella doesn't stay for long anymore
    * kito and Änski are interested
    * there will be a small meeting today, 4pm
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [janina] childcare 2: toi training
    * for tilmann and I ToI will only work if basically everyone contributes to childcare once in a while
    * that probably requires some skillshare that is better done before
    * are people up for that? if yes, we can use e.g. the last week before ToI as training time
* [chandi] Group request in the mails: Buwa-Kollektiv Frühjahrs-Klausur-Tagung, 26.2.-2.3.
    * janina already replied them


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Larissa, Thore
* **Thu.:** Andrea
* **Fri.:** Nathalie
* **Open Tuesday:** Silvan

### Unavailabilities
* **Mon.:** änski
* **Tue.:** chandi, Janina
* **Wed.:** änski 
* **Thu.:** janina, kito, doug, bella
* **Fri.:** kito, bella
* **Open Tuesday:** änski, chandi
* **Week:** 

### Result
- **Mon.:** Janina, bella
- **Tue.:** Doug, Änski
- **Wed.:** Larissa, Thore
- **Thu.:** Andrea
- **Fri.:** Nathalie, chandi
- **Open Tuesday:** Silvan, kito


## 7. For next week
* 
