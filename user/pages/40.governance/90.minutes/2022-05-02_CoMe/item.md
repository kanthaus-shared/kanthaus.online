---
title: Coordination Meeting
date: "2022-05-02"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #242
* Date: 2022-05-02
* Facilitator: chandi
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Matthias
* Physical board caretaker: Larissa
* Digital calendar: Zui
* Reservation sheet purifier: Doug + Linnea and Hue
* Present: Doug, Anja, Antonin, Louis_WA, Silvan, Larissa, Linnea_WA, Clara_WA, Chandi, Zui, Hue_WA, Martin_WA, Janina, Maxime, Tilmann

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf4844.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.7 people/day (+3.1)
- **⚡ Electricity**
    - usage: 22.4 €/week (⬇️-37%)
    - of that for heating: 30.58 €/week
    - paid: -41.53 €/week 
    - ☀️ self produced: 49% (⬇️-5%)
    - emissions: 9 kg CO₂ₑ/week
- **💧 Water**
    - paid: 13.26 €/week (⬆️+10%)
    - emissions: 1 kg CO₂ₑ/week

### Income
* no income

### Expenditure
* 

### Things that happened

#### In or around Kanthaus
* in-house pandemic is finally over!
* many visitors arrived for the start of the building weeks
* a new table area with semi-permanent pavillon was created in the garden
* spring is here: the lawn was mown
* nice outside social sauna with many topics
* talk about verkehrswende (transportation transformation) in Leipzig
* anarchist days in Leipzig

#### Wider world
* 1th of May with demonstrations everywhere
* heat waves in india & pakistan with lots of people dying
* turkey attacked kurdistan
* france: left-green alliance was created for the legistlative elections, which could result in a push away from neoliberalism in the EU


## 2. This week planning

### People arriving and leaving
* **Mon.:** Matthias leaves for a week
* **Tue.:** 
* **Wed.:** Anja leaves, Lea arrives
* **Thu.:** kito&Lucía arrive, Martin_WA leaves
* **Fri.:** kito&Lucía leave, Lea leaves, maybe Silvan and Larissa leaves for the weekend
* **Sat.:** 
* **Sun.:** kito, Lucía, Marita and Smilla arrive
* **Mon.:** Julë arrives, Martin_WA comes back
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_around 17-18°C and sunny the whole week. a bit colder and slight rain towards the end._

### Evaluations and check-ins
- *Larissa* _Member_ : _Absolute Days_ threshold 223/180 (+24%)
- *Silvan* _Volunteer_ : _Absolute Days_ threshold 66/60 (+10%)
- *Anja* _Volunteer_ : _Absolute Days_ threshold 63/60 (+5%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [chandi]
    * 11:30 Building weeks starting plenum @piano room [Janina]
    * 14:00 House tour: the opening @dining room [Janina]
    * 17:30 House tour: the sequel @garden [Andrea]
    * repark KMW & trailer to even/this side [Zui]
* Tuesday
    * 10:00 moving furniture out of the silent office [Antonin]
    * 11:00 Larissa's evaluation [Maxime]
    * 15:00 - 17:00 Open Tuesday [Anja]
* Wednesday
    * Yoga room work starts [Silvan]
    * Park KMW & trailer on odd/that side [Doug]
* Thursday
    * 10:00 Power Hour [Fac.: Janina, DJ: ?]
* Friday
    * yellow bin [Louis]
    * 12:00 Market pickup [Janina, 
    * Hue]
* Saturday
    * Georg-Schwarz-Straßenfest @ Leipzig
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do
_Newest tasks on top_
* [ ] fix garden pavement stones
* [ ] make the wall nice in K20-2 bathroom where the boiler has been
* [ ] remove old gas heater from basement to trash corner - needs 3-4 people 
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] Light: special bikes shed
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] where to store the furniture of the silent office during the building weeks? some ideas: dragon room, back of the private bike shed, the cave, K20 basement where the scaffolding is stored (because some of it will go to HZ soon), outside under a good tarp?
    * [Doug] definitely stack vertically!
    * [chandi] basements should be okay for some weeks -> there is space in K22
    * [zui] then we need to finish quickly so that we can have a bassment party for the end of the building weeks..^^
* [Janina] Repro orga for this week: Presentation of reworked sheet and invitation to take part!
* [Tilmann] let's wear name tags during the next days, for the building week
* [Doug] Constitutional amendment: I'll push it soon to an ukuvota.


<!-- check if anyone has a point that didn't speak already -->
### Round 2
* [Janina] Another visitor request for building weeks: Natascha is looking for a host from 12./13.5. to 18.5. She has already been in the deconstruction week in January 2020 and wants to come back now.
    * could it become to full already?
    * but probably we can make it work!
    * there is support for Nathasch to come and chandi can imagine hosting
* [Antonin] TV crew coming next Tuesday (10th May) to film a documentary about Kanthaus. I am looking for people who would be happy to talk to them on camera and be filmed while doing stuff. On the day I will make ribbons that people can wear if they do not want to be filmed.

### Round 3
* [Antonin] There is food to be washed, please do not let it go bad!
    * Janina can be approached for help, Martin can imagine helping

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** zui
* **Tue.:** Janina
* **Wed.:** Maxime
* **Thu.:** Silvan
* **Fri.:** Antonin
* **Open Tuesday:** Anja, Doug

### Unavailabilities
* **Mon.:** Larissa, Hue_WA, Clara_WA, Linnea_WA, Martin_WA, Louis_WA
* **Tue.:** Larissa, chandi, Antonin
* **Wed.:** Antonin
* **Thu.:** Antonin, Martin_WA
* **Fri.:** Larissa, Martin_WA
* **Open Tuesday:** Maxime, Larissa, chandi
* **Week:** Tilmann

### Result
- **Mon.:** Zui, Chandi
- **Tue.:** Janina, Linnea_WA
- **Wed.:** Maxime, Martin_WA
- **Thu.:** Silvan, Louis_WA
- **Fri.:** Antonin, Hue_WA
- **Open Tuesday:** Anja, Doug

## 7. For next week
* 
