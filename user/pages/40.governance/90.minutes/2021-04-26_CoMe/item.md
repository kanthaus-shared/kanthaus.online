---
title: Coordination Meeting
date: "2021-04-26"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #188

- Date: 2021-04-26
- Facilitator: chandi
- Notary: kito
- Corona commissioner: chandi
- Mika caretaker: Maxime
- Levi caretaker: Janina
- Physical board caretaker: Antonin
- Digital calendar: Tilmann
- Reservation sheet purifier: Thore
- Present: Maxime, Silvan, Larissa, Vroni, Thore, Janina, Tilmann, Zui, Andrea, Lea, Antonin, Matthias, chandi, Nathalie, kito, Talita
----

<!-- Minute of silence (?) -->

### 0. Check-in round

### 1. Last week review
##### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_53a06909b308619deb089fd48e60ba6f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 17.4 people/day (+1.7)
- **⚡ Electricity**
    * usage: 8.85 €/day (⬇️-1%)
    * paid: -2.23 €/day 
    * ☀️ self produced: 72% (⬆️+2%)
    * emissions: 7 kg CO₂ₑ/week

- **💧 Water**
    * paid: 4.16 €/day (0%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 3.18 €/day (⬇️-39%)
    * emissions: 75 kg CO₂ₑ/week

<!-- add output from script here -->

##### Expenditure
* 48€ for 2 years of web hosting fees for kanthaus.online
* 118,80€ for washing powder (for ~1-2 years)
* ~315€ for Tiles, Ytong, ..
* ~10€ for 2.5kg of nails
* ~320€ for bitumen disposal

##### Income
- none in the shoe
- 50€ donation on the bank account

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
<!-- Update the current probability on the sheet! -->
- µCOVIDs available last week: 12754
- µCOVIDs used last week: 9164
- µCOVIDS balance from last week: -1150 
- µCOVIDs additional this week: 5000
- µCOVIDs available this week: 3850

###### Corona Status
- Numbers compared to the week before
  - 7-day incidence (Germany): 174 per 100.000 (+1%)
- Current relevant Restrictions
  - curfew from 22-5, walks alone allowed until 24

Source: https://www.revosax.sachsen.de/vorschrift/19062

##### Things that happened
- loooots of roof work happened again
    - tiles on the roof
    - Lattung, Unterspannbahn, ...
    - Skillshare!!
    - lot of small tasks
- interesting social sauna
- quinoa and potatoes are empty
- ukuvota about corona framework update
- the snack kitchen has been freed from part of its wallpaper, displaying its nice green and ochre colours

### 2. This week planning

##### People arriving and leaving
- **Mon.:** 
- **Tue.:** Doug coming back early, Vroni leaves, Lea also leaves
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Clara probably comes back 
- **Sat.:**
- **Sun.:** 
- **Mon.:** 
- **Some day:**

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
- no freezing in the nights, mostly sunny and dry, a little bit windy

##### Evaluations and check-ins
- *Doug* _Member_ : 3 days until _Absolute Days_ threshold (180)
- *Kito* _Volunteer_ : 6 days until _Absolute Days_ threshold (60)

##### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [chandi]
    - 12:00 Antipatriarchy group [matthias]
    - 13:00 Lunch
    - 14:30 Check-in [chandi]
    - 15:00 Roof work
    - 20:00 check out
    - 21:00 maybe movie
- Tuesday
    - Hausmüll
    - 8:30 to 9:30 Breakfast
    - 9:30 Check-in [janina]
    - 10:00 to 13:00 Roof work
    - 13:00 to 14:30 Lunch break
    - 14:30 Check-in
    - 14:45 to 18:00 Roof work
    - 19:00 Dinner
    - 20:00 Check-out
- Wednesday
    - 7:00 to 8:30 Early morning roof work shift
    - 8:30 to 9:30 Breakfast
    - 9:30 Check-in [kito]
    - 10:00 to 13:00 Roof work
    - 13:00 to 14:30 Lunch break
    - 14:30 Check-in
    - 14:45 to 18:00 Roof work
    - 18:00 BUND Wurzen meeting (online)
    - 19:00 Dinner
    - 20:00 Check-out
- Thursday
    - 10:00 Power Hour [Fac.: Talita, DJ: matthias?]
    - 14:30 Check-in
    - 14:45 to 18:00 Roof work
    - 16:00 Online meeting - "wurzen vernetzen"
    - 21:00 Next X-Men movie?
- Friday
    - Biotonne
    - 7:00 to 8:30 Early morning roof work shift
    - 8:30 to 9:30 Breakfast
    - 9:30 Check-in [thore]
    - 10:00 to 13:00 Roof work
    - 13:00 to 14:30 Lunch break
    - 14:30 Check-in
    - PARTY!
    - 19:00 Dinner
- Saturday
    - 15:00 building weeks reflecion
    - 19:00 Dinner
- Sunday
    - foodsharing-fairteiler Wenceslaigasse opening!
    - 20:30 project updates
- Some day
    - Sharing event(?)
- Next Monday
  - 10:00 CoMe [Fac: kito]
- Next week summary

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

### 3. Shopping plans
- [matthias] Handwaschpaste;
  - either https://www.ebay.de/itm/173268758357 organic 12€ for 500g
  - or cheap, (also plastic free), non-organic https://www.ebay.de/itm/142337022968 13€ for 6x 500g
      - preference for non-organic one to put it in multiple places
- [Doug] the sink for 500 euros (Link in last week's come) I would buy some time on Tuesday if no resistance!
  - https://yunity.slack.com/archives/CNLJL8PEG/p1619036886009100
  - relevant how deep it is? visit one in real life? -> put on slack
  - still concerns, but no resistance
- [antonin] some bike parts to fix various communal bikes for the cases where we cannot reuse what we already have, and some spokes to install dynamo hubs on two bikes (presumably about 100€ in total)
    - support!

### 4. Discussion & Announcements

#### Round 1
- [lise] on wednesday 18:00 the BUND Wurzen meeting will happen online. If people are interested in joining and taking over some task, you're warmly invited. The meeting is mostly about finding other people to be the official contact person, since i want to start a BFD myself and need replacement.
- [clara] asking for the final confirmation: Would it be okay to have the GemOek meeting in Kanthaus from the evening of the third of May till the 5th of May? Rumo, Cuca, and Cato would come. They would do COVID-tests after they arrive/Before they come.
    - we would go over the microcovid-threshold... (probably) can they reduce them by isolating some days before
        - update framework, include testing/vaccination!
    - we still would like to make it possible, but should watch our framework in general
- [matthias] After individual talks with Chandi and Antonin, I/we invite all cis-men to a first meetup today at 12 to talk about a regular antipatriarchy group / group for critical self reflection about masculinity
- [Silvan] Please don't hang up hammocks between two Wäscheständer in the garden. This will damage them and we can't use them anymore.
    - only if there is a Dachlatte it would be possible 
- [janina] visitor requests and resident situation in may
   - Vince: 3/05 - 18/05
   - Lucie: 3/05 - 14/05 (still to be confirmed)
   - GemOek people (Rumo, Cuca, Cato): 3/05 - 5/05
      - pad with plans will be revived to get an overview of house-fulness
- [Anja] Mail from Frau Polster (Diakonie): is someone interested in going on with the Gemeinschaftsgarten-Projekt that we could establish there? Or do we know people who are interested? I can't commit to it any more physically but I would be sad if it can't happen at all.
    - there is also a telegram group
    - Nathalie is informed (and can share information) but doesn't want to take responsibility
    - nobody immediately enthusiastic
- [maxime] we got a lot of food \o/ as usual, all help to get it sorted and cleaned is welcome. Some notes on the food we got: lots of eggs that should probably be tested before being used, as they might have been staying in a dumpster for quite some days, exposed to the sun part of the day.
- [zui] 
<!--  ask for points from people who haven't spoken yet -->
#### Round 2
- [matthias] It's tax declaration time. Bodhi and me will likely do it, but it will also likely be the last time for both of us. Do we already want to find a plan for the future, e.g. somebody wants to be part of the process somehow?
    - would be nice that people who live her do finances
    - nice skillshare would be cool and make it easier
    - meeting for "how can the process look like"?
    - some people are interested
    - maybe have a pre-meeting 
- [janina] lets teach andrea how to ride a bike!
    - maybe on sunday
 ```
 -------- __@ 
 ----- _`\<,_    
 ---- (*)/ (*)  
 ```
#### Round 3
- [matthias] association meetings when Bodhi is here? We both like to step down from being board members - chance for other people to jump in!
    - earliest end of next week or week after
    - set up a dudle? short one! - chandi will do it
#### Round 4
- [matthias] Another KMW Battery (80€) + 10W Solar panel to keep it charged (30€) https://www.amazon.de/Monokristallines-Solarmodul-Solarladeger%C3%A4t-Photovoltaikanlagen-Solarbetriebene/dp/B081YRGJHV
    - no resistance yet 
#### Round 5
- [matthias] Sewage pipe in K20 basement is leaking - replace/fix?
  - [Doug] I repaired last leak with that paste, didn't take long and would recommend/could guide other people to do it. If people want to replace pipe, please only do so with named responsibility!
### 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

#### Volunteers
- **Mon.:** Vroni, kito
- **Tue.:** Silvan, Andrea
- **Wed.:** Zui, Larissa
- **Thu.:** Nathalie, Janina
- **Fri.:** Antonin, Talita
- **Sat.:** Maxime, Thore
- **~~Open Tuesday~~:** 

#### Unavailabilities
- **Mon.:** Talita, Thore, Larissa, Zui, chandi
- **Tue.:** Talita, Thore, Larissa, Antonin
- **Wed.:** Talita, Thore, Antonin, Janina, Matthias, chandi
- **Thu.:** Larissa
- **Fri.:** Silvan, Matthias, chandi
- **Sat.:** Silvan, Andrea, Talita, Matthias, chandi
- **~~Open Tuesday~~:** 
- **Week:** Lea

#### Lunch cooking this week
- **Mon.:**
- **Tue.:**
- **Wed.:** 
- **Thu.:**
- **Fri.:** 
- **Sat.:** 

#### Weekend team this week
- **Participants**: 

### 6. For next week

