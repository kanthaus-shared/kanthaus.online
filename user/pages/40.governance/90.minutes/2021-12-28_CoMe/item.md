---
title: Coordination Meeting
date: "2021-12-28"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #224
- Date: 2021-12-28
- Facilitator: Tilmann
- Notary: Silvan
- Mika caretaker: all
- Levi caretaker: all
- Physical board caretaker: Anja
- Digital calendar: chandi
- Reservation sheet purifier: Anja
- Present: Mika, Levi, Silvan, Chandi, Nono, Anja, Janina, Tilmann, Maral, Larissa, Doug, zui

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_90691784fc329f32af037bcfcb8d82a9.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.9 people/day (-4.9)
- **⚡ Electricity**
  - usage: 176.94 €/week (⬆️+43%)
  - of that for heating: 128.5 €/week
  - paid: 157.67 €/week 
  - ☀️ self produced: 11% (⬆️+3%)
  - emissions: 106 kg CO₂ₑ/week
- **💧 Water**
  - paid: 19.94 €/week (⬇️-6%)
  - emissions: 2 kg CO₂ₑ/week


### Expenditure
- ?

### Income
- 20€

### Things that happened
- we saved a lot of Glögi
- more amazing food was saved, made and eaten
- small and cozy christmas
- a lot of lithium batteries were tested
- a trip to snowy wolfsberg lake
- more attic insulation progress
- we watched movies together!


#### Wider world
- Silke Helfrich died :(
- stricter Corona measures again...

## 2. This week planning

### People arriving and leaving
- **Mon.:** Larissa, Silvan and chandi come back
- **Tue.:** Maral leaves
- **Wed.:** Andrea, Matthias come back
- **Thu.:** Wolfram arrives
- **Fri.:** Martín arrives, Charly comes back
- **Sat.:** 
- **Sun.:** 
- **Mon.:** Anja might leave, Wolfram leaves
- **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins
- check-in with Nono and maybe Maral


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * Park cars on even/this side [x]
- Tuesday
    * 10:00 CoMe [Tilmann]
    * 11:00 Maral checkin [Janina]
    * 15:00 Freeshop & Glögi
    * 18:00 workout @ Yogaroom [?]
- Wednesday
    * Let's go hiking! (see details in discussion-section) [ZuLa]
    * after hiking: ToI-Meeting
    * Park cars on odd/opposit side [x]
- Thursday
    * 10:00 Power Hour [Fac.: chandi, DJ: ?]
    * 13:00 Nono checkin [Janina]
    * 15:00 Social Sauna [Janina + childcare?]
- Friday
    * Yellow bins [Larissa]
    * New Year's Eve!
- Saturday
- Sunday
- Next Monday
    * 10:00 CoMe [Doug]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans


## 4. To do
_Small fixes of things in the house_

* [ ] fix hipster room window curtain [Larissa & anja]
* [ ] more storage spaces for visitors [charly?]
* [x] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900) [chandi]
* [ ] Electrically disconnect the ceran stove on the right kitchen side


### New this week
* [ ] Add 2022 trash dates to the digital calender

## 5. Discussion & Announcements

### Round 1
- [ZuLa] We would like to go hiking this week, either wednesday or thursday (depending on others who are interested and other appointments, maybe we could move the ToI-Meeting?). The goal is to make it kid-friendly so the kids can join (and JaTi too!) so it probably won't be a difficult trail and maybe even not too far from Wurzen. No exact plans yet... Anyone interested??
    - [Nathalie] I'd even prefer the TOI meeting on thursday, would make it a bit more likely that I join!
- [Nathalie] group request from 28.01-30.01 (friday to sunday) 8 people, mail from Simsa 14.12.
    - [Nathalie] it would be nice, if they get any reply. Personally I could imagine to co-host and be rather up for the physical part (preparing rooms, house-tour, open the door). Anyone up for coordination? Also I'm increasingly worried about corona and I wonder how other people feel about another group. Saying no is equally fine to me (also if there is no hosting motivation).
    - [chandi] It's now already 1.5 years of kinda lock down and it seems like the pandemic will still go on for a while, I would be happy I've we can find ways to make it possible (like having masks in shared rooms, eating outside, etc). It anyway sounds like a group all the time busy by themselves, only there for their (scheduled) meeting without too much shared spaces? shouldn't that be possible corona wise? -> I can imagine hosting them, depending on the corona-related mood :)
        - [chandi] I'll write them and host them (if it works out)
- [tilmann] about the food tasks sheet: let's make it deactivated by default and only in use if decided for in come for a given week. in such a case it should also have a 'facilitator' who reminds people to sign up for tasks.
    - [chandi] why not have it there always? it's useful to see if anybody was dumpsterdiving already
    - [Janina] let's use the sheet this week! I will remind people about the sheet this week
- [chandi] From today on there is again the rc3, the online congress by the CCC, with plenty of talks (also a lot of non-techy ones! :P) If you're interested in one, I'm happy to watch it together :) the schedule: https://rc3.world/2021/public_fahrplan
    - [Silvan] If you want to go to the 2D-rc3-world ask me for a ticket :) 
- [janina] new years eve party: apparently martin volunteered to organize a bassment party
    - klugis might also want to do something, anna definitely is

<!-- check if anyone has a point that didn't speak already -->

### Round 2

### Round 3

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** Anja, Silvan

### Unavailabilities
- **Tue.:** zui, larissa
- **Wed.:** larissa, janina, doug
- **Thu.:** doug
- **Fri.:** chandi
- **Open Tuesday:** 
- **Week:** Maral

### Result
- **Tue.:** Doug, Nono
- **Wed.:** Chandi, Tilmann
- **Thu.:** Zui, Larissa
- **Fri.:** Janina
- **Open Tuesday:** Anja, Silvan

## 7. For next week
- 
