---
title: Coordination Meeting
date: "2021-10-18"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #214
- Date: 2021-10-18
- Facilitator: Tilmann
- Notary: Janina
- Mika caretaker: Larissa, Kim
- Levi caretaker: Kim, Sam
- Physical board caretaker: Andrea
- Digital calendar: chandi
- Reservation sheet purifier: Nathalie
- Present: Kim, Silvan, Guido, Maxime, Leonie, chandi, Doug, Matthias, Delfin, Andrea, Anja, Artiola, Nathalie, Sam, Jonnes, Janina, Tilmann, Yola

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_5fff3dc636c22cf7d81acffe95a07fa4.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 23.3 people/day (+12.6)
- **⚡ Electricity**
    - usage: 9.07 €/day (⬆️+54%)
    - paid: 2.61 €/day
    - ☀️ self produced: 59% (⬇️-9%)
    - emissions: 23 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.97 €/day (⏫+64%)
    - emissions: 2 kg CO₂ₑ/week


### Expenditure
- Bauschutt 10€

### Income
- Shoe: 5€
- Box: none


### Things that happened
#### In and around Kanthaus
- energetic start of the roof month!
    - lots of people arrived
    - a super house tour was given
    - a roof work introduction session was held
    - the scaffolding was built up
    - the chimney and ventilation shaft in the K22-2 bathroom was torn down, two big holes were drilled
    - a lot of trash was brought away
    - attic holes for ventilation
    - some movies were screened
    - AoE LAN party after fancy summer roll dinner
    - lots of communally organized repro work took place
- a non-personalized slack account for visitors was created
- lots of signs were improved, created and replaced
- communal laptops got repaired
- the vortex was overflowing
- sorting in the freeshop happened
- a vacuum cleaner broke in an effort of cleaning it
- the bread cutting machine broke
- we had trips to Püchau and to the lake

#### In the wider world
- energy prices rise strongly, will be a tough winter for a lot of people
- our next government wants to do a lot of costly, non specific things without taking money from anywhere

## 2. This week planning

### People arriving and leaving
- **Mon.:** Kim leaves, Antonin left, friend of Sam arrives, Luka comes back, zui comes back
- **Tue.:** Konrad_WA arrives, Bodhi and Lise arrive (but sleep mainly in the NaTho-Base), Silvan maybe leaves
- **Wed.:** Sam+1 leave
- **Thu.:**
- **Fri.:** Anja leaves, Clara leaves, Nathalie leaves, Roswitha, Gerd, Arnulf and Janine arrive
- **Sat.:** Lise leaves
- **Sun.:** Franzi arrives, Änsky arrives, Clara comes back, Nathalie comes back, Arnulf and Janine leave
- **Mon.:** Antonia_WA arrives, Jan might come
- **Some day:** Bodhi leaves, Silvan comes back


### Weather forecast

<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

Nice weather this week: top temperatures of 20°C on Wednesday and no rain the whole week. But a storm will be coming on Wednesday evening/Thursday.

### Evaluations and check-ins

_paused for roof month_

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Tilmann]
    * 11:00 Roof meeting [Doug]
    * Park cars on even side (only little trailer left) [maxime]
- Tuesday
    * Open Tuesday
- Wednesday
    * Park cars on odd side [maxime]
    * 16:00 Radeln Ohne Alter Wurzen meeting [Nathalie, Antonin]
- Thursday
- Friday
    * Yellow bin [chandi]
    * 10:00 Power Hour [Janina]
    * 12:00 Market Pickup [Anja]
- Saturday
- Sunday
    * Mika's 2nd birthday
        * 15:00? cake
        * 17:00? party with jam session and dancing in the piano room :D
- Next Monday
    * 10:00 CoMe [Doug]
- Next week summary
    * Roof building month!


_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
- [Doug] a crate or two of beer - could be Kanthaus money or not? (will buy privately for now)
- [Doug] soap
- [Matthias] I'm monitoring the market for used vaccum cleaners but don't know yet what to get.
- [Maxime] lentils or something similar as a source of iron. have not concrete offer in mind yet.
    - [janina] thinks there is general acceptance/support..?
    - [chandi] concerns because of lentils mostly not being regional and not great from an environmental POV


## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure k22-2 bathroom sink
* [ ] KMW: Fix the water leak under the front shield
* [ ] KMW: Fix cooling pipe so it does not touch the hot exhaust

### Medium priority
* [ ] More storage spaces for visitors
* [x] make k20 garden door easily closable from outside (or acquire a new door) -> apparently old door handle in workshop on left https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000 [Tilmann]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen and kitchen - new solution
* [ ] reinforce hook for laundry rack K22-1# [Antonin]
* [ ] lubricate door hinges (wich?)
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch

## 5. Discussion & Announcements

### Round 1
- [janina] sleeping spot orga
    - write down your preferences until this afternoon before it gets dark, then we still have some time to resolve possible conflicts
- [bodhi] hz people visit, see my slack post (https://yunity.slack.com/archives/C3RS56Z38/p1634504907339600)
    - [chandi] we will find out on the day what fits and how they can integrate, sleeping in the yoga room should be no issue
    - [nathalie] I can give them an introduction to the building month structure
- [tilmann] not doing task lottery, because it's building week and we have the daily sheets that allow for more spontaneous organization?
    - seems fine with people
- [Anja] reminder: I will do a sewing action this afternoon. If you want clothes to be fixed, put them in the box in the dragon room. I also fix dirty clothes! ^^
- [Doug] Status update on the bread cutting machine? And can we get another, preferably mechanical one?
    - [chandi] it doesn't turn properly and makes sound when you try to manually turn it. didn't look at it in detail. would also like to get a new one.
    - [maxime] can't we just fix it? maybe there's just some dirt inside? let's first have a more thorough look.
<!-- Anybody who hasn't spoken so far wants to add a point? -->


## 7. For next week
