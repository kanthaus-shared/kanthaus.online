---
title: Coordination Meeting
date: "2022-08-01"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #254
* Date: 2022-08-01
* Facilitator: Eric
* Notary: Doug
* Mika caretaker: -
* Levi caretaker: -
* Physical board caretaker: Martin
* Digital calendar: Doug
* Reservation sheet purifier: Matthias
* Present: Doug, Eric, Nathalie, Matthias, Martin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/16af0b2d-7450-4726-9e20-86869122e124.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.1 people/day (-1.7)
- **⚡ Electricity**
    - usage: 32.65 €/week (⬇️-1%)
    - of that for heating: 3.34 €/week
    - paid: -44.78 €/week 
    - ☀️ self produced: 77% (⬇️-4%)
    - emissions: 8 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.81 €/week (⬇️-9%)
    - emissions: 1.0 kg CO₂ₑ/week

### Expenditure


### Income
* 500€ donation

### Things that happened

#### In or around Kanthaus


#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* A 7.0 magnitude earthquake strikes Luzon in the Philippines, killing 10 people and injuring at least 394 others.
* Infolge der Dürre und Hitze in Europa ist es in den Nationalparks Sächsische Schweiz und Böhmische Schweiz zu Waldbränden gekommen.

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Nathalie, Thore, Doug leave
* **Wed.:** maxime comes back (Oh no, I'll miss you! :cry: [Doug])
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Antonin comes back
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->



### Evaluations and check-ins
- Antonin?


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Eric]
    * Park KMW & trailer on even/this side [Matthias]
    * Papier [Doug]
* Tuesday
    * Hausmüll [-]
* Wednesday
    * Park KMW & trailer on odd/that side [Matthias]
* Thursday
    * 10:00 Power Hour [Fac.:Martin]
* Friday
    * Biotonne [Martin]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do
_Newest tasks on top_
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?] ~~Denkmalschutz der Fassade?~~
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher [chandi]
* [ ] first aid kits visible in the staricases K20-1 and K22-1
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] add a doorbell in the garden
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Doug] Nextcloud alternates being fine and unusably slow. How can we keep it being fine?
    * [Antonin] I am planning to put in an expense request for next CoMe to rent server space to host it there
    * People positive with that! Seems like the small group is happy/neutral for you to go forward.
* [Matthias] Asks to borrow Nathanael 7-10 days.
    * No resistance in room.
* [Nathalie] brings up food chain issue...
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Andrea, Doug

### Unavailabilities
* **Mon.:** Matthias
* **Tue.:** Martin
* **Wed.:** Matthias
* **Thu.:** Matthias
* **Fri.:** Matthias
* **Open Tuesday:** 
* **Week:** Nathalie

### Result
- **Mon.:** Matthias
- **Tue.:** Eric
- **Wed.:** Martin
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** Andrea, Doug
- (Done without lottery, OMG)

## 7. For next week
* 

