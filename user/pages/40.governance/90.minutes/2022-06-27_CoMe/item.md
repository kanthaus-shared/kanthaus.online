---
title: Coordination Meeting
date: "2022-06-27"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #249
* Date: 2022-06-27
* Facilitator: Doug
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Nono
* Physical board caretaker: Martin
* Digital calendar: chandi
* Reservation sheet purifier: Larissa
* Present: Martin, chandi, Larissa, Doug, Janina, Tilmann

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/21d6adde-63d2-4ef5-ae62-bb12141b8633.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.7 people/day (+3.3)
- **⚡ Electricity**: broken
    - of that for heating: 4.03 €/week
    - paid: -59.09 €/week 
    - emissions: 4 kg CO₂ₑ/week
- **💧 Water**: broken


### Expenditure
* [kito] 78,75€ for 25kg of organic oats
* [antonin] ~5€ for dishwasher powder

### Income
* 217€ (shoe)

### Things that happened

#### In or around Kanthaus
* various travels came to an end and brought people home
* basic crypto shit coworking in flinta* tech context
* short but really worthwhile social sauna
* big birthday party for kito in the garden
* larissa put the mats back into the yoga room
* mika and larissa checked out the open air pool in burkartshain - in the rain
* gemök meeting in harzgerode right now
* some people went to the dresden pride

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* basic right to abortion was cancelled in the US
* in contrast paragraph 219a was removed in Germany which now enables doctors to inform about abotion more freely
* earth quake in afganistan with at least 1.000 deads
* 1st female vice president in Colombia
* G7 summit is happening
* electricity prices are going down a bit due to a shift from money being paid not for getting the power but for emitting C0² in Germany


## 2. This week planning

### People arriving and leaving
* **Mon.:** Andrea, Anja leave. Thore, Nathalie, zui return.
* **Tue.:** Nono leaves. Andrea returns. Larissa leaves, Findus and Jelli leave
* **Wed.:** Andrea leaves. Kito, Matthias return. Larissa comes back, chandi leaves
* **Thu.:** Andrea returns.
* **Fri.:** 
* **Sat.:** 
* **Sun.:** zui's mom visits for the day
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
 _tropical thunder week_

### Evaluations and check-ins
- [Andrea] I would like to have my evaluation this week but I'm only vailable on thursday at 17.00. would anyone be up for facilitating it? 

:judge: Due for evaluation (and seen within the last 7 days):

- *Anja* _Volunteer_ : _Absolute Days_ threshold 119/60 (+98%)
- *Andrea* _Volunteer_ : _Absolute Days_ threshold 118/60 (+97%)
- *Matthias* _Volunteer_ : _Absolute Days_ threshold 96/60 (+60%)
- *Doug* _Member_ : _Absolute Days_ threshold 223/180 (+24%)
- *Antonin* _Member_ : _Absolute Days_ threshold 213/180 (+18%)

:clock1130: Due for evaluation soon (in the next 7 days, and seen within the last 7 days):

- *Martin_WA* _Visitor_ : 2 days until _Days Visited_ threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [doug]
    * ~~Full Force Festival pickup?~~
    * Park KMW & trailer on even/this side [Doug]
* Tuesday
    * 10:00 Knowlege sharing about Kanthaus network redesign @piano room [chandi]
* Wednesday
    * 10:00 Monthly teams meeting [Doug]
    * 10:00 @ALM Fokusgruppe Nachhaltigkeit [Janina]
    * Park KMW & trailer on odd/that side [Martin/Matthias?]
* Thursday
    * early: Anja-Antonin moving action 
    * 17:00 Andrea's evaluation [Doug]
* Friday
    * yellow bins [Martin]
    * 10:00 Power Hour [Fac.: Janina, DJ: ?] 
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* Knowledge share:
    * [kito] request for ebay-bot skillshare (how to implement it in mattermost) (chandi?) for thursday or friday (but it's not urgent)

## 3. Shopping plans
* [antonin] someone might want to get dishwasher powder (whatever The Right One should be - I am assuming it's not what I bought as an interim solution) and Spüli (I could not find any in the reserve)
    * we usually didn't buy special dishwashing detergent, just the cheapest from the supermarket
    * there is a lot of dishwashing liquid in the basement of K20, Tilmann might get it up


## 4. To do
_Newest tasks on top_
* [ ] fix weekly-report script
    * water stats [antonin?]
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher
* [ ] first aid kits visible in the staricases K20-1 and K22-1
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [janina] we are warmly invited to participate in the pop-up culture cafe in Wenceslaigasse that is starting on July 4th.
    * Janina is in contact with Emma, Larissa with Martina - talk to us to take part! :)
* [doug] coming and going: leave out daytrips?
* [Antonin] Moving action: There's already 4 people and a van, if you want to help you can but it's not direly needed.

<!-- check if anyone has a point that didn't speak already -->

### Round 2
*

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** Larissa
* **Fri.:** Janina
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** 
* **Tue.:** Martin
* **Wed.:** 
* **Thu.:** Doug
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** chandi, Tilmann

### Result
* **Mon.:** Martin
* **Tue.:** 
* **Wed.:** Doug
* **Thu.:** Larissa
* **Fri.:** Janina
* **Open Tuesday:** Anja

## 7. For next week
* 

