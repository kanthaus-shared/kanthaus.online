---
title: Coordination Meeting
date: "2021-10-11"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #213
- Date: 2021-10-11
- Facilitator: Matthias
- Notary: Antonin
- Mika caretaker: Silvan & Larissa
- Levi caretaker: Janina
- Physical board caretaker: Andrea
- Digital calendar: chandi
- Reservation sheet purifier: Doug
- Present: Matthias, Tilmann, Larissa, Nathalie, Thore, Maxime, Zui, Andrea, Antonin, Doug, Janina, Levi, Silvan, Mika

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_a67c226df7045de890ddabcb4defda57.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.7 people/day (+0.1)
- **⚡ Electricity**
    - usage: 5.88 €/day (⬇️-4%)
    - paid: -0.6 €/day 
    - ☀️ self produced: 69% (⬇️-1%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.43 €/day (⬇️-18%)
    - emissions: 1 kg CO₂ₑ/week


### Expenditure
- [antonin] 199.90€ fees for scaffolding authorization
- [matthias] 43,99€ for missing pieces for heating installation
- [matthias] 34€ for drillbit to drill 130mm ventilation pipe holes into the walls
- [matthias] 35,50€ for 16 original Miele Hyclean 3D GN vacuum cleaner bags
- [matthias] 1679€ for ventilation parts

### Income
0

### Things that happened
#### In and around Kanthaus
- 2nd pool trip with the kids
- really empty and calm weekend
- zui and larissa did a bike trip and went to the party in pödi
- first night frost for this cold season
- new ventilation hole in Lantern
- a lot of new signs appeared in kanthaus
- a solidarische nachbarversammlung happened in GSX and Kh was represented there
- Felix visited and took his bike back

#### In the wider world
- Kurz, Altmaier and AKK stepped back from their positions :tada:
- the Greens in Scottland are getting in power and setting up free busses for the young

## 2. This week planning

### People arriving and leaving
- **Mon.:** 5 FFJ people and Leonie_WA arrive, Clara comes back
- **Tue.:** Anja arrives, Artiola_WA arrives
- **Wed.:** 
- **Thu.:** Guido_WA arrives, Silvan leaves, Zui leaves
- **Fri.:** Silvan comes back
- **Sat.:** 
- **Sun.:** 
- **Mon.:** Zui comes back
- **Some day:** 


### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
rainy in the next days, average temperatures should rise a bit. Keep windows and doors closed!

### Evaluations and check-ins

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe
    * 13:00 Roof meeting 
    * Park cars on even side [Matthias]
- Tuesday
    * 13:00 Roof meeting
    * Open Tuesday
    * Afternoon: Detailed house tour [Janina]
- Wednesday
    * Building month start!
    * Park cars on odd side [Matthias]
- Thursday
- Friday
    * Biotonne [Thore]
    * 10:00 Power Hour [Larissa]
    * 12:00 Market Pickup [Nathalie/Janina]
- Saturday
- Sunday

- Next Monday
    * 10:00 CoMe [Tilmann]
- Next week summary
    * Roof building month!
    * Lots of visitors next week-end

- Days until Roof month: 2!

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans

## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure k22-2 bathroom sink

### Medium priority
* [ ] More storage spaces for visitors
* [ ] make k20 garden door easily closable from outside (or acquire a new door) -> apparently old door handle in workshop on left https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [x] fix loose table leg in Lantern [Tilmann]
* Ventilation preparation:
    * [ ] remove chimney and ventilation pipes in K22-2 bathroom 
    * [ ] remove chimney in K20-2 communal sleeping room
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen - new solution
* [ ] reinforce hook for laundry rack K22-1# [Antonin]
* [x] toilet fill valve in baby bathroom is leaking - maybe clean it? [Tilmann]
* [x] toilet flush chain in K20-0# became too short, make it longer [Antonin]
* [ ] lubricate door hinges

## 5. Discussion & Announcements

### Round 1
- [chandi] Let's provide more space for visitors again!
    - **short term storages:** I'll put everything in unlabled spaces into the vortex 1h after come
    - **communal closet:** please consider, whether you need all the storage space or whether it could be used by others. prominent:
        * Clara (most space usage)
        * Nathalie & Thore
    - **office spaces**: please only block 1 desk slot for more than a day
        * ping matthias @ main office + fansipan
        * ping andrea @ dragon + silent
    - **dragon room**: the new intermediate?!
      - [tilmann] we want to move the ventilation parts out of the way, maybe to ex food storage? Many of these items should be sold.
    - [matthias] also check your boxes in the intermediate storage month
- [doug/maxime] [Roof+ month!](https://pad.kanthaus.online/roofMonthOct2021)
  - week structure proposals: work days by default would be Monday to Friday, but could be exchanged with a week-end day, for example if we can expect the weather to be much better, to be decided in the DCM (Day Coordination Meeting)
  - day structure proposal [read calendar example]
     - 8:30: breakfast
     - 09:00-12:00 morning session
     - 12:00 lunch
     - 13:00-16:00 afternoon work session
     - 18:00 Day Coordination Meeting
     - 19:00 dinner
     - 20:00 kitchen clean-up
     - 20:30 entertainment, dumpster diving
  - you are welcome to add ideas in [the pad](https://pad.kanthaus.online/roofMonthOct2021) in the Evening/Weekend plans (Movie ideas, Games)
  - [maxime] there was the idea to use the dragon room as a backup lunch room during the building month, when eating in the garden isn't an option (raining or too cold)
     - [Andrea] there is also the potentially conflicting plan of using it as office space
- [janina] sleeping spot planning for this week?
  - [doug/maxime] we would invite people to fill their preferred sleeping place for the week in this sheet [show sleeping planing sheet]. We would then get the people arriving later to do the same with the help of their hosts. People with external sleeping place options (flat in Wurzen, garden, etc) but still considering sleeping in KH could see where to sleep after everone else is settled. Also a facilitator for this process could be useful (to avoid having a sleeping meeting). This sleeping planning could happen on Monday (after come, or dinner).
  - [Matthias] doing it not today but only once most people have arrived
  - [Janina] guests might not have strong feelings because they do not know the place yet
  - [Zui] people using privates for a long time should still keep the room tidy during daytime (do not store your stuff there)
  - [Janina] putting boxes of personal stuff in the ex-food storage
  - [Nathalie] short-term storage next to the bathroom might be enough, not living out of their backpacks
  - [Matthias] we should all clear our stuff from the backpack storage area
  - [Tilmann] leaving personal stuff in the communal if they find some space?
     - [Zui] it wakes up people if people access stuff there
- [Andrea] "Awareness team" for Building month.
  - 2-3 people who have the task to be available to be approached about issues they do not feel like voicing in other contexts
  - not only for guests but for longer-term residents too
  - need other people
  - maybe meeting on next Monday evening to think about the needs and the format
  - [Nathalie, Chandi] could imagine like joining
- [Antonin] making the communal bookable like the dorm?
  - [Silvan] making the sign easy to read
<!-- Anybody who hasn't spoken so far wants to add a point? -->

### Round 2
- [chandi] Announcement: There is discussion about how to proceed with the heatpump sourcing? 
    * Slackpost: https://yunity.slack.com/archives/C3RS56Z38/p1633872757311600
    * Ukuvota: https://ukuvota.world/#/app/38846291-8cb1-4255-5f2a-04098274ffb9/collect
    * [chandi] being cold already
    * [Matthias] in the past 3 years, we never started heating before first of november
    * [Tilmann] heating can be turned on, with (slightly more) expensive options
    * consensus to discuss more details outside of CoMe -> see Slack
    * [Maxime] trying to decouple the questions: when should we start heating, and how should we heat?
    * [Doug] proposal to make a whiteboard to record who would like to have heating turned on
- [tilmann] encourages people to use hot water bottles when they feel cold (in addition to warm clothing) - it's MUCH more energy efficient than turning on central heating and thus might be useful during the next weeks!
  - [Matthias] hot showers are also fine! (or even baths!!)
- [antonin] is interested in finding ways to organize dumpster washing more, see Slack: https://yunity.slack.com/archives/C3RS56Z38/p1633938452318500
- [Maxime] door of K20-2 is squeaking, should we lubricate it?
<!-- Anybody who hasn't spoken so far wants to add a point? -->

### Round 3

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Nathalie, Thore
- **Tue.:** Zui
- **Wed.:** Andrea
- **Thu.:** Larissa
- **Fri.:** 
- **Open Tuesday:** Anja
- **Food washing:**

### Unavailabilities
- **Mon.:** chandi
- **Tue.:**
- **Wed.:** Antonin
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** Janina, chandi, Maxime
- **Week:** Silvan

### End result
- **Mon.:** Nathalie, Thore
- **Tue.:** Zui, Tilmann
- **Wed.:** Andrea, Janina
- **Thu.:** Larissa, Chandi
- **Fri.:** Antonin, Maxime
- **Open Tuesday:** Anja, Doug

## 7. For next week
