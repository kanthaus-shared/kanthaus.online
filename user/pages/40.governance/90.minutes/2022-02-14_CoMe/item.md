---
title: Coordination Meeting
date: "2022-02-14"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #231
* Date: 2022-02-14
* Facilitator: Nathalie
* Notary: kito
* Mika caretaker: Tilmann
* Levi caretaker: Tilmann
* Alex caretaker: Vince
* Physical board caretaker: Janina
* Digital calendar: chandi
* Reservation sheet purifier: Matthias
* Present: chandi, Larissa, Larry, Vince, Silvan, Thore, Andrea, Antonin, Janina, findus, Maxime, Doug, Jelli, Matthias, Nathalie, Cille, Clara, kito

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf4808.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.6 people/day (+13.1)
- **⚡ Electricity**
    - usage: 129.84 €/week (⏫+114%)
    - of that for heating: 80.1 €/week
    - paid: 92.43 €/week
    - ☀️ self produced: 26% (⬆️+2%)
    - emissions: 77 kg CO₂ₑ/week
- **💧 Water**
    - paid: 0.0 €/week (0%)
    - emissions: 0 kg CO₂ₑ/week



### Expenditure
* no expenditures

### Income
* 50€ donation

### Things that happened

#### In or around Kanthaus
* kidz meeting took place
* there was an mcm breakfast
* return to kanthaus
* a lot of sorting and cleaning
* chimney in dumpster kitchen was removed
* wolfis farewellparty
* maxime came back
* the new family arrived
* kids slept in the same room and enjoyed it
* mirrors for yoga room arrived
* social sauna took place
* first time this years 3 days of sun in a row

#### Wider world
* Ukraine conflict keeps escalating
* "new" Bundespräsident is the old one
* EEG will be fundamentally redesigned in April so don't install your solar panels now!

## 2. This week planning

### People arriving and leaving
* **Mon.:** Larry leaves, Clara leaves, Callum arrives and leaves
* **Tue.:** Anja leaves
* **Wed.:** findus and Jelli leave (or thursday)
* **Thu.:** Astrid arrives
* **Fri.:** Acrolotl-people arrive
* **Sat.:** Astrid leaves
* **Sun.:** kito leaves, Acrolotl-people leave
* **Mon.:**
* **Some day:** Clara returns

### Weather forecast
* generally quite nice
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

- Anja Volunteer : Absolute Days threshold 94/60 (+57%)
- Nathalie Volunteer : Absolute Days threshold 81/60 (+35%)
- Findus Visitor : Days Visited threshold 28/21 (+33%)
- Tilmann Member : Absolute Days threshold 199/180 (+11%)
- Zui Volunteer : Absolute Days threshold 61/60 (+2%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Silvan Volunteer : 5 days until Absolute Days threshold (60)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Nathalie]
    * Park cars on even/this side [chandi]
    * evening: Maxime will do a presentation about his last months
* Tuesday
    * black bin gets emptied [Antonin, Cille]
    * 18:00 Workout@Yoga Room [Silvan, Larissa]
* Wednesday
    * Park cars on odd/that side [Matthias]
    * 14:00 ToI-meeting
* Thursday
    * 10:00 Power Hour [Fac.: Andrea, DJ: kito]
    * 15:00 Tech-Café [Matthias]
    * 18:00 Foodsharing Wurzen meeting @flat [Thore]
* Friday
    * organic waste bin gets emptied [Matthias]
    * 11:00 Nathalie's evaluation [Larissa]
* Saturday
    * Acrolotl online starts (AcroYoga/Yoga-event)
    * 13:00 Critical Mass
    * afterwards: board game party [Cille]
* Sunday
    * 10:00 ToI-day
* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary
    *

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [matthias, tilmann] 140€ for more Abluftfilter for the ventilation system (should last for about 8 years)
    * knowledge sharing session on how to change them at some point
* [Antonin] 20€ for 4m of parapet trunking for the silent office (including 4 double plugs)

## 4. To do
_Newest tasks on top_
* [ ] repair WiFi in the Hipster
* [x] remove washroom chimney [chandi]
* [ ] repair Schaukasten
* [ ] Clean K18 garden
* [ ] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Install light in the hallway of the K20-1 flat
* [ ] Install light in the hallway of the K20-2 flat

### more advanced tasks
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Doug] suggest that all resource graph lines are made thicker, and the yellow for solar is darker.
    * [Antonin] +1
    * [chandi] done!
    * [Antonin] ❤ ❤
* [Antonin] Trebsen visit this week?
    * visit the empty house a person mentioned in email
    * combine it with Ebay-Kleinanzeigen-Abholung in Grimma
    * Janina, Silvan are interested into join
    * person was already here 2 years ago and is a big fan of us
    * maybe tomorrow, if person has time, group will anounce it on slack
* [Nathalie] after-TOI-day schedule https://yunity.slack.com/archives/C3RS56Z38/p1644754633002779
    * close ToI-chapter, continue the processes which started
    * maybe better to schedule it more in advance next time
    * sunday is probably the best day, let's do it on sunday
* [chandi] Washroom continuation - who can support? :)
    * chimney is torn down, but some tasks need to be done (espacially remove  tarpaper (Teerpappe))
    * Tilmann will put ventilation and waterpipes, will also make dust
    * more follow up tasks: bring stones away to Deponie Bennewitz (chandi and Nathalie will do this), mounting sink again, ...
    * tasks will be added to the list, we will talk again next CoMe
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Larissa] tobis book-meeting
    * 5 people in the end of April for 2 nights, who can host?
    * Larissa can do it, but is not sure if she is here, Matthias could be back-up-host, Janina also
    * general support
* [chandi] Any Feedback about the KiDZ meeting and last groups in general
    * impact of masks virally/socially.. could be discussed
    * should all residents use comunal rooms only with masks if group decides to wear mask?
    * solution for people who have their desk in Fansipan
    * maybe some brief information what the group does/did during their time in Kanthaus, not so many informal talks because of pandemic situation
* [Silvan] found a car park recycler close to Wurzen, if you need parts or want to join visiting talk to Silvan

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Antonin
* **Tue.:**
* **Wed.:** Larissa
* **Thu.:** Andrea, Silvan
* **Fri.:**
* **Open Tuesday:**

### Unavailabilities
* **Mon.:** kito, Doug
* **Tue.:** chandi, Thore
* **Wed.:** kito, chandi, Doug
* **Thu.:** Doug, chandi, Thore, Nathalie, Janina, kito
* **Fri.:** Doug
* **Open Tuesday:** chandi, Thore, Janina, Maxime, Vince
* **Week:** Matthias, Jelli, findus, Larry, Clara

### Result
- **Mon.:** Antonin, Maxime
- **Tue.:** Janina, kito
- **Wed.:** Larissa, Cille
- **Thu.:** Andrea, Silvan
- **Fri.:** Thore, Vince
- **Open Tuesday:** Nathalie, Doug

## 7. For next week
* Washroom continuation - who can support? :)
