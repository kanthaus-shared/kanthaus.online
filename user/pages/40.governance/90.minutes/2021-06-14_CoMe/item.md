---
title: Coordination Meeting
date: "2021-06-14"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #195
- Date: 2021-06-14
- Facilitator: Thore
- Notary: Antonin
- ~~Mika caretaker:~~
- Physical board caretaker: larissa
- Digital calendar: chandi
- Reservation sheet purifier: andrea
- Present: Talita, Andrea, Nathalie, Thore, chandi, Silvan, Larissa, Brieuc

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_b67a1cca5e401f40e85104bb5faa3e99.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 7.6 people/day (-2.1)
- **⚡ Electricity**
    - usage: 4.72 €/day (⬆️+3%)
    - paid: -6.45 €/day 
    - ☀️ self produced: 77% (⬆️+3%)
    - emissions: 4 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.39 €/day (⬆️+13%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
- 0 €

### Income
- 0 €


### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 8527
- µCOVIDs used last week: 3111
- µCOVIDS balance from last week: 5416
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 10416

### Things that happened
- a day trip to the Sächsische Schweiz
- a slackline appeared in the garden
- the cloud room got a brand new look
- some people went to the circus in Dresden

## 2. This week planning

### People arriving and leaving
- **Mon.:**
- **Tue.:**
- **Wed.:** Silvan will leave
- **Thu.:**
- **Fri.:** Andrea may leave, Larissa leaves, Brieuc leaves
- **Sat.:** Andrea will be back
- **Sun.:** Silvan and Larissa come back
- **Mon.:** 
- **Some day:** Maybe Clara

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [Thore]
    - 15:00 Kritma round [Chandi]
- Tuesday
    - 16:00 Open Tuesday [Antonin, Franziska]
- Wednesday
    - 12:00 Brieuc Check-In [Talita]
- Thursday
    - 10:00 Power Hour [Fac.: Talita]
    - 18:00 foodsharing Wurzen meeting
- Friday
    - Gelber Tonne [Antonin]
- Saturday
- Sunday
- Next Monday
    - 10:00 CoMe [Chandi]
- Next week summary

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

## 3. Shopping plans
- [Talita] bike parts for fixing tandem ~30€


## 4. Discussion & Announcements


### Round 1
- [Antonin] FFF Wurzen holding a meeting in KH next week? It would be 5-7 people for up to 2 hours in the cloud room, between 21st and 23rd of June
    - no resistance
- [Silvan] The gray Bluetooth box is extremely dirty with white paint. Who will clean it and how can we manage it, that such things doesn't happen again?
    - [Talita] feels responsible for cleaning it
- [Nathalie] Food going bad! Pls check leftovers before you prepare new food and store leftovers in a cool place in the evening
- [Chandi] FFJ (https://freiwilliges-freies-jahr.de) Week in Kanthaus? 7ppl, 7 days in the timeframe 11.- 24.10.2021
    * https://yunity.slack.com/archives/G7E8RPFMH/p1623074707001100

### Round 2
- [Antonin] ~~anyone up for a bauschutt-tour with me this week?~~ the trailer is not here. Where was it announced?
    - next time use calendar and slack to announce
- [Silvan] regular tasks: watering the garden, reparking the car
    - [Thore] can take care of reparking
    - [Talita] and [Andrea] could help Thore with watering

### Round 3
- [Silvan] Kritma round later? 16:30?


## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Thore, Antonin
- **Tue.:** Brieuc
- **Wed.:** Andrea, Talita
- **Thu.:** Larissa
- **Fri.:** Nathalie, Chandi
- **Open Tuesday:** Antonin, Franzi

### Unavailabilities
- **Mon.:** chandi, Thore
- **Tue.:** Antonin, chandi, Thore
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Brieuc
- **Open Tuesday:** 
- **Week:** Silvan

## 6. For next week
