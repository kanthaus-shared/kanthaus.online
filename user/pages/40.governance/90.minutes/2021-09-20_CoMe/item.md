---
title: Coordination Meeting
date: "2021-09-20"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #210
- Date: 2021-09-20
- Facilitator: chandi
- Notary: Janina
- Mika caretaker: Larissa
- Levi caretaker: Tilmann
- Physical board caretaker: Matthias 
- Digital calendar: Antonin
- Reservation sheet purifier: Silvan
- Present: zui, Matthias, Silvan, Antonin, Janina, Maxime, Thore, Tilmann, Bodhi, chandi, Larissa

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_860e5f75ad2871df138bd48a24af953c.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.0 people/day (+0.0)
- **⚡ Electricity**
    - usage: 6.72 €/day (⬆️+13%)
    - paid: 0.37 €/day 
    - ☀️ self produced: 62% (⬇️-13%)
    - emissions: 16 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.38 €/day (⬇️-5%)
    - emissions: 2 kg CO₂ₑ/week



### Expenditure
- 13€ for compost toilet air exhaust parts
- 60-70€ for toilet paper, covid tests, masks, dishwasher detergent, drill bits4/5mm steel+concrete, anglegrinder blades
- 350€ for window parts
- 7500€ for heat pump, warm water storage and some pipes

### Income
none

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)

> From [Collective Agreements](https://kanthaus.online/en/governance/collectiveagreements): *If adjusted prevalence is < 0.1%, we have no formal restrictions.*
> Current **adjusted prevalence** can be found [here](https://www.microcovid.org/?duration=600&interaction=oneTime&personCount=1&riskBudget=100000&riskProfile=average&scenarioName=custom&setting=indoor&subLocation=Germany_Sachsen&theirMask=none&topLocation=Germany&voice=loud&yourMask=none&yourVaccineType=johnson): click on "Details" in the "Step 1" column

- Incidence
    - Germany: 90,6
    - Saxony: 32,5
    - Landkreis Leipzig: 21,7
- Adjusted prevalence Saxony: 0,11% 
- µCOVIDs available last week: 9572
- µCOVIDs used last week: 1910
- µCOVIDS balance from last week: 7662
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 12.662


### Things that happened
#### In and around Kanthaus
- the bike sheds got cleared of a lot of things
- the compost was flipped
- an onion gleaning action happened
- preliminary work for the installation of the heat pump has started
- the compost toilet now has a ventilation pipe that reaches up to the roof
- the chimney in the snack kitchen was torn down and there was an intermediate snack kitchen in the elephant room for a day
- thore's birthday led to a bike trip that yielded 150kg of hokkaido pumpkins from jahnishausen
- an epidemia of gastrointestinal disease was survived
- another bean harvesting action took place - this time with foodsavers from Leipzig
- food arrived from HZ
- people went to the big mobility transformation demo in Leipzig

#### In the wider world
- 

## 2. This week planning

### People arriving and leaving
- **Mon.:** 
- **Tue.:** Bodhi leaves
- **Wed.:** 
- **Thu.:** Larissa and Silvan leave, Matthias might leave for some days
- **Fri.:** chandi leaves, zui leaves
- **Sat.:**
- **Sun.:** 
- **Mon.:** 
- **Some day:** 


### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Autumn is here! Temperatures drop in the beginning of the week but will rise a bit towards the weekend.


### Evaluations and check-ins
- *Bodhi* _Volunteer_ : _Absolute Days_ threshold 132/60 (+120%)
- *Larissa* _Member_ : _Absolute Days_ threshold 192/180 (+7%)
- *Janina* _Member_ : _Absolute Days_ threshold 186/180 (+3%)
- *Silvan* _Volunteer_ : 0 days until _Absolute Days_ threshold (60)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe
    * 15:30 Coffee & Cake in the garden
    * Park cars on even side [Maxime]
- Tuesday
    * 11:30 Bodhi's evaluation [Antonin]
    * Open Tuesday
- Wednesday
    * 11:00 Larissa's evaluation [Thore]
    * Park cars on odd side [Maxime]
- Thursday
    * 10:00 Power Hour [Fac.: Antonin, DJ: ?]
    * 15:00 sharing event [Janina]
- Friday
    * Yellow bin [maxime]
    * 12:00 Market Pickup [Andrea]
- Saturday
    * 19:00 theater @ Villa Klug
- Sunday
    * 10:00 germ fighting hours
- Next Monday
    * Paper [Antonin]
    * 10:00 CoMe [Janina]
- Next week summary

- Weeks until Roof month: 3.5!

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
* [Antonin] Sponges
* [Antonin] Detergent for bathroom surfaces?
* [Janina] Containers to store food
* [Matthias] Permanent fineliner (and foil pens?)

## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure wash kitchen sink
* [ ] secure k22-2 bathroom sink

### Medium priority
* [ ] More storage spaces for visitors
* [ ] make k20 garden door easily closable from outside (or acquire a new door) -> apparently old door handle in workshop on left https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] fix loose table leg in Lantern
* [ ] empty dry food storage shelf (find new places for the stuff)
* Ventilation preparation:
    * [x] Tear down chimney in Snack Kitchen
    * [ ] Tear down chimney in communal sleeping room
    * [ ] Tear down chimney in dining room
    * [ ] Tear down chimney in piano room
* [x] Kitchen shelf is slanting / leaning over [Tilmann]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen - new solution
* [ ] bed in private is dissolving and making many small crumbs

## 5. Discussion & Announcements

### Round 1
* [maxime] let's invite people to join us during the incoming building month! A text in English can be found in the [building month pad](https://pad.kanthaus.online/roofMonthOct2021), a German translation would be welcome. You are welcome to take initiatives regarding who to invite, as we are far from saturation yet: according to the poll, we will be 8 at most during a given week (not counting the 4 FFY that might be there during first weeks, just let us know when you get some committed answers. Invited people are welcome to directly answer the poll https://poll.disroot.org/373TNY9W87QM3TH9
* [janina] collective agreements change about language: proposal phase ends today at 6 pm. then the voting phase starts and runs for 7 days.
* [matthias] Ventilation planning group gets more concrete and might want to start implementing ventilation for K20-2 + K22-2-2 after the roof month. We need pipes, fittings, a ventilation device, some valves and some fire protection stuff and gipskarton stuff to prepare the covers for the pipes which might end up around 2000-4000€ for the first stage (which will be the most expensive step of this project). Should we plan it in more detail and make an expense request?
    * yes! the group awaits a detailed plan and clear expense request
* [chandi] How can we keep this floor tidier?
    * [zui] quick tidy up directly after come?
    * [janina] think it's a recurring topic and we need more structure to ensure an acceptable level of tidyness. I actually have quite some ideas and could imagine having a meeting about that topic. will make a slack post
* [clara] Announcement: On the first of October there will be the release of a documentary about "Ella". More infos: https://yunity.slack.com/archives/CFDNQL9C6/p1631969621005800. We would like to stream the movie in the garden at 19.30 and also invite people from outside (Klugis, NDK people etc...)

### Round 2
* [janina] MOI: time to decide the time it should happen! and/or maybe we agree on an orga team and let them handle everthing?
    * not super much interest in having a MOI again
    * Larissa, Zui and Janina are interested in thinking about it and will talk
* [matthias] sickness measures?
    * exchanging bedsheets, deep clean the house - when should it happen?
    * there are still some people with diarrhea, so maybe not now?
    * on Wednesday a person comes visiting, so maybe we do the cleaning action before that?
        * or we do it on the weekend, because the house will be quite empty then
        * Sunday seems to be the most fitting day for that
        * Matthias will write a task list, Janina facilitates
    * another proposal: one definitely 'clean' toilet could be the lower staircase toilet
        * accepted, will last for this and next week
        * Antonin will make a sign
    * let's please all be very conscious about not spreading poop traces in the next two weeks!
    * another proposal: remove all towels next to the sinks, because those are always breeding grounds for germs
        * or exchanging them daily, everyone is invited to take care, Matthias will also
        * with no new people integrating it should be okay
* [silvan] how to continue with the corona framework?
    * there was/is a vote on this in #kanthaus-corona which had the outcome that people would like to either update or suspend it
    * current framework is a bit useless
    * almost everybody in the house is vaccinated - only andrea might not be. matthias will talk to her and find out how to continue.
    * tilmann has resistance against just dropping it informally and would rather start a formal process to change the Collective Agreements.
        * https://ukuvota.world/#/app/0d1d1106-1b6c-bcd6-3220-28344677a870/collect
    * maxime has the slightly related question if we ask people to be vaccinated for the building month. 
        * seems to be a bigger topic. clear proposal will come in next come.

### Round 3


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** zui, chandi
- **Tue.:** 
- **Wed.:** 
- **Thu.:** Thore
- **Fri.:** Andrea, Janina
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:**
- **Tue.:** larissa
- **Wed.:** Antonin
- **Thu.:** larissa
- **Fri.:** larissa
- **Open Tuesday:** Antonin, Larissa, Maxime
- **Week:** Matthias, Silvan, Bodhi

### End result
- **Mon.:** zui, chandi
- **Tue.:** Antonin
- **Wed.:** Maxime, Larissa
- **Thu.:** Thore
- **Fri.:** Andrea, Janina
- **Open Tuesday:** Tilmann

## 7. For next week

