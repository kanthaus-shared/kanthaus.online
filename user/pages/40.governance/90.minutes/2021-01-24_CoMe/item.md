---
title: Coordination Meeting
date: "2022-01-24"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #228
* Date: 2022-01-24
* Facilitator: Antonin
* Notary: clara
* Mika caretaker: Kita
* Levi caretaker: janina, larissa
* Physical board caretaker: kito
* Digital calendar: clara
* Reservation sheet purifier: doug
* Present: chandi, clara, matthias, larissa, kito, doug, levi, janina, antonin, silvan, nathalie, änski, tilmann

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/606af270e324df8cb28591506.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 21.1 people/day (+3.6)
- **⚡ Electricity**
    - usage: 152.88 €/week (⬇️-3%)
    - of that for heating: 91.92 €/week
    - paid: 127.61 €/week 
    - ☀️ self produced: 16% (⬆️+4%)
    - emissions: 87 kg CO₂ₑ/week
- **💧 Water**
    - paid: 23.14 €/week (⬆️+13%)
    - emissions: 2 kg CO₂ₑ/week
    - 
### Expenditure
- toilet paper and oil 25€
- Haferflocken ~?

### Income

- 100€ Donation from group

### Things that happened

#### In or around Kanthaus
* great bitte wenden brunch at kanthaus
* another group vitsited kanthaus finally
* another finance & passing over responsibilty meeting
* the pfennig bau person came
* a K18 meeting
* we watched a documentary about us and danni
* very casual social sauna happened


#### Wider world
* corona numbers are rising in germany
* corona measurements world wide are getting reduced

## 2. This week planning

### People arriving and leaving
* **Mon.:** änski arrives, simon arrives
* **Tue.:** Anja, Vroni and Bodhi leave
* **Wed.:** Andrea and Bodhi come back
* **Thu.:** änski leaves, Lise & Bodhi leave with KMW, simon leaves
* **Fri.:** 8-Gemök-People arrive, findus arrives
* **Sat.:** JaTiMiLe leave for ToI
* **Sun.:** 8-Gemök-People leave
* **Mon.:** 
* **Some day:** clara leaves

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
No sun at all, some rain towards the end of the week, not freezing.

### Evaluations and check-ins
- Bodhi Volunteer : Absolute Days threshold 125/60 (+108%)
   * [Bodhi] i'm probably due but would prefer not to be evaluated this time, if its very important to people it can be today (monday). Will try to have it on my umbrella for my next visit (end of Feb.)
- Anja Volunteer : Absolute Days threshold 73/60 (+22%)
- kito would like to be evaluated


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
    * Park cars on even/this side [done]
    * 14:00 Knowledgesharing on Door Tags [chandi]
    * 17:00 Radeln Ohne Alter Wurzen meeting @Flat? [Nathalie, Antonin]
* Tuesday
    * 14:00 Painting banners for Critical Mass @Dragon Room [Clara, Janina]
* Wednesday
    * Park cars on odd/that side [Antonin]
    * 11:00 Kito's Evaluation [clara]
    * 14:00 ToI orga meeting @Lantern [Larissa, Nathalie, Doug, Janina]
    * 20:00 Presentation about situation for refugees in Bosnia @Piano Room [Kito]
* Thursday
    * 10:00 Power Hour [Fac.: Larissa, DJ: ?]
* Friday
    * 11:00 Project Updates @Piano Room [Doug] 
* Saturday
    * ?:00 big oatly unloading action 🥛 [chandi]
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Matthias] Spices! All bio: Paprika, Cumin seeds, curry, garlic powder, oregano for 62€ ![](https://pad.kanthaus.online/uploads/606af270e324df8cb28591505.png)
* [Antonin] organic wheat flour, 10kg, 7.50€

## 4. To do

* [ ] repair Schaukasten 
* [ ] Clean K18 garden
* [ ] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [x] Electrically disconnect the ceran stove on the right kitchen side
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Install light in the hallway of the K20-1 flat
* [ ] Install light in the hallway of the K20-2 flat

### more advanced tasks
* [ ] continue Grundstücksverschmelzung K20 & K22 combining both properties)


## 5. Discussion & Announcements

### Round 1
* [toi team] The ToI session schedule is to be finalized this Wednesday! You can look at the current version in the [online calendar](https://cloud.kanthaus.online/apps/calendar/timeGridWeek/2022-02-01). Please give your feedback before Wednesday's ToI meeting!
    * From Wednesday on people are encouraged to already sign in for repro shifts, so that most of the coordinative work can be done before ToI even starts. Please help us make that work!
    * As always: If you have any questions, please do not hesitate to approach us!
* [tilmann, janina] ToI childcare needs: If we think of 12h/day childcare (e.g. 8am to 8pm), and just one person for both kids, that would mean 12hx7days=84h in a week. Divided by 14 people that leaves 6h of childcare per person. This figure is just to get a feeling, of course people can do different amounts of childcare and we will always be the fallback.
    * With this in mind, let's use this week as trial! 2-3 hour slots work very well! :)
    * If people are up for night shifts, that's also very welcome!
* [Antonin] do we want to do a money fish-bowl again? This week or rather after TOI?
    * rather the week after TOI
* [chandi] Gemök Group of 8 people on the weekend. Details: https://yunity.slack.com/archives/C3RS56Z38/p1641388683328000
    * corona wise same as the group previous weekend (isolated, masks in communal rooms)
    * reserved rooms: Hipster, Yoga, and ex-food-storage (or sleep kitchen possible? @family)
    * Feedback other group: it worked out nicely; them wearing masks worked surprisingly well, house didn't feel crowded, but also sad to not interact more
* [larissa] KMW will go to Harzgerode (thursday) and then to Könnern. Everything bulky that you want in Könnern and don't need for three days should go in the KMW by Thursday!

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Antonin] inviting Florian, maybe Wednesday
- [larissa] Jacket sale is over, they are now in the Vortex
- [kito] you can sign up in tasks again this week


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** kito
* **Tue.:** Matthias, Clara
* **Wed.:**
* **Thu.:** 
* **Fri.:** chandi
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** nathalie, Antonin
* **Tue.:** änski, nathalie, Antonin
* **Wed.:** änski, nathalie
* **Thu.:** änski, Antonin, doug
* **Fri.:** änski, 
* **Open Tuesday:** Janina, änski
* **Week:** Larissa, Tilmann, Lise, Silvan

### Result
- **Mon.:** kito, änski
- **Tue.:** matthias, clara
- **Wed.:** doug
- **Thu.:** janina
- **Fri.:** chandi, antonin
- **Open Tuesday:** nathalie
 
## 7. For next week
* 
