---
title: Coordination Meeting
date: "2021-07-26"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #202
- Date: 2021-07-26
- Facilitator: Janina
- Notary: Matthias
- Mika caretaker: Doug
- Levi caretaker: Janina
- Physical board caretaker: Larissa
- Digital calendar: Zui
- Reservation sheet purifier: Andrea
- Present: Larissa, Doug, Clara, Matthias, Zui, Lenz, Michal, Tilmann, Momo, Andrea, Talita, Anja, Maxime, Janina

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
- script broken again... -.-


### Expenditure
- Toilet paper, frying oil
    - [michal] Please consider buying raps and not sunflower (much healthier omega fatty acid ratio)
    - [matthias] I know that chandi prefers sunflower due to its high oleic attributes
    - :-() maybe can buy both? :)

### Income
- 15€ in the shoe

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- Incidence: 14.3 (Germany), 5.0 (Saxony), 3.9 (Landkreis Leipzig)
- Adjusted prevalence Saxony: 0.01 -> framework still suspended
- µCOVIDs available last week: {COVIDS FOR LAST WEEK NOT FOUND!}
- µCOVIDs used last week:
- µCOVIDS balance from last week:  
- µCOVIDs additional this week: 5000
- µCOVIDs available this week:  
- µCOVIDs/person mean for this week:  (18 people)

### Things that happened
- Matthias got oat milk, soy cubes, tools, tents and Clara from HZ
- Chris the American biker visited for 2 nights
- Hanabi - twice!
- We watched the 2019 version of Les Miserables and had nice discussions afterwards
- Nathalie, Larissa, Zui, Kiki and Maxime shared photos and stories from the time they were away
- Sooo much sweet milk...
- Janina and Andrea invented canihua-onion-patties
- NDK outdoor cinema the whole week
- JaTiMi visited a KiTa
- Momo's check-in
- some people attended a feminist festival in Halle

## 2. This week planning

### People arriving and leaving
- **Mon.:** Jon_R arrives, Silvan comes back
- **Tue.:** Lenz will leave; maybe some people come for the party and stay 1 night, NaTho come back
- **Wed.:** Larissa and Silvan maybe leave, Anja and Doug leave
- **Thu.:** Larissa and Silvan definitely leave
- **Fri.:**
- **Sat.:** Jon_R leaves
- **Sun.:**
- **Mon.:** Larissa comes back
- **Some day:** Michal leaves (??h), Doug comes back

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_Unfortunately the weather will stay quite hot over the next week. No rain, no wind, a little bit of clouds. A bit colder towards the weekend_

### Evaluations and check-ins
- Tilmann
- Anja

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe: Janina
    * 14:00 Knowledge sharing: GIMP [Maxime]
    * Repark cars in front of K20 [already done]
- Tuesday
    * Maxime's Birthday
    * 11:00 Board member meeting [Matthias]
    * 15:00 Bitte Wenden Meeting (local mobility transformation group) [Clara, Janina]
    * 16:00 Open Tuesday with big pants sale [Anja]
    * 20:30 Happy Bassday: electro/dub party [Maxime, Doug]
- Wednesday
    * Repark cars to the other side of the road not in front of K20 [Maxime]
- Thursday
    * 10:00 Power Hour [Fac.: Clara, DJ: the headphones might be gone :scream:]
    * 15:00 Sharing event [Janina(if childcare provided)]
- Friday
    * 11:00 Tilmann's evaluation [Maxime]
    * 12:00 Markt Abholung [Andrea]
    * Gelbe Tonnen [Zui]
- Saturday
    *
- Sunday
    *
- Next Monday
    * 10:00 CoMe [Zui]
- Next week summary
    *

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- Sunflower seeds (Janina posted in food channel)
    - wheat flower if the same shop has some? :D (matthias)
- 6€ Rapsoil [Talita]
    - yes. Additionally maybe somebody can look up if somehow reasonable to buy bigger amounts?

## 4. To do
(Doug has details about some of these)
* fix backpack storage shelf in k20 staircase
* replace broken/string/toaster shoe rack spots
* make k20 garden door easily closable from outside (or acquire a new door)
* secure wash kitchen sink
* fix curtain holders in 'the private'
* fix hipster room door indicator
* think about plastic waste containers to be used in kitchen, snack kitchen and dumpster kitchen

## 5. Discussion & Announcements

### Round 1
- [Silvan] I would like to [take stuff from KH to ACRONYX](https://pad.kanthaus.online/KH-Acronyx) Is everybody ok with it?
- [michal] Maria was mentioning a missing white plastic chair from the small garden in front of the flats. Does anybody know about it?
    - Nobody has seen it
- [Janina] Visitor request for one night end of July: Robin and Anett
    - Matthias and Clara take care
- [Doug] Building month: [poll](https://poll.disroot.org/kCZtUK2KRDezaR3R) suggests 25th Oct - 21st Nov. Shall we do it? :)
    - [Tilmann] seems a bit late, maybe try one week earlier? November is often wet from dew and less sun
    - [Clara] For the FFJ group it would be cool to have 1 week with and one without building week (11.-24. of October they are here)
    - [Janina] we should consider that and then decide for the date.
    - [Maxime] Maybe the weather should be weighted into this more?
    - [Zui] How about not doing it anymore this year and rather wait until next spring if people are not motivated?
    - [Janina] Maybe amore open discussion on this really makes sense to avoid frustration.
- [matthias] board member meeting?
    - There are some tasks still to be done and upcoming. Would be cool to distribute them and have everything clear.
    - Would be for old and new board members of both associations.
    - Especially handing in the reports of the general meetings needs to happen very soon.
    - Everyone can attend, it doesn't have to be the board members specifically, but at least those should feel responsible... :)
- [Maxime] Please don't forget to refill newspaper in compost bins
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Silvan] The plants in the garden need water, especialy the tomatoes, cucumbers and zuccinis need to be watered daily.
- [Doug] Short survey about Nextcloud calendar/contact integration! I would be grateful for responses: https://cloud.kanthaus.online/apps/forms/dLCXDW9SXbkfprcL
- [Tilmann] need more yellow bins! 2 more would be good…
    - [Janina] Paper would actually also be nice.
    - [Tilmann] maybe we can use the bins of the flat as well?
        - We should maybe ask Thore about this again
    - Larissa can take care of it
- [matthias] Disabling phone/having the answering machine answer within 5 seconds?
    - Maybe we should have phones in more places?
    - Some people actually like answering the phone
    - Maybe would need rather 10-15 seconds so people expecting a phone call can get it
        - [maxime] please no: high noise pollution


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Larissa
- **Tue.:** Clara
- **Wed.:** Andrea + Maxime
- **Thu.:** Momo + Zui
- **Fri.:** Matthias + Momo
- **Open Tuesday:** Anja + Janina

### Unavailabilities
- **Mon.:** Clara, Andrea, Zui, Momo
- **Tue.:** maxime, Andrea, Momo, Zui
- **Wed.:** Momo
- **Thu.:** Andrea
- **Fri.:**
- **Open Tuesday:** maxime, Andrea
- **Week:** michal, Talita, Tilmann, Doug, Lenz

## 7. For next week
-
