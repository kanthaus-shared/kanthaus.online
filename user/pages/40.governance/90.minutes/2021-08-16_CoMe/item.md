---
title: Coordination Meeting
date: "2021-08-16"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #205
- Date: 2021-08-16
- Facilitator: Maxime
- Notary: Matthias
- ~~Mika caretaker:~~
- ~~Levi caretaker:~~
- Physical board caretaker: Andrea
- Digital calendar: Matthias
- Reservation sheet purifier: Clara
- Present: Maxime, Clara, Matthias, Andrea, Talita, Laura

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_531f455efcc98d63628123b775f33ab4.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 7.1 people/day (-8.0)
- **⚡ Electricity**
    - usage: 3.13 €/day (⏬-46%)
    - paid: -5.97 €/day
    - ☀️ self produced: 69% (0%)
    - emissions: 3 kg CO₂ₑ/week
- **💧 Water**
    - paid: 1.92 €/day (⏬-43%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
- [talita] toilet paper (6,5€) and rapsoil for frying (3,5€) - 10€
- [talita] yoga room: paint (92€), gips (15€), support for gipsed corners (8€)- 115€

### Income
- 0€ in the shoe

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)

> From [Collective Agreements](https://kanthaus.online/en/governance/collectiveagreements): *If adjusted prevalence is < 0.1%, we have no formal restrictions.*
> Current **adjusted prevalence** can be found [here](https://www.microcovid.org/?duration=600&interaction=oneTime&personCount=1&riskBudget=100000&riskProfile=average&scenarioName=custom&setting=indoor&subLocation=Germany_Sachsen&theirMask=none&topLocation=Germany&voice=loud&yourMask=none&yourVaccineType=johnson): click on "Details" in the "Step 1" column

- Incidence:
    - Germany: 36.6 (+55%)
    - Saxony: 12.6
    - Landkreis Leipzig: 32,6 (+152%)  <- masks are obligatory again in markets etc.!
- Adjusted prevalence Saxony: 0,03% (last check: 2021-08-15)
- µCOVIDs available last week: {COVIDS FOR LAST WEEK NOT FOUND!}
- µCOVIDs used last week:
- µCOVIDS balance from last week:
- µCOVIDs additional this week: 5000
- µCOVIDs available this week:
- µCOVIDs/person mean for this week:  (18 people)

### Things that happened

#### Around Kanthaus
* Some of us went watching the documentary "Capital in the Twenty-First Century" at Villa Klug
* There was a visit of Kreuzgasse 1
* We had a chat in the garden
* All Kanthaus residents (the 5 of them) went together to Wolfsberg
* Talita made huge progress in the Yoga Room
* Our garden gave us a lot of ripe tomatoes and basilicum
* The clock in the dining room was replaced by a much more accurate and less noisy Penguin of Time, which let us know when it is time, and also allows to have meals in silence without hearing a ticking

#### In the wider world
* The 6th IPCC report was published, stating that climate change is going faster that previously anticipated
* After 20 years of war, Talibans took Kabul and now control the whole of Afghanistan. A new refugee crisis is to be expected.

## 2. This week planning

### People arriving and leaving
- **Mon.:** Mariha, her partner and their 2 year old child arrive; ZuLa come back, michal leaves,
- **Tue.:** Doug arriving
- **Wed.:**
- **Thu.:** Talita and Laura leaves (or Friday morning)
- **Fri.:** Nathalie arrives, Larissa leaves maybe with zui,
- **Sat.:**
- **Sun.:** (Sunday or Monday) chandi comes back, Clara leaves (or Monday)
- **Mon.:**
- **Some day:**

### Weather forecast

<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

Nice, but windy and sometimes a bit rainy weather. Take care of doors and windows :-)

### Evaluations and check-ins

Due for evaluation:
- Andrea Volunteer : Absolute Days threshold 68/60 (+13%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Maxime]
    * ~~14:00 Knowledge sharing []~~
    * Park cars on even-side [Maxime]
- Tuesday
    * Hausmüll [Matthias]
    * Open Tuesday [Doug]
- Wednesday
    * Park cars on odd-side [Maxime]
    * Landgut Nemt pickup []
    * 15:00 Evaluation Andrea [Maxime]
- Thursday
    * 10:00 Power Hour [Fac.: Matthias, DJ: ]
    * 15:00 Sharing event []
- Friday
    * Biotonne [Clara]
    * 12:00 Market Pickup [Maxime]
    * Landgut Nemt pickup []
- Saturday
- Sunday
- Next Monday
    * 10:00 CoMe [Matthias]
- Next week summary
    *

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
- [Talita] 27€ paint for the yoga room

## 4. To do
_Small fixes of things in the house_

* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] make k20 garden door easily closable from outside (or acquire a new door) https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] secure wash kitchen sink
* [ ] think about plastic waste container to be used in dumpster kitchen
  - [maxime] what about using a big cardboard that could be emptied in the yellow bin, and replaced from time to time when too nasty (we associated recommendation to empty liquids before throwing there)
* [ ] fix loose table leg in Lantern
* Ventilation preparation:
    * [ ] Tear down chimney in Snack Kitchen
    * [ ] Tear down chimney in communal sleeping room
    * [ ] Tear down chimney in dining room
    * [ ] Tear down chimney in piano room


## 5. Discussion & Announcements

### Round 1
- [matthias] Do we want a "Nick-trailer like" trailer? Matthias would like to build it for approx. 280€. Would feature perfect suitability for green boxes, lights and a "tool/waterbottle storage" https://yunity.slack.com/archives/C3RS56Z38/p1628839029144500
    - Maybe Nathanael can be used in the mean time? Do we really need a small trailer?
    - On the other hand having more than one nice dumpsterdiving trailer for simultaneous dumpster diving might make sense
    - People are happily invited to participate in the slack thread
    - Might be brought up again next week
- [maxime] should there be a task lottery this week, or should we do like last week?
    - Spontaneous cooking :-)
- [Doug] there is a lot of support and no resistance to Maxime's post for the roof month framework https://yunity.slack.com/archives/C3RS56Z38/p1628514489137000?thread_ts=1628181795.098000&cid=C3RS56Z38
    - Roof month from 2021-10-11 - 2021-11-07
    - FFJ group will be here all the time then (2021-10-11 - 2021-10-24), some confusion as we thought there was only one week overlapping -> Communication with the group again
    - We will wait for their feedback and then talk about it again
- [Thore] It´s time to call Felix from Landgut Nemt to coordinate the aftercrop actions. Can someone do this? I'll be back in Wurzen at the 29 of August and can imagine to organize then some actions with someone together? Anyone interested?
    - No reactions in the (small) room, let's move the discussion to Slack?


<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [matthias] I added chimney tear down tasks in the todo list above from the ventilation team. The list follows our priorities in which we want to work. We think, it might be good to announce early and have the possibility to do work already now, when the house is not so crowded. In the step of installing the ventilation later this year, we would then properly close and seal what comes up behind the chimneys :-)
- [Maxime] fairtailer?
  - Andrea volunteers to check this week

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
* None this week: voluntary based

### Volunteers
- **Mon.:**
- **Tue.:**
- **Wed.:**
- **Thu.:**
- **Fri.:**
- **Open Tuesday:** Doug

### Unavailabilities
- **Mon.:** Andrea
- **Tue.:** Andrea
- **Wed.:**
- **Thu.:**
- **Fri.:**
- **Open Tuesday:**
- **Week:**

### End result
- **Mon.:**
- **Tue.:**
- **Wed.:**
- **Thu.:**
- **Fri.:**
- **Open Tuesday:**

## 7. For next week

