---
title: Coordination Meeting
date: "2022-08-08"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #255
* Date: 2022-08-08
* Facilitator: Martin
* Notary: Janina
* Mika caretaker: Larissa
* Levi caretaker: Larissa
* Physical board caretaker: Eric
* Digital calendar: Antonin
* Reservation sheet purifier: Janina
* Present: Martin, Janina, Eric, Maxime, Tilmann, Antonin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/131a73a6-b241-4e0e-95f7-608a43185fe0.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.0 people/day (-0.3)
- **⚡ Electricity**
    - usage: 39.71 €/week (⬆️+26%)
    - of that for heating: 2.75 €/week
    - paid: -61.35 €/week
    - ☀️ self produced: 85% (⬆️+9%)
    - emissions: 9 kg CO₂ₑ/week
- **💧 Water**
    - paid: 11.76 €/week (⬇️-7%)
    - emissions: 0.9 kg CO₂ₑ/week

### Expenditure
*

### Income
*

### Things that happened

#### In or around Kanthaus
* Matthias officially moved out
* Hang-out at Villa Klug
* Generally empty summer vibe

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* Escalating tension around Taiwan due to Pelosi visit.
* Summer fires everywhere

## 2. This week planning

### People arriving and leaving
* **Mon.:** Juda & Lissy come, Antonin comes back, Silvan leaves
* **Tue.:** Juda & Lissy leave, Larissa leaves
* **Wed.:** Tobi arrives
* **Thu.:** Nathalie, Thore arrive
* **Fri.:** Tobi leaves, Matthias arrives for a few days
* **Sat.:**
* **Sun.:**
* **Mon.:**
* **Some day:** Bodhi might come

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_temperature rises towards the end of the week, no rain, some clouds

### Evaluations and check-ins

:judge: Due for evaluation (and seen within the last 7 days):
- Antonin Member (just arrived, so not in the list, but 2 weeks ago already over 30%)
- Silvan Volunteer : Absolute Days threshold 81/60 (+35%)
- Eric_WA Visitor : Days Visited threshold 28/21 (+33%)
- Nathalie Volunteer : Absolute Days threshold 66/60 (+10%)
- Thore Volunteer : Absolute Days threshold 62/60 (+3%)



### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
    * Park KMW & trailer on even/this side [Maxime]
* Tuesday
    * 15:00 - 17:00 Open Tuesday [Eric]
* Wednesday
    * 13:30 Antonin's evaluation [Janina]
    * 15:00 Bicycle repair workshop at Kulturcafe [Martin, Eric]
    * Park KMW & trailer on odd/that side [Maxime]
* Thursday
    * 10:00 Power Hour [Fac.: Antonin, DJ: ?]
    * 20:00 Project Updates [Antonin]
* Friday
    * Gelbe Tonne [Martin]
    * 12:00 Market pickup [Eric]
    * 15:00 K-Pop Cafe at Kulturcafe [Janina, Andrea]
* Saturday
    * Potential tree cutting action @Unkraut's garden
* Sunday
    *
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary
    * Highfield festival

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* [matthias] Bicycle repair station "opening" and adding more tools on Saturday? (@martin, antonin; potentially others?) -> Martin will talk to Matthias

## 3. Shopping plans
* [Antonin] Server to host our NextCloud instance: 10.80€/month (+ one-time set-up fee of 10.80€) for a [Start-2-S-SATA server from Scaleway (online.net) (9€/month pre-tax)](https://www.scaleway.com/en/pricing/?tags=available,baremetal-dedibox-start)
    * some discussion, but generally no resistance


## 4. To do
_Newest tasks on top_
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put on and off?
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Doug] Responsibility hats up for grabs!
    * :womans_hat: Freeshop (Eric, Unkraut, Larissa?)
        * Larissa leaves tomorrow, but Eric is on
    * :mortar_board: Sorting (Martin, Maxime, Silvan?)
        * Silvan is not here, but Jati are motivated to do it with Eric and Martin
* [Janina] Anja's offer to get cucumbers and/or tomatoes: I'd be very motivated to make preserved tomato sauce if anybody was to get a trailer full of tomatoes! :)
    * [maxime] could do!

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Janina] Let's try and get the attics insulated before winter! It's not undoable and mostly Tilmann needing the time to do it. We could even frame it as child-care weeks instead of building weeks this time...^^
    * Suggestion: Block mid-October to mid-November for this, e.g. 17.10 - 13.11.
    * Also, Attic Focus this week: Tilmann wants to spend time on the attic if there's childcare this week
      * Martin will do some tomorrow!

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:**
* **Tue.:** Janina
* **Wed.:**
* **Thu.:**
* **Fri.:** Antonin
* **Open Tuesday:** Eric

### Unavailabilities
* **Mon.:** Martin, Maxime
* **Tue.:**
* **Wed.:**
* **Thu.:**
* **Fri.:** Martin
* **Open Tuesday:**
* **Week:** Tilmann

### Result
- **Mon.:**
- **Tue.:** Janina
- **Wed.:** Martin
- **Thu.:** Maxime
- **Fri.:** Antonin
- **Open Tuesday:** Eric

## 7. For next week
*
