 ---
title: Coordination Meeting
date: "2021-03-22"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #183

- Date: 2021-03-22
- Facilitator: zui
- Notary: Tilmann
- Mika caretaker: Silvan/Maxime
- Physical board caretaker: Janina
- Digital calendar: chandi
- Reservation sheet purifier: Talita
- Present: zui, Talita, Michal, Janina, Tilmann, Silvan, Nathalie, Maxime, chandi

----

<!-- Minute of silence -->

### 0. Check-in round

### 1. Last week review
##### Stats
<!--  don't read them all out^^ -->
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_3e8da9b09d49dff8ce9583fb9e1e4959.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.0 people/day (-0.7)
- **🌡️ Average outdoor temperature:** 4.2 °C (-2.6 °C)
- **⚡ Electricity**
    * usage: 8.93 €/day (⬆️+12%)
    * paid: 2.94 €/day 
    * ☀️ self produced: 57% (⬇️-1%)
    * emissions: 7 kg CO₂ₑ/week

- **💧 Water**
    * paid: 3.06 €/day (0%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 7.35 €/day (⬆️+5%)
    * emissions: 173 kg CO₂ₑ/week

##### Expenditure
- [chandi] 14€ frying oil

##### Income

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 16167
- µCOVIDs used last week: 1935
- µCOVIDS balance from last week: 14232
- µCOVIDs additional this week: 5000
- µCOVIDs available this week: 19232
- µCOVIDs/person mean for this week: a lot

##### Things that happened
- interesting social glue session
- Lise left with a KMW full of things - farewell and see you soon!
- another power day in the yoga room with more work in the days after
- new shelves and cupboards arrived
- Janina was reaccepted as member
- Nathalie and Doug checked out a rickshaw for ROAW
- Kanthaus presentation at GS10
- chandi and Tilmann worked on a Kitchen shelve prototype
- Acroyoga!

### 2. This week planning

##### People arriving and leaving
- **Mon.:**
- **Tue.:** Thore arrives
- **Wed.:** EO arrives, Michal leaves (or a day later)
- **Thu.:** 
- **Fri.:** Larissa and Kerstin arrive (maybe Hannah)
- **Sat.:** 
- **Sun.:** EO leaves
- **Mon.:**
- **Some day:** 

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
Please give a summary, dear notary! It's great for future reference.
-->

##### Evaluations and check-ins
- Michal Volunteer : Absolute Days threshold 61/60 (+2%)

##### Schedule ([In Nextcloud](https://cloud.kanthaus.online/apps/calendar/))
<!-- Ja&Ti's availability due to Mika care as of 2021-02-20: 14-17, 60% of the time -->
- Monday
    - 10:00 CoMe [Zui]
    - 13:00 On-site appointment with city officials for foodsharing Fairteiler [Janina, Nathalie]
    - ~~15:00 Sharing event [?]~~
- Tuesday
    - 9:00 - 11:00 chimney sweeper visit [chandi]
    - 15:00 Michal evaluation [Tilmann]
- Wednesday

- Thursday
    - 10:00 Power Hour [Fac.: zui, DJ: zui]
    - 14:00 Knowledge Sharing: back-up party [chandi] <!-- https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
    - 20:00 Evening Activity: acroyoga [Zui]
- Friday
    - plasic trash (yellow)
    - Activity: Pödelwitz visit
    
- Saturday
    - 20:00 project updates [Janina]
- Sunday

- some day:

- Next Monday
    - 10:00 CoMe [chandi]
    - 15:00 Sharing Event []
- Next week summary


### 3. Shopping plans

- [Bodhi] 134 € Battery circular saw. this one is a bit bigger then lises, same blade-size as the non-battery one we have and has a absaugung. https://geizhals.de/makita-dss611z-akku-handkreissaege-solo-a1222079.html?hloc=at&hloc=de&v=e
  - no resistance
- [Antonin] 116.27 € scaffolding security accessories (Gerüstschutznetz 48.80€, Baustellenwarnleuchte 22.56€, Gebrauchte Leitbake 46€ für 2, alle mit Versandkosten)
  - no resistance
- [Silvan] 20-50€ for a vacuum cleaner or a special part to clean the carpets properly :)
  - no resistance

### 4. Discussion & Announcements

#### Round 1
<!-- Is there much food to be washed? -->
- [zui] food to be washed? not much vegetables, we may need to get more before the next weekend
- [Thore] The voting phase for the private agreement ended yesterday with 8 support and 1 accept. Thank you :-) So when I'm coming back tomorrow I would use the private again. 
- [Janina] will there be a building month? there are visitor requests
  - [zui] proposal for building weeks for first roof (e.g. 15.-29. than a party?) and the second roof end of the summer
  - [antonin] is happy with those dates
  - [tilmann] scaffolding space is reserved for 2 months, this seems to be the worst case estimation by Antonin and Matthias?
  - [chandi] 2 weeks is not totally unrealistic, 3 weeks might be a better goal
  - [zui] needs to be discussed again
- [Nathalie] lunch team
  - [Janina] we go forward with the dynamic lunch team. please always remove your peg after eating, anybody can remove pegs in the evening
  - [Thore] Can someone put a peg tomorrow for me? So that I can eat lunch.

<!-- pause for people who haven't spoken, to think about whether they have a point -->
#### Round 2
