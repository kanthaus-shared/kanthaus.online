---
title: Coordination Meeting
date: "2021-11-29"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #220
- Date: 2021-11-29
- Facilitator: Nathalie
- Notary: Tilmann
- Mika caretaker: Janina
- Levi caretaker: Matthias
- Physical board caretaker: Maxime
- Digital calendar: chandi
- Reservation sheet purifier: doug
- Present: Nathalie, Chandi, Mika, Janina, Charly, Maxime, Larissa, Tilmann, Doug, Matthias

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_d1b030fe93f9a0d9f940c5ec526fdd5f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.6 people/day (+1.7)
- **⚡ Electricity**
    - usage: 17.87 €/day (⬆️+52%)
    - Heating costs: 11.82€/day (for 1 MWh/week) of that, 11% has been covered by our own solar electricity
    - paid: 14.76 €/day
    - ☀️ self produced: 17% (⬇️-4%)
    - emissions: 74 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.77 €/day (⬆️+17%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [chandi] 70€ 25x Corona-Schnelltests + 70 FFP2 Masken
- [matthias] ~158 € + ~6 hours for the KMW trailer stuff:
    - 30€ for KMW trailer Radlager
    - 36€ for KMW trailer biyearly checkup
    - 92€ for KMW trailer tires
- [antonin] 25€ for Denkmalschutzantrag
- [silvan] 16€ for 80cent-stamps
- [tilmann/matthias] ~1000€ for next insulation parts (Dampfsperre)
- [maxime] 10€ for 5kg oatmeal
- [matthias] 5€ building rubble disposal

### Income
- nothing


### Things that happened
- a lot of food was saved
- rubble has been disposed, space below scaffolding cleared
- some radiators were cleaned
- deep clean behind piano room sofa with toys re-discovered
- cafe espanol with turon eating
- AoE LAN party
- roof actually did get finished!
- Rickshaw introduction for Radeln ohne Alter Wurzen
- Rickshaw items have a place in K20 entrance
- Bitte Wenden meeting took place
- mold discovered in main office with quick&dirty fixup

### Wider world
- new corona variant "Omicron" found. our region is now at an incidence rate of 1717, intensive care number aproach their all time high, even though there are less beds & staff available within this wave.

## 2. This week planning

### People arriving and leaving
- **Mon.:** Elisa comes
- **Tue.:** Zui, Änsky, Lise, Anja, findus arrive, Andrea leaves
- **Wed.:**
- **Thu.:** Andrea comes back
- **Fri.:** Anja leaves
- **Sat.:**
- **Sun.:** Änsky leaves
- **Mon.:** Lise leaving today or tomorrow
- **Some day:** Elisa leaves (whenevener their work is done, probably Friday), Clara comes

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Snowy today, more rain in the next days, better towards the end of the week

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Andrea Volunteer : Absolute Days threshold 103/60 (+72%)
- Zui Member : Absolute Days threshold 201/180 (+12%)
- Charly_WA Visitor : Days Visited threshold 22/21 (+5%)
- Clara Volunteer : Absolute Days threshold 62/60 (+3%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Chandi Member : 0 days until Absolute Days threshold (180)
- Matthias Member : 2 days until Absolute Days threshold (180)
- Silvan Volunteer : 6 days until Absolute Days threshold (60)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Nathalie]
    * ~~Park cars on even/this side [Maxime]~~
- Tuesday
    * 11:00 Andrea's Evaluation [Fac.: Nathalie]
    * 14:00 MoI planning session [Nathalie]
    * 15:00 - 17:00 Open Tuesday
- Wednesday
    * Park cars on odd/that side [Maxime]
    * 10:00 Monthly coordination meeting [Matthias]
    * 11:00 Charly's Evaluation [Fac.: Doug]
    * 19:00 Interaction choir online [Nathalie]
- Thursday
    * 10:00 Power Hour [Fac.: Tilmann, DJ: ?]
    * 15:00 Sharing event [Janina, childcare by Doug]
- Friday
    * Yellow Bins [Larissa]
    * 14:00 - 15:00 Attic cleanup action [tilmann, matthias, +3 (you?), doug may be asked] https://yunity.slack.com/archives/CN9H631BM/p1638171892035800
- Saturday
    *
- Sunday
    * 15:00 FINTA cafe [Larissa]
- Next Monday
    * 10:00 CoMe [chandi]
- Next week summary
    *

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- knowledge sharing proposals:
    -  https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [chandi] stock up on basic incredients we buy regulary. I know that a lot of people don't really care, whether we buy ecological or not, but it's important to me and I'm happy to pay the surcharge by myself! ;)
    - Frying oil
        * 93€ 20l Bio (=4,65 €/l) https://www.rossmann.de/de/ernaehrung-enerbio-bratoel/p/4305615678061
        * 99€ 20l Naturland (=4,95 €/l) https://www.crg-shop.de/products/biio-bratol-high-oleic-professional?variant=40297883140269  (a wired shop with only 3 ratings on ebay and no other reference towards)
        * 'Cheapest' 0.99€/l
    - sunflower seeds
        * 28€ 10kg Bio (=2,78 €/kg) https://www.rossmann.de/de/ernaehrung-enerbio-sonnenblumen-kerne/p/4305615678276
    - oats
        * 37€ 20kg Naturland (=1,85 €/kg) https://www.dm.de/dmbio-flocken-haferflocken-feinblatt-naturland-p4058172307409.html
        * 'Cheapest' 0.78€/kg
    - wheat flour?
- [Doug] ibuprofen

## 4. To do
_Small fixes of things in the house_

* [x] install a light switch (or detector) in the washroom [Antonin]
* [x] bringing KMW Trailer to TÜV **within November** [Larissa, Matthias, Doug]
* [ ] fix K22-2 toilet flush pipe
* [ ] fix hipster room window curtain
* [ ] more storage spaces for visitors [charly]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Building week clean up tasks
    * [x] Tarp in the garden
    * [ ] scaffolding net in the garden
         - [antonin] if we are to store the scaffolding in the basement again, then I would wait for that to happen first (to leave space for big pieces first), and then store it there too, maybe?
    * [x] Working clothes, shoes, etc. in the K22 hallway
* [ ] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900)
* [ ] sort and introduce a categorization in the kitchen shelves

### New this week
* [ ] Fix the scarves drawer in the K20 hallway
* [ ] Ex-food-storage: install private occupancy indicator
* [ ] repair right bike shed light


## 5. Discussion & Announcements

### Round 1
- [matthias] Insulation building work: There is lots of work to be done in the attic for all kinds of knowledge levels (lots of cleanup, some woodwork, putting insulation material in corners, ...). Please support Tilmann and me if you feel like doing physical work. Also childcare to free up Tilmann is highly appreciated!
- [Doug] Cleanup bingo experiment finished. [Not very successful](https://yunity.slack.com/archives/CFDNQL9C6/p1638104662096100). Suggest running ukuvota for next experiment(s) and focussing on clear attribution/monitoring for greater success.
    - [Janina] Would like to try again with the planning sheet approach we used in building month. Have one prepared. But am also fine with waiting a week.
        - We will do it for this week, then pause and reflect on it afterwards
- [Anja] I will fix clothes and do some sewing stuff while I'm around. Please put any broken clothes (communal or personal) in the box that is (maybe still) in the dragon room (and label it if it's private and if it's not clear what has to be fixed). If you find broken clothes in the communal, just put them directly there. :)
- [Nathalie] 2 days till the first of december. If you have an idea for a nice little item, text, riddle, you can put in in the adventscalenadar and we'll open it after dinner!
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [matthias] Ventilation building work: We'd like to continue ventilation towards K20-2 bathroom. It is quite some work with some dependencies. We'd like to do it soon and have the K20-2 bathroom dirty for about 10 days - it will be mostly usable, but might have limitations. Also here, help is always appreciated!
    - Generally expect that shower can be used before 9am and after 7pm
    - Might also include one day of K20-2 hallway, Communal sleeping room, dorm interruption - but will coordinate with the affected people just in time.
- [Nathalie] new sheet in the dinning room: dinner with Thore. Will be put up after CoMe. You can sign up for having dinner in the flat (includes cooking). Takes from 6pm to 7:30pm I would say (or start earlier)
- [Doug] inviting a family for chat/tea to Kanthaus
<!-- check if anyone has a point that didn't speak already -->

### Round 3
- [Tilmann] Announcement: if you're looking for a place to dry laundry, K22-2 bathroom became suitable recently, due to central ventilation!
- [Silvan] process to invite guests, given the Corona situation
    - chandi's process seems good (to announce in Slack before)


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Larissa, Silvan
- **Tue.:**
- **Wed.:**
- **Thu.:** Andrea
- **Fri.:** Nathalie
- **Open Tuesday:** Anja

### Unavailabilities
- **Mon.:** maxime, Matthias, chandi
- **Tue.:** Matthias, Tilmann
- **Wed.:**
- **Thu.:** Doug, chandi
- **Fri.:** Tilmann
- **Open Tuesday:** maxime, Matthias, chandi, Tilmann
- **Week:**

### Result
- **Mon.:** Larissa, Silvan
- **Tue.:** chandi, Janina
- **Wed.:** Maxime, Tilmann
- **Thu.:** Andrea, Matthias
- **Fri.:** Nathalie, charly
- **Open Tuesday:** Anja, Doug

## 7. For next week
-
