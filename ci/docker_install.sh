#!/bin/bash

# Mostly taken from https://docs.gitlab.com/ee/ci/examples/php.html

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer,
# ansible for deployment and wget to install composer (sigh…) and libzip-dev for
# the zip PHP extension, libpng-dev for the GD extension
apt-get update -yqq
apt-get install git ansible wget libzip-dev libpng-dev -yqq

docker-php-ext-install zip gd
